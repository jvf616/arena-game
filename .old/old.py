from random import random

print()
print("Welcome to the Arena, where those who enter can seek glory or death!")
print("Defeat all enemies to achieve victory!")

name = input("Who dares enter the arena? Enter a name:")

print()
print("Welcome to the arena", name, "!") 
print("Before you enter learn about the classes and actions.(enter/return to continue)")
skip = input("If you're already an experienced player, type Skip.")
if skip == "skip" or skip == "Skip":
    pass
else:
    print()
    print("A class determines your Armor Class, Health Points, Damage Die,") 
    input("amount of Health Potions, times you can Parry, and amount of Spells.")
    print()
    print("In Combat you have three actions, and two special attacks:")
    print("Actions include: Attack with damage die, use a Health Potion, or Counter an attack.")
    input("Special attacks include Spells and Parry.")
    print()
    print("A Counter is if the enemy rolls lower than your Armor Class, you hit them with extra damage.") 
    input("But if the enemy hits you, they hit with extra damage as well.")
    print()
    print("Spells are a special attack that does double damage.") 
    input("The spell must beat the enemy armor class to hit and you loose a spell each time it's used.")
    print()
    print("Parry is a special attack that allows you to deflect an incoming attack and do half damage.")
    input("The Parry is only used if successful. You get a 50/50 chance of success.")
    print()
    print("Similar to D&D, this game uses a 20 sided die to determine if you hit someone") 
    print("with an attack or spell or successfully parry. Damage is determined by damage die,")
    input("which include an 8, 10, and 12 sided die. They are different for each class.")
    print()
    print("All input options will be upper case! You don't need to capitalize your input.") 
    input("Use enter/return when instructed, just be mindful each time you press enter/return.")

for challenge in range(100):
    print()
    difficulty = input("With that said, which class will you choose? Barbarian, Soldier, Shield Master, Wizard, Sorcerer, Warlock, Paladin, Cleric, Battle Mage?")
    if difficulty == "barbarian" or difficulty == "Barbarian":
        title = "the Barbarian"
        character_armor = 13
        character_health = 35
        health_potion = 3
        damage_die = 12
        parry = 4
        spell = 0
        crit = 5
        print()
        print("With high vitality, high damage, and no armor,", name, title, "strikes fear in everyone they approach: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "soldier" or difficulty == "Soldier":
        title = "the Soldier"
        character_armor = 16
        character_health = 30
        health_potion = 4
        damage_die = 10
        parry = 3
        spell = 0
        crit = 4
        print()
        print(name, title, "has trained for battle for all their career, mastering decent strikes, health, and damage: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".")
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "shield master" or difficulty == "Shield Master":
        title = "the Shield Master"
        character_armor = 17
        character_health = 25
        health_potion = 3
        damage_die = 8
        parry = 4
        spell = 0
        crit = 3
        print()
        print("Valuing defense over vitality and damage,", name, title, "can avoid the most dangerous hits: AC is", 
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".")
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "Fey" or difficulty == "fey":
        title = "The Fey"
        character_armor = 20
        character_health = 100
        health_potion = 10
        damage_die = 20
        parry = 10
        spell = 10
        crit = 10
        print()
        print("Wait a minute,", name, title, ", well that's a little over powered?: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "Wizard" or difficulty == "wizard":
        title = "the Wizard"
        character_armor = 13
        character_health = 25
        health_potion = 4
        damage_die = 10
        parry = 0
        spell = 8
        crit = 4
        print()
        print("Studying the art of magic,", name, title, "can cast devestating damage and arcane spells: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "Sorcerer" or difficulty == "sorcerer":
        title = "the Sorcerer"
        character_armor = 14
        character_health = 30
        health_potion = 3
        damage_die = 8
        parry = 0
        spell = 6
        crit = 3
        print()
        print("Born with magic coursing through their veins,", name, title, "can use magic at their whim: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "Warlock" or difficulty == "warlock":
        title = "the Warlock"
        character_armor = 15
        character_health = 35
        health_potion = 2
        damage_die = 12
        parry = 0
        spell = 4
        crit = 5
        print()
        print("Making a pact with a magical being,", name, title, "has access to the full power of their patron: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "Cleric" or difficulty == "cleric":
        title = "the Cleric"
        character_armor = 16
        character_health = 30
        health_potion = 4
        damage_die = 8
        parry = 2
        spell = 4
        crit = 3
        print()
        print("Trained for battle, blessed by a diety,", name, title, "uses magic and a sword to fight: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "Paladin" or difficulty == "paladin":
        title = "the Paladin"
        character_armor = 17
        character_health = 25
        health_potion = 3
        damage_die = 10
        parry = 3
        spell = 3
        crit = 4
        print()
        print("On their holy quest,", name, title, "holds their oath till their last breath: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "Battle Mage" or difficulty == "battle mage":
        title = "the Battle Mage"
        character_armor = 15
        character_health = 30
        health_potion = 2
        damage_die = 12
        parry = 4
        spell = 2
        crit = 5
        print()
        print("Trained in both magic and sword,", name, title, "is a force to be reckoned with: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    else:
        print()
        print("You need to choose a class to continue!!!!")
        continue

print()
input("If you ever forget your character details during or brefore combat, type in Status(hit enter/return to continue)")
print()

for preparing in range(5):
    ready = input("Are you prepared to die? Yes or No?")
    if ready == "yes" or ready == "Yes":
        print()
        input("You enter a vast gladitorial arena, with a chearing crowd and the Master of the Arena watching.")
        print("A gladiator has entered the arena,", name, ", prepare for combat!")
        break
    elif ready == "no" or ready == "No":
        print()
        print("Then you are a coward", name, "...there will be no glory for you!")
        exit()
    elif ready == "Status" or ready == "status":
        print("AC is", character_armor, ", current Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        continue
    else:
        print()
        print("Can't decide what to do? Don't worry,", name, ", a decision will be made for you...")
        print("A gladiator has entered the arena,", name, ", prepare for combat!")
        break

enemy_health = 15
enemy_armor = 8

for battle in range(100):
    print()
    action = input("Will you Attack, Heal, Counter, or cast a Spell?(Use enter/return to attack)")
    if action == "attack" or action == "":
        random_number = random()
        num_one_twenty = (random_number * 20) + 1
        round_num = int(num_one_twenty)
        if round_num == 20:
            random_damage = random()
            damage = (random_damage * damage_die) + 1
            crit_damage = int(damage) + crit
            enemy_health = enemy_health - crit_damage
            print("You rolled a 20. That's a critical hit!", name, "did", crit_damage, "damage.")
        elif round_num == 1:
            print(name, "rolled a 1. You left yourself open for the enemy to get an additional attack that can't be parried!")
            random_number = random()
            num_one_twenty = (random_number * 20) + 1
            round_num = int(num_one_twenty)
            if round_num >= character_armor:
                random_damage = random()
                num_one_ten = (random_damage * 10) + 1
                round_damage = int(num_one_ten)
                character_health = character_health - round_damage
                print("The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
            else:
                print("The Gladiator misses", name)
        elif round_num >= enemy_armor:
            random_damage = random()
            damage = (random_damage * damage_die) + 1
            round_damage = int(damage)
            enemy_health = enemy_health - round_damage
            print("You rolled a", round_num, ". That's a hit!", name, "did", round_damage, "damage.")
        else:
            print("You rolled a", round_num, ". You missed!")
    elif action == "heal" or action == "Heal":
        if health_potion > 0:
            random_healing = random()
            first_num_one_four = (random_healing * 4) + 1
            second_num_one_four = (random_healing * 4) + 1
            round_healing = int(first_num_one_four) + int(second_num_one_four) + 2
            character_health += round_healing
            health_potion = health_potion - 1
            print("You healed", round_healing, ".", name, "has a total health of", character_health, ". Your total Health Potions are", health_potion)
        elif health_potion == 0:
            print("You have used all of your Healing Potions")
            continue
    elif action == "Counter" or action == "counter":
        random_number = random()
        num_one_twenty = (random_number * 20) + 1
        round_num = int(num_one_twenty)
        if round_num < character_armor:
            random_damage = random()
            damage = (random_damage * damage_die) + 1
            crit_damage = int(damage) + crit
            enemy_health = enemy_health - crit_damage
            print("The enemy rolled a", round_num, ".", name, "successfully countered the enemy attack! You did", crit_damage, "damage.")
            if enemy_health <= 0:
                print()
                print(name, title, "has killed the Gladiator!")
                break
            else:
                continue
        else:
            random_damage = random()
            num_one_ten = (random_damage * 10) + 1
            round_damage = int(num_one_ten) + crit
            character_health = character_health - round_damage
            print("Failed to counter. The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
            if character_health <= 0:
                print()
                print(name, title, "DIED!!!! Game Over.")
                exit()
            else:
                continue
    elif action == "spell" or action == "Spell":
        if spell > 0:
            random_number = random()
            num_one_twenty = (random_number * 20) + 1
            round_num = int(num_one_twenty)
            if round_num >= enemy_armor:
                random_damage = random()
                damage = (random_damage * damage_die) + 1
                spell_damage = int(damage) * 2
                enemy_health = enemy_health - int(spell_damage)
                spell = spell - 1
                print("Your spell hits, causing the enemy to stagger.", name, "did", int(spell_damage), "damage. You have", spell, "spells left.")
                if enemy_health <= 0:
                    print()
                    print(name, title, "has killed the Gladiator!")
                    break
                else:
                    continue
            else:
                spell = spell - 1
                print(name, "your spell missed. You have", spell, "spells left.")
        elif spell == 0:
            print("You have no spells.")
            continue
    elif action == "Status" or action == "status":
        print("AC is", character_armor, ", current Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        continue
    else:
        continue
    
    if enemy_health <= 0:
        print()
        print(name, title,"has killed the Gladiator!")
        break
    else:
        pass

    print("The Gladiator swings at", name, "!")

    random_number = random()
    num_one_twenty = (random_number * 20) + 1
    round_num = int(num_one_twenty)
    if round_num >= character_armor:
        if parry > 0:
            defense = input("The Gladiator is going to hit you, will you parry? Yes or No?")
            if defense == "yes" or defense == "Yes":
                random_number = random()
                num_one_twenty = (random_number * 20) + 1
                round_num = int(num_one_twenty)
                if round_num > 10:
                    random_damage = random()
                    damage = (random_damage * damage_die) + 1
                    parry_damage = (int(damage) / 2) + 1
                    enemy_health = enemy_health - round(parry_damage)
                    parry = parry - 1
                    print("You successfully parried the attack.", name, "did", round(parry_damage), "damage. You have", parry, "parries left.")
                    if enemy_health <= 0:
                        print()
                        print(name, title, "has killed the Gladiator!")
                        print()
                        break
                    else:
                        continue
                else:
                    print(name, "failed to parry. You still have", parry, "parries left.")
                    random_damage = random()
                    num_one_ten = (random_damage * 10) + 1
                    round_damage = int(num_one_ten)
                    character_health = character_health - round_damage
                    print("The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
            elif defense == "no" or defense == "No":
                random_damage = random()
                num_one_ten = (random_damage * 10) + 1
                round_damage = int(num_one_ten)
                character_health = character_health - round_damage
                print("The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
            else:
                random_damage = random()
                num_one_ten = (random_damage * 10) + 1
                round_damage = int(num_one_ten)
                character_health = character_health - round_damage
                print(name, "tried to parry but must have misstepped.")
                print("The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
        else:
            random_damage = random()
            num_one_ten = (random_damage * 10) + 1
            round_damage = int(num_one_ten)
            character_health = character_health - round_damage
            print("The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
    else:
        print("The Gladiator misses", name)

    if character_health <= 0:
        print()
        print(name, title, "DIED!!!! Game Over.")
    else:
        pass

for prepare in range (5):
    print()
    ready_two = input("Are you prepared to continue? Yes or No?")
    if ready_two == "yes" or ready_two == "Yes":
        print("Another gladiator has entered the arena! They look tougher than the last one", name, ", prepare for combat!")
        break
    elif ready_two == "no" or ready_two == "No":
        print("One kill isn't that bad,", name, title, ", just disappointing")
        exit()
    elif ready_two == "Status" or ready_two == "status":
        print("AC is", character_armor, ", current Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        continue
    else:
        print("Can't decide what to do? Don't worry,", name, ", a decision will be made for you...")
        print("A gladiator has entered the arena,", name, ", prepare for combat!")
        break

second_enemy_health = 25
second_enemy_armor = 12

for battle in range(100):
    print()
    action = input("Will you Attack, Heal, Counter, or cast a Spell?(Use enter/return to attack)")
    if action == "attack" or action == "":
        random_number = random()
        num_one_twenty = (random_number * 20) + 1
        round_num = int(num_one_twenty)
        if round_num == 20:
            random_damage = random()
            damage = (random_damage * damage_die) + 1
            crit_damage = int(damage) + crit
            second_enemy_health = second_enemy_health - crit_damage
            print("You rolled a 20. That's a critical hit!", name, "did", crit_damage, "damage.")
        elif round_num == 1:
            print(name, "rolled a 1. You left yourself open for the enemy to get an additional attack that can't be parried!")
            random_number = random()
            num_one_twenty = (random_number * 20) + 1
            round_num = int(num_one_twenty)
            if round_num >= character_armor:
                random_damage = random()
                num_one_ten = (random_damage * 10) + 1
                round_damage = int(num_one_ten)
                character_health = character_health - round_damage
                print("The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
            else:
                print("The Gladiator misses", name)
        elif round_num >= second_enemy_armor:
            random_damage = random()
            damage = (random_damage * damage_die) + 1
            round_damage = int(damage)
            second_enemy_health = second_enemy_health - round_damage
            print("You rolled a", round_num, ". That's a hit!", name, "did", round_damage, "damage.")
        else:
            print("You rolled a", round_num, ". You missed!")
    elif action == "heal" or action == "Heal":
        if health_potion > 0:
            random_healing = random()
            first_num_one_four = (random_healing * 4) + 1
            second_num_one_four = (random_healing * 4) + 1
            round_healing = int(first_num_one_four) + int(second_num_one_four) + 2
            character_health += round_healing
            health_potion = health_potion - 1
            print("You healed", round_healing, ".", name, "has a total health of", character_health, ". Your total Health Potions are", health_potion)
        elif health_potion == 0:
            print("You have used all of your Healing Potions")
            continue
    elif action == "Counter" or action == "counter":
        random_number = random()
        num_one_twenty = (random_number * 20) + 1
        round_num = int(num_one_twenty) + 3
        if round_num < character_armor:
            random_damage = random()
            damage = (random_damage * damage_die) + 1
            crit_damage = int(damage) + crit
            second_enemy_health = second_enemy_health - crit_damage
            print("The enemy rolled a", round_num, ".", name, "successfully countered the enemy attack! You did", crit_damage, "damage.")
            if second_enemy_health <= 0:
                print()
                print(name, title, "has killed the Gladiator!")
                break
            else:
                continue
        else:
            random_damage = random()
            num_one_ten = (random_damage * 10) + 1
            round_damage = int(num_one_ten) + crit
            character_health = character_health - round_damage
            print("Failed to counter. The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
            if character_health <= 0:
                print()
                print(name, title, "DIED!!!! Game Over.")
                exit()
            else:
                continue
    elif action == "spell" or action == "Spell":
        if spell > 0:
            random_number = random()
            num_one_twenty = (random_number * 20) + 1
            round_num = int(num_one_twenty)
            if round_num >= enemy_armor:
                random_damage = random()
                damage = (random_damage * damage_die) + 1
                spell_damage = int(damage) * 2
                second_enemy_health = second_enemy_health - int(spell_damage)
                spell = spell - 1
                print("Your spell hits, causing the enemy to stagger.", name, "did", int(spell_damage), "damage. You have", spell, "spells left.")
                if second_enemy_health <= 0:
                    print()
                    print(name, title, "has killed the Gladiator!")
                    break
                else:
                    continue
            else:
                spell = spell - 1
                print(name, "your spell missed. You have", spell, "spells left.")
        elif spell == 0:
            print("You have no spells.")
            continue
    elif action == "Status" or action == "status":
        print("AC is", character_armor, ", current Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        continue
    else:
        continue
    
    if second_enemy_health <= 0:
        print()
        print(name, title, "has killed the Gladiator!")
        print()
        break

    print("The Gladiator swings at", name, "!")

    random_number = random()
    num_one_twenty = (random_number * 20) + 1
    round_num = int(num_one_twenty) + 3
    if round_num >= character_armor:
        if parry > 0:
            defense = input("The Gladiator is going to hit you, will you parry? Yes or No?")
            if defense == "yes" or defense == "Yes":
                random_number = random()
                num_one_twenty = (random_number * 20) + 1
                round_num = int(num_one_twenty)
                if round_num > 10:
                    random_damage = random()
                    damage = (random_damage * damage_die) + 1
                    parry_damage = (int(damage) / 2) + 1
                    second_enemy_health = second_enemy_health - round(parry_damage)
                    parry = parry - 1
                    print("You successfully parried the attack.", name, "did", round(parry_damage), "damage. You have", parry, "parries left.")
                    if second_enemy_health <= 0:
                        print()
                        print(name, title, "has killed the Gladiator!")
                        print()
                        break
                    else:
                        continue
                else:
                    print(name, "failed to parry. You still have", parry, "parries left.")
                    random_damage = random()
                    num_one_ten = (random_damage * 10) + 1
                    round_damage = int(num_one_ten)
                    character_health = character_health - round_damage
                    print("The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
            elif defense == "no" or defense == "No":
                random_damage = random()
                num_one_ten = (random_damage * 10) + 1
                round_damage = int(num_one_ten)
                character_health = character_health - round_damage
                print("The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
            else:
                random_damage = random()
                num_one_ten = (random_damage * 10) + 1
                round_damage = int(num_one_ten)
                character_health = character_health - round_damage
                print(name, "tried to parry but must of misstepped.")
                print("The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
        else:
            random_damage = random()
            num_one_ten = (random_damage * 10) + 1
            round_damage = int(num_one_ten)
            character_health = character_health - round_damage
            print("The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
    else:
        print("The Gladiator misses", name)

    if character_health <= 0:
        print()
        print(name, title, "DIED!!!! Game Over.")
        exit()
    else:
        pass

for prepare in range(5):
    print()
    ready_three = input("One more fighter, are you ready? Yes or No?")
    if ready_three == "yes" or ready_three == "Yes":
        print()
        print("The Champion has entered the arena,", name, ", prepare for combat!")
        break
    elif ready_three == "no" or ready_three == "No":
        print()
        print("Honorable kills,", name, title, ", just not enough to achieve true glory!")
        exit()
    elif ready_three == "Status" or ready_three == "status":
        print("AC is", character_armor, ", current Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        continue
    else:
        print()
        print("Can't decide what to do? Don't worry,", name, ", a decision will be made for you...")
        print("The Champion has entered the arena,", name, ", prepare for combat!")
        break

third_enemy_health = 35
third_enemy_armor = 15

for battle in range(100):
    print()
    action = input("Will you Attack, Heal, Counter, or cast a Spell?(Use enter/return to attack)")
    if action == "attack" or action == "":
        random_number = random()
        num_one_twenty = (random_number * 20) + 1
        round_num = int(num_one_twenty)
        if round_num == 20:
            random_damage = random()
            damage = (random_damage * damage_die) + 1
            crit_damage = int(damage) + crit
            third_enemy_health = third_enemy_health - crit_damage
            print("You rolled a 20. That's a critical hit!", name, "did", crit_damage, "damage.")
        elif round_num == 1:
            print(name, "rolled a 1. You left yourself open for the enemy to get an additional attack that can't be parried!")
            random_number = random()
            num_one_twenty = (random_number * 20) + 1
            round_num = int(num_one_twenty)
            if round_num >= character_armor:
                random_damage = random()
                num_one_ten = (random_damage * 10) + 1
                round_damage = int(num_one_ten)
                character_health = character_health - round_damage
                print("The Champion hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
            else:
                print("The Champion misses", name)
        elif round_num >= third_enemy_armor:
            random_damage = random()
            damage = (random_damage * damage_die) + 1
            round_damage = int(damage)
            third_enemy_health = third_enemy_health - round_damage
            print("You rolled a", round_num, ". That's a hit!", name, "did", round_damage, "damage.")
        else:
            print("You rolled a", round_num, ". You missed!")
    elif action == "heal" or action == "Heal":
        if health_potion > 0:
            random_healing = random()
            first_num_one_four = (random_healing * 4) + 1
            second_num_one_four = (random_healing * 4) + 1
            round_healing = int(first_num_one_four) + int(second_num_one_four) + 2
            character_health += round_healing
            health_potion = health_potion - 1
            print("You healed", round_healing, ".", name, "has a total health of", character_health, ". Your total Health Potions are", health_potion)
        elif health_potion == 0:
            print("You have used all of your Healing Potions")
            continue
    elif action == "Counter" or action == "counter":
        random_number = random()
        num_one_twenty = (random_number * 20) + 1
        round_num = int(num_one_twenty) + 5
        if round_num < character_armor:
            random_damage = random()
            damage = (random_damage * damage_die) + 1
            crit_damage = int(damage) + crit
            third_enemy_health = third_enemy_health - crit_damage
            print("The enemy rolled a", round_num, ".", name, "successfully countered the enemy attack! You did", crit_damage, "damage.")
            if third_enemy_health <= 0:
                print()
                print(name, title, "has killed the Champion!")
                break
            else:
                continue
        else:
            random_damage = random()
            num_one_ten = (random_damage * 10) + 1
            round_damage = int(num_one_ten) + crit
            character_health = character_health - round_damage
            print("Failed to counter. The Champion hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
            if character_health <= 0:
                print()
                print(name, title, "DIED!!!! Game Over.")
                exit()
            else:
                continue
    elif action == "spell" or action == "Spell":
        if spell > 0:
            random_number = random()
            num_one_twenty = (random_number * 20) + 1
            round_num = int(num_one_twenty)
            if round_num >= enemy_armor:
                random_damage = random()
                damage = (random_damage * damage_die) + 1
                spell_damage = int(damage) * 2
                third_enemy_health = third_enemy_health - int(spell_damage)
                spell = spell - 1
                print("Your spell hits, causing the enemy to stagger.", name, "did", int(spell_damage), "damage. You have", spell, "spells left.")
                if third_enemy_health <= 0:
                    print()
                    print(name, title, "has killed the Champion! You are now the Champion of the Arena, congratulations!")
                    break
                else:
                    continue
            else:
                spell = spell - 1
                print(name, "your spell missed. You have", spell, "spells left.")
        elif spell == 0:
            print("You have no spells.")
            continue
    elif action == "Status" or action == "status":
        print("AC is", character_armor, ", current Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        continue
    else: 
        continue
    
    if third_enemy_health <= 0:
        print()
        print(name, title, "has killed the Champion! You are now the Champion of the Arena, congratulations!")
        break

    print("The Champion swings at", name, "!")

    random_number = random()
    num_one_twenty = (random_number * 20) + 1
    round_num = int(num_one_twenty) + 5
    if round_num >= character_armor:
        if parry > 0:
            defense = input("The Champion is going to hit you, will you parry? Yes or No?")
            if defense == "yes" or defense == "Yes":
                random_number = random()
                num_one_twenty = (random_number * 20) + 1
                round_num = int(num_one_twenty)
                if round_num > 10:
                    random_damage = random()
                    damage = (random_damage * damage_die) + 1
                    parry_damage = (int(damage) / 2) + 1
                    third_enemy_health = third_enemy_health - round(parry_damage)
                    parry = parry - 1
                    print("You successfully parried the attack.", name, "did", round(parry_damage), "damage. You have", parry, "parries left.")
                    if third_enemy_health <= 0:
                        print()
                        print(name, title, "has killed the Champion! You are now the Champion of the Arena, congratulations!")
                        break
                    else:
                        continue
                else:
                    print(name, "failed to parry. You still have", parry, "parries left.")
                    random_damage = random()
                    num_one_ten = (random_damage * 10) + 1
                    round_damage = int(num_one_ten)
                    character_health = character_health - round_damage
                    print("The Champion hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
            elif defense == "no" or defense == "No":
                random_damage = random()
                num_one_ten = (random_damage * 10) + 1
                round_damage = int(num_one_ten)
                character_health = character_health - round_damage
                print("The Champion hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
            else:
                random_damage = random()
                num_one_ten = (random_damage * 10) + 1
                round_damage = int(num_one_ten)
                character_health = character_health - round_damage
                print(name, "tried to parry but must of had a misstep.")
                print("The Champion hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
        else:
            random_damage = random()
            num_one_ten = (random_damage * 10) + 1
            round_damage = int(num_one_ten)
            character_health = character_health - round_damage
            print("The Champion hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
    else:
        print("The Champion misses", name)

    if character_health <= 0:
        print()
        print(name, title, "DIED!!!! Game Over.")
        exit()
    else:
        pass

print()
print("But wait...the Master of the Arena does not approve. They have ordered your execution!")

for choice in range(10):
    print()
    decision = input("Do you Run for the exit or Fight?")
    if decision == "run" or decision == "Run":
        print()
        print("You have successfully dodged the arrows. But the gate has closed before you could exit!")
        break
    elif decision == "fight" or decision == "Fight":
        random_number = random()
        num_one_twenty = (random_number * 20) + 1
        round_num = int(num_one_twenty) + 4
        if round_num >= character_armor:
            random_damage = random()
            num_one_four = (random_damage * 4) + 1
            round_damage = int(num_one_four)
            character_health = character_health - round_damage
            if character_health > 0:
                print()
                print(name, "was hit with an arrow! Your health is now at", character_health, ". The archers prepare another volley.")
                continue
            else:
                print()
                print(name, title, "was hit with an arrow! You DIED!!! Game Over")
                exit()
        else:
            print()
            print("You dodged all of the archers arrows. They prepare another volley.")
    elif decision == "Status" or decision == "status":
        print("AC is", character_armor, ", current Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        continue
    else:
        continue

gate_health = 10

for choice in range(20):
    print()
    print("As arrows fly past", name, ", you try to get passed the gate.")
    print()
    decision_two = input("Do you Open the gate or Break it down.(use enter or return to use break)")
    if decision_two == "Open" or decision_two == "open":
        random_number = random()
        num_one_twenty = (random_number * 20) + 1
        round_num = int(num_one_twenty)
        if round_num >= 18:
            print()
            print("Someone closed the gate but never locked it. The gate opens!")
            break
        else:
            random_damage = random()
            num_one_four = (random_damage * 4) + 1
            round_damage = int(num_one_four)
            character_health = character_health - round_damage
            print()
            print("It won't budge. Try something else", name, ". You were hit by an arrow, your health is at", character_health, ".")
            if character_health > 0:
                continue
            else:
                print()
                print(name, title, "DIED!!! Game Over")
                exit()
    elif decision_two == "" or decision_two == "break":
        random_number = random()
        num_one_twenty = (random_number * 20) + 1
        round_num = int(num_one_twenty)
        if round_num > 5:
            random_damage = random()
            damage = (random_damage * damage_die) + 1
            round_damage = int(damage)
            gate_health = gate_health - round_damage
            if gate_health <= 0:
                print()
                print("The gate is destroyed!", name, "has escaped the arena floor")
                break
            else:
                print()
                print("The gate looks to be damaged. Keep trying to Break the gate!")
                continue
        elif round_num <= 5:
            random_number = random()
            num_one_twenty = (random_number * 20) + 1
            round_num = int(num_one_twenty)
            if round_num >= character_armor:
                random_damage = random()
                num_one_four = (random_damage * 4) + 1
                round_damage = int(num_one_four)
                character_health = character_health - round_damage
                print()
                print(name, "was hit by an arrow when trying to break down the door. your health is at", character_health, ".")
                if character_health > 0:
                    continue
                else:
                    print()
                    print(name, title, "DIED!!! Game Over")
                    exit()
            else:
                print()
                print("You have to swing harder than that. Keep trying!")
                continue
    elif decision_two == "Status" or decision_two == "status":
        print("AC is", character_armor, ", current Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        continue

print()
decision_three = input("As you run down the hallway you see three doors, which do you take? Left, Right, or Center?")
if decision_three == "Center" or decision_three == "center":
    print()
    print("You just walked into a room covered in bones and blood. In front of", name, "is a bloodthristy lion.")
    a_center_decision = input("Do you Stand still, Run, or Fight?")
    if a_center_decision == "Run" or a_center_decision == "run":
        print("The Lion chases and pounces on", name, ". The Lion bites your neck, killing you, and then devours you.")
        print(name, title, "DIED!!! Game Over.")
        exit()
    elif a_center_decision == "Stand" or a_center_decision == "stand":
        print("The Lion rushes past", name, ", you can hear the guards from the left screaming. You take the right door to avoid the lion.")
    elif a_center_decision == "Fight" or a_center_decision == "fight":
        lion_health = 30
        lion_armor = 11
        for lion_combat in range(100):
            print()
            center_decision = input("Will you Attack, Heal, Counter, or cast a Spell?(press enter/return for attack)")
            if center_decision == "" or center_decision == "attack":
                random_number = random()
                num_one_twenty = (random_number * 20) + 1
                round_num = int(num_one_twenty)
                if round_num == 20:
                    random_damage = random()
                    damage = (random_damage * damage_die) + 1
                    crit_damage = int(damage) + crit
                    lion_health = lion_health - crit_damage
                    print("You rolled a 20. That's a critical hit!", name, "did", crit_damage, "damage.")
                elif round_num == 1:
                    print(name, "rolled a 1. You left yourself open for the enemy to get an additional attack that can't be parried!")
                    random_number = random()
                    num_one_twenty = (random_number * 20) + 1
                    round_num = int(num_one_twenty)
                    if round_num >= character_armor:
                        random_damage = random()
                        num_one_ten = (random_damage * 10) + 1
                        round_damage = int(num_one_ten)
                        character_health = character_health - round_damage
                        print("The Lion hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
                    else:
                        print("The Lion misses", name)
                elif round_num >= lion_armor:
                    random_damage = random()
                    damage = (random_damage * damage_die) + 1
                    round_damage = int(damage)
                    lion_health = lion_health - round_damage
                    print("You rolled a", round_num, ". That's a hit!", name, "did", round_damage, "damage.")
                else:
                    print("You rolled a", round_num, ". You missed!")
            elif center_decision == "heal" or center_decision == "Heal":
                if health_potion > 0:
                    random_healing = random()
                    first_num_one_four = (random_healing * 4) + 1
                    second_num_one_four = (random_healing * 4) + 1
                    round_healing = int(first_num_one_four) + int(second_num_one_four) + 2
                    character_health += round_healing
                    health_potion = health_potion - 1
                    print("You healed", round_healing, ".", name, "has a total health of", character_health, ". Your total Health Potions are", health_potion)
                elif health_potion == 0:
                    print("You have used all of your Healing Potions")
                    continue
            elif center_decision == "Counter" or center_decision == "counter":
                random_number = random()
                num_one_twenty = (random_number * 20) + 1
                round_num = int(num_one_twenty) + 2
                if round_num < character_armor:
                    random_damage = random()
                    damage = (random_damage * damage_die) + 1
                    crit_damage = int(damage) + crit
                    lion_health = lion_health - crit_damage
                    print("The enemy rolled a", round_num, ".", name, "successfully countered the enemy attack! You did", crit_damage, "damage.")
                    if lion_health <= 0:
                        print()
                        print(name, title, "has killed the Lion!")
                        break
                    else:
                        continue
                else:
                    random_damage = random()
                    num_one_ten = (random_damage * 10) + 1
                    round_damage = int(num_one_ten) + crit
                    character_health = character_health - round_damage
                    print("Failed to counter. The Lion hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
                    if character_health <= 0:
                        print()
                        print(name, title, "DIED!!!! Game Over.")
                        exit()
                    else:
                        continue
            elif center_decision == "spell" or center_decision == "Spell":
                if spell > 0:
                    random_number = random()
                    num_one_twenty = (random_number * 20) + 1
                    round_num = int(num_one_twenty)
                    if round_num >= lion_armor:
                        random_damage = random()
                        damage = (random_damage * damage_die) + 1
                        spell_damage = int(damage) * 2
                        lion_health = lion_health - int(spell_damage)
                        spell = spell - 1
                        print("Your spell hits, causing the enemy to stagger.", name, "did", int(spell_damage), "damage. You have", spell, "spells left.")
                        if lion_health <= 0:
                            print()
                            print(name, title, "has killed the Lion! You hear guards coming from the left passage. So you take the right door!")
                            break
                        else:
                            continue
                    else:
                        spell = spell - 1
                        print(name, "your spell missed. You have", spell, "spells left.")
                elif spell == 0:
                    print("You have no spells.")
                    continue
            elif center_decision == "Status" or center_decision == "status":
                print("AC is", character_armor, ", current Health is", character_health, ", and max damage is", damage_die, ".") 
                print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
                continue

            if lion_health <= 0:
                print()
                print(name, title, "has killed the Lion! You hear guards coming from the left passage. So you take the right door!")
                break

            print("The Lion claws at", name, "!")
            random_number = random()
            num_one_twenty = (random_number * 20) + 1
            round_num = int(num_one_twenty) + 2
            if round_num >= character_armor:
                if parry > 0:
                    defense = input("The Lion is going to hit you, will you parry? Yes or No?")
                    if defense == "yes" or defense == "Yes":
                        random_number = random()
                        num_one_twenty = (random_number * 20) + 1
                        round_num = int(num_one_twenty)
                        if round_num > 10:
                            random_damage = random()
                            damage = (random_damage * damage_die) + 1
                            parry_damage = (int(damage) / 2) + 1
                            lion_health = lion_health - round(parry_damage)
                            parry = parry - 1
                            print("You successfully parried the attack.", name, "did", round(parry_damage), "damage. You have", parry, "parries left.")
                            if lion_health <= 0:
                                print()
                                print(name, title, "has killed the Gladiator!")
                                print()
                                break
                            else:
                                continue
                        else:
                            print(name, "failed to parry. You still have", parry, "parries left.")
                            random_damage = random()
                            num_one_ten = (random_damage * 10) + 1
                            round_damage = int(num_one_ten)
                            character_health = character_health - round_damage
                            print("The Lion hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
                    elif defense == "no" or defense == "No":
                        random_damage = random()
                        num_one_ten = (random_damage * 10) + 1
                        round_damage = int(num_one_ten)
                        character_health = character_health - round_damage
                        print("The Lion hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
                    else:
                        random_damage = random()
                        num_one_ten = (random_damage * 10) + 1
                        round_damage = int(num_one_ten)
                        character_health = character_health - round_damage
                        print(name, "tried to parry but must of had a misstep.")
                        print("The Lion hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)        
                else:
                    random_damage = random()
                    num_one_ten = (random_damage * 10) + 1
                    round_damage = int(num_one_ten)
                    character_health = character_health - round_damage
                    print("The Lion hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
            else:
                print("The Lion misses", name)

            if character_health <= 0:
                print()
                print(name, title, "DIED!!!! Game Over.")
                exit()
            else:
                pass
elif decision_three == "Right" or decision_three == "right":
    print("You keep running down the hallway.")
elif decision_three == "Left" or decision_three == "left":
    print()
    print(name, "has exited the arena. But some part of you wants vengence against the Master of the Arena")
    decision_three_left = input("Do you Escape with your life or Return to the arena to kill the master?")
    if decision_three_left == "Escape" or decision_three_left == "escape":
        print()
        decision_three_left_final = input("Do you continue on Foot or Steal a nearby horse?")
        if decision_three_left_final == "Foot" or decision_three_left_final == "foot":
            print()
            print("The guards catch up to you, surround you, and kill you.")
            print(name, title, "is still a champion...just a dead one as well. Game Over.")
            exit()
        elif decision_three_left_final == "Steal" or decision_three_left_final == "steal":
            print()
            print(name, "swiftly passes the guards and leaves the city!")
            print("You are still a champion, but you can never return to the arena and only seek glory in other locations.")
            print()
            print("Conratulations,", name, title, ". You survived the Arena!!!")
            exit()
    elif decision_three_left == "Return" or decision_three_left == "return":
        print()
        print(name, title, "desires Vengence! And you shall have it or die!")
    else:
        print()
        print(name, title, "thought long and hard about what to do. However, the guards surrounded you while you did it.", name, "was then executed.")
        print(name, title, "is still a champion...just a dead one as well. Game Over.")
        exit()
else:
    print()
    print("You tripped and fell on your sword. You bleed out and die.")
    print(name, title, "are still a champion...just a dead one as well. Game Over.")
    exit()

guards_health = 50
guards_armor = 13

print()
print("You keep running forward until you get too the Master of the Arena's balcony door.")
for choice in range(100):
    if guards_health <= 0:
        break
    else:
        pass
    print()
    decision_four = input("There are two guards waiting at the door. Do you Intimidate them to leave or Fight?")
    if decision_four == "Intimidate" or decision_four == "intimidate":
        random_number = random()
        num_one_twenty = (random_number * 20) + 1
        round_num = int(num_one_twenty)
        if round_num > 15:
            print("The Guards run away cowering, leaving the door unguarded for", name, ".")
            break
        else:
            print("The guards are not afraid of you.")
            random_number = random()
            num_one_twenty = (random_number * 20) + 1
            round_num = int(num_one_twenty) + 4
            if round_num >= character_armor:
                random_damage = random()
                num_one_ten = (random_damage * 10) + 1
                round_damage = int(num_one_ten) * 2
                character_health = character_health - round_damage
                print("The Guards hit you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
                if character_health <= 0:
                    print()
                    print(name, title, "DIED!!!! Game Over.")
                    exit()
                else:
                    continue
            else:
                print("The Guards missed", name)
                continue
    elif decision_four == "Status" or decision_four == "status":
        print("AC is", character_armor, ", current Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        continue
    elif decision_four == "Fight" or decision_four == "fight":
        for fight in range(100):
            print()
            action = input("Will you Attack, Heal, Counter, or cast a Spell?(Use enter/return to attack)")
            if action == "attack" or action == "":
                random_number = random()
                num_one_twenty = (random_number * 20) + 1
                round_num = int(num_one_twenty)
                if round_num == 20:
                    random_damage = random()
                    damage = (random_damage * damage_die) + 1
                    crit_damage = int(damage) + crit
                    guards_health = guards_health - crit_damage
                    print("You rolled a 20. That's a critical hit!", name, "did", crit_damage, "damage.")
                elif round_num == 1:
                    print(name, "rolled a 1. You left yourself open for the enemy to get an additional attack that can't be parried!")
                    random_number = random()
                    num_one_twenty = (random_number * 20) + 1
                    round_num = int(num_one_twenty)
                    if round_num >= character_armor:
                        random_damage = random()
                        num_one_ten = (random_damage * 10) + 1
                        round_damage = int(num_one_ten)
                        character_health = character_health - round_damage
                        print("The Guards hit you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
                    else:
                        print("The Guards missed", name)
                elif round_num >= guards_armor:
                    random_damage = random()
                    damage = (random_damage * damage_die) + 1
                    round_damage = int(damage)
                    guards_health = guards_health - round_damage
                    print("You rolled a", round_num, ". That's a hit!", name, "did", round_damage, "damage.")
                else:
                    print("You rolled a", round_num, ". You missed!")
            elif action == "heal" or action == "Heal":
                if health_potion > 0:
                    random_healing = random()
                    first_num_one_four = (random_healing * 4) + 1
                    second_num_one_four = (random_healing * 4) + 1
                    round_healing = int(first_num_one_four) + int(second_num_one_four) + 2
                    character_health += round_healing
                    health_potion = health_potion - 1
                    print("You healed", round_healing, ".", name, "has a total health of", character_health, ". Your total Health Potions are", health_potion)
                elif health_potion == 0:
                    print("You have used all of your Healing Potions")
                    continue
            elif action == "Counter" or action == "counter":
                random_number = random()
                num_one_twenty = (random_number * 20) + 1
                round_num = int(num_one_twenty) + 4
                if round_num < character_armor:
                    random_damage = random()
                    damage = (random_damage * damage_die) + 1
                    crit_damage = int(damage) + crit
                    guards_health = guards_health - crit_damage
                    print("The enemy rolled a", round_num, ".", name, "successfully countered the enemy attack! You did", crit_damage, "damage.")
                    if guards_health <= 0:
                        print()
                        print(name, title, "has killed the Guards!")
                        break
                    else:
                        continue
                else:
                    random_damage = random()
                    num_one_ten = (random_damage * 10) + 1
                    round_damage = (int(num_one_ten) * 2) + crit
                    character_health = character_health - round_damage
                    print("Failed to counter. The Guards hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
                    if character_health <= 0:
                        print()
                        print(name, title, "DIED!!!! Game Over.")
                        exit()
                    else:
                        continue
            elif action == "spell" or action == "Spell":
                if spell > 0:
                    random_number = random()
                    num_one_twenty = (random_number * 20) + 1
                    round_num = int(num_one_twenty)
                    if round_num >= guards_armor:
                        random_damage = random()
                        damage = (random_damage * damage_die) + 1
                        spell_damage = int(damage) * 2
                        guards_health = guards_health - int(spell_damage)
                        spell = spell - 1
                        print("Your spell hits, causing the enemy to stagger.", name, "did", int(spell_damage), "damage. You have", spell, "spells left.")
                        if guards_health <= 0:
                            print()
                            print(name, title, "has killed the Guards!")
                            break
                        else:  
                            continue
                    else:
                        spell = spell - 1
                        print(name, "your spell missed. You have", spell, "spells left.")
                elif spell == 0:
                    print("You have no spells.")
                    continue
            elif action == "Status" or action == "status":
                print("AC is", character_armor, ", current Health is", character_health, ", and max damage is", damage_die, ".") 
                print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
                continue
            else:
                continue
            
            if guards_health <= 0:
                print()
                print(name, title, "has killed the Guards!")
                break
            else:
                pass
            
            print("The guards both attack.")
            random_number = random()
            num_one_twenty = (random_number * 20) + 1
            round_num = int(num_one_twenty) + 4
            if round_num >= character_armor:
                if parry > 0:
                    defense = input("The Guards are going to hit you, will you parry? Yes or No?")
                    if defense == "yes" or defense == "Yes":
                        random_number = random()
                        num_one_twenty = (random_number * 20) + 1
                        round_num = int(num_one_twenty)
                        if round_num > 10:
                            random_damage = random()
                            damage = (random_damage * damage_die) + 1
                            parry_damage = (int(damage) / 2) + 1
                            guards_health = guards_health - round(parry_damage)
                            parry = parry - 1
                            print("You successfully parried the attack.", name, "did", round(parry_damage), "damage. You have", parry, "parries left.")
                            if guards_health <= 0:
                                print()
                                print(name, title, "has killed the Guards!")
                                print()
                                break
                            else:
                                continue
                        else:
                            print(name, "failed to parry. You still have", parry, "parries left.")
                            random_damage = random()
                            num_one_ten = (random_damage * 10) + 1
                            round_damage = int(num_one_ten) * 2
                            character_health = character_health - round_damage
                            print("The Guards hit you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
                    elif defense == "no" or defense == "No":
                        random_damage = random()
                        num_one_ten = (random_damage * 10) + 1
                        round_damage = int(num_one_ten) * 2
                        character_health = character_health - round_damage
                        print("The Guards hit you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
                    else:
                        random_damage = random()
                        num_one_ten = (random_damage * 10) + 1
                        round_damage = int(num_one_ten) * 2
                        character_health = character_health - round_damage
                        print(name, "tried to parry but must of had a misstep.")
                        print("The Guards hit you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
                else:
                    random_damage = random()
                    num_one_ten = (random_damage * 10) + 1
                    round_damage = int(num_one_ten) * 2
                    character_health = character_health - round_damage
                    print("The Guards hit you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
            else:
                print("The Guards missed", name)
        
            if character_health <= 0:
                print()
                print(name, title, "DIED!!!! Game Over.")
                exit()
            else:
                pass

print()
print("You enter the balcony and the Master of the Arena is staring at you, surprise in their eyes.")
decision_five = input("Do you Spare them or do you get Vengence?")
if decision_five == "Spare" or decision_five == "spare":
    print()
    print("As you walk away, letting go of hate, the Master of the Arena lunges towards you with a knife.")
    input("Use a Parry or cast a Spell to counter the attack!(press enter or return to see if you're successful)")
    if parry > 0 or spell > 0:
        print()
        input("You used the last of your strength to reverse the Master of the Arena's attack and send them flying off the balcony.")
        print()
        input("You look over and see the Master dead, with the crowd chearing for you!")
        print()
        print(name, "have defeated the Master of the Arena!", name, title, "is the new Master of the Arena!")
        print("Congrtulations! You have won the Arena. Now present the next champion.")
        print()
        exit()
    else:
        print("you can't parry or cast a spell.")
        print()
        print("You were so close...but the Master got the jump on you and killed", name, title, "! Game Over.")
        exit()
elif decision_five == "Vengence" or decision_five == "vengence":
    print()
    print("You have defeated the Master of the Arena!", name, title, "is the new Master of the Arena!")
    print()
    print("Congrtulations! You have won the Arena. Now present the next champion.")
    print()
    exit()