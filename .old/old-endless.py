from random import random

print()
print("Welcome to Endless Combat, where those who enter can seek glory will only find death!")

name = input("Who dares enter Endless Combat? Enter a name:")

print()
print("Welcome to the arena", name, "!") 
print("Before you enter learn about the classes and actions.(enter/return to continue)")
skip = input("If you already an experienced player, type Skip.")
if skip == "skip" or skip == "Skip":
    pass
else:
    print()
    print("A class determines your Armor Class, Health Points, Damage Die,") 
    input("amount of Health Potions, times you can Parry, and amount of Spells.")
    print()
    print("In Combat you have three actions, and two special attacks:")
    print("Actions include: Attack with damage die, use a Health Potion, or Counter an attack.")
    input("Special attacks include Spells and Parry.")
    print()
    print("A Counter is if the enemy rolls lower than your Armor Class, you hit them with extra damage.") 
    input("But if the enemy hits you, they hit with extra damage as well.")
    print()
    print("Spells are a special attack that does double damage.") 
    input("The spell must beat the enemy armor class to hit and you loose a spell each time it's used.")
    print()
    print("Parry is a special attack that allows you to deflect an incoming attack and do half damage.")
    input("The Parry is only used if successful. You get a 50/50 chance of success.")
    print()
    print("Similar to D&D, this game uses a 20 sided die to determine if you hit someone") 
    print("with an attack or spell or successfully parry. Damage is determined by damage die,")
    print("which include an 8, 10, and 12 sided die. They are different for each class.")
    print()
    print("All input options will be upper case! You don't need to capitalize your input.") 
    input("Use enter/return when instructed, just be mindful each time you press enter/return.")

for challenge in range(100):
    print()
    difficulty = input("With that said, which class will you choose? Barbarian, Soldier, Shield Master, Wizard, Sorcerer, Warlock, Paladin, Cleric, Battle Mage?")
    if difficulty == "barbarian" or difficulty == "Barbarian":
        title = "the Barbarian"
        character_armor = 13
        character_health = 35
        health_potion = 3
        damage_die = 12
        parry = 4
        spell = 0
        crit = 5
        print()
        print("With high vitality, high damage, and no armor,", name, title, "strikes fear in everyone they approach: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "soldier" or difficulty == "Soldier":
        title = "the Soldier"
        character_armor = 16
        character_health = 30
        health_potion = 4
        damage_die = 10
        parry = 3
        spell = 0
        crit = 4
        print()
        print(name, title, "has trained for battle for all their career, mastering decent strikes, health, and damage: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".")
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "shield master" or difficulty == "Shield Master":
        title = "the Shield Master"
        character_armor = 17
        character_health = 25
        health_potion = 3
        damage_die = 8
        parry = 4
        spell = 0
        crit = 3
        print()
        print("Valuing defense over vitality and damage,", name, title, "can avoid the most dangerous hits: AC is", 
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".")
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "Fey" or difficulty == "fey":
        title = "The Fey"
        character_armor = 20
        character_health = 100
        health_potion = 10
        damage_die = 20
        parry = 10
        spell = 10
        crit = 10
        print()
        print("Wait a minute,", name, title, ", well that's a little over powered?: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "Wizard" or difficulty == "wizard":
        title = "the Wizard"
        character_armor = 13
        character_health = 25
        health_potion = 4
        damage_die = 10
        parry = 0
        spell = 8
        crit = 4
        print()
        print("Studying the art of magic,", name, title, "can cast devestating damage and arcane spells: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "Sorcerer" or difficulty == "sorcerer":
        title = "the Sorcerer"
        character_armor = 14
        character_health = 30
        health_potion = 3
        damage_die = 8
        parry = 0
        spell = 6
        crit = 3
        print()
        print("Born with magic coursing through their veins,", name, title, "can use magic at their whim: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "Warlock" or difficulty == "warlock":
        title = "the Warlock"
        character_armor = 15
        character_health = 35
        health_potion = 3
        damage_die = 12
        parry = 0
        spell = 5
        crit = 5
        print()
        print("Making a pact with a magical being,", name, title, "has access to the full power of their patron: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "Cleric" or difficulty == "cleric":
        title = "the Cleric"
        character_armor = 16
        character_health = 30
        health_potion = 4
        damage_die = 8
        parry = 2
        spell = 4
        crit = 3
        print()
        print("Trained for battle, blessed by a diety,", name, title, "uses magic and a sword to fight: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "Paladin" or difficulty == "paladin":
        title = "the Paladin"
        character_armor = 17
        character_health = 25
        health_potion = 3
        damage_die = 10
        parry = 3
        spell = 3
        crit = 4
        print()
        print("On their holy quest,", name, title, "wields a mighty sword with a spell or two just in case: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    elif difficulty == "Battle Mage" or difficulty == "battle mage":
        title = "the Battle Mage"
        character_armor = 15
        character_health = 30
        health_potion = 2
        damage_die = 12
        parry = 4
        spell = 2
        crit = 5
        print()
        print("Trained in both magic and sword,", name, title, "is a force to be reckoned with: AC is",
        character_armor, ", Health is", character_health, ", and max damage is", damage_die, ".") 
        print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
        begin = input("Is this the class for you? Yes or No?")
        if begin == "yes" or begin == "Yes":
            break
        elif begin == "no" or begin == "No":
            continue
        else:
            continue
    else:
        print()
        print("You need to choose a class to continue!!!!")
        continue

print()
input("If you ever forget your character details during combat, type in Status(hit enter/return to continue)")
print()
ready = input("Are you prepared to die? Yes or No?")
if ready == "yes" or ready == "Yes":
    print()
    print("A gladiator has entered the arena,", name, ", prepare for combat!")
elif ready == "no" or ready == "No":
    print()
    print("Then you are a coward", name, "...and too bad, your fate is sealed!")
else:
    print()
    print("Can't decide what to do? Don't worry,", name, ", a decision will be made for you...")
    print("A gladiator has entered the arena,", name, ", prepare for combat!")



enemy_health = 10
enemy_armor = 4
enemy_strength = 0
counter = -1

for doom in range(100):
    if enemy_armor == 18:
        counter += 1
        enemy_strength += 0.25
    else:
        enemy_armor += 1
        counter += 1
        enemy_strength += 0.25
    if enemy_health <= 0:
        enemy_health = 10
        enemy_health = enemy_health + (5 * counter)
    print("Enemy health: ", enemy_health)
    print("Enemy armor: ", enemy_armor)
    print("Dead enemies: ", counter)
    print("Enemy strength:", int(enemy_strength))
    for battle in range(100):
        print()
        action = input("Will you Attack, Heal, Counter, or cast a Spell?(Use enter/return to attack)")
        if action == "attack" or action == "":
            random_number = random()
            num_one_twenty = (random_number * 20) + 1
            round_num = int(num_one_twenty)
            if round_num == 20:
                random_damage = random()
                damage = (random_damage * damage_die) + 1
                crit_damage = int(damage) + crit
                enemy_health = enemy_health - crit_damage
                print("You rolled a 20. That's a critical hit!", name, "did", crit_damage, "damage.")
            elif round_num == 1:
                print(name, "rolled a 1. You left yourself open for the enemy to get an additional attack that can't be parried!")
                random_number = random()
                num_one_twenty = (random_number * 20) + 1
                round_num = int(num_one_twenty) + int(enemy_strength)
                if round_num >= character_armor:
                    random_damage = random()
                    num_one_ten = (random_damage * 10) + 1
                    round_damage = int(num_one_ten)
                    character_health = character_health - round_damage
                    print("The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
                else:
                    print("The Gladiator misses", name)
            elif round_num >= enemy_armor:
                random_damage = random()
                damage = (random_damage * damage_die) + 1
                round_damage = int(damage)
                enemy_health = enemy_health - round_damage
                print("You rolled a", round_num, ". That's a hit!", name, "did", round_damage, "damage.")
            else:
                print("You rolled a", round_num, ". You missed!")
        elif action == "heal" or action == "Heal":
            if health_potion > 0:
                random_healing = random()
                first_num_one_four = (random_healing * 4) + 1
                second_num_one_four = (random_healing * 4) + 1
                round_healing = int(first_num_one_four) + int(second_num_one_four) + 2
                character_health += round_healing
                health_potion = health_potion - 1
                print("You healed", round_healing, ".", name, "has a total health of", character_health, ". Your total Health Potions are", health_potion)
            elif health_potion == 0:
                print("You have used all of your Healing Potions")
                continue
        elif action == "Counter" or action == "counter":
            random_number = random()
            num_one_twenty = (random_number * 20) + 1
            round_num = int(num_one_twenty) + int(enemy_strength)
            if round_num < character_armor:
                random_damage = random()
                damage = (random_damage * damage_die) + 1
                crit_damage = int(damage) + crit
                enemy_health = enemy_health - crit_damage
                print("The enemy rolled a", round_num, ".", name, "successfully countered the enemy attack! You did", crit_damage, "damage.")
                if enemy_health <= 0:
                    print()
                    print(name, title, "has killed the Gladiator!")
                    break
                else:
                    continue
            else:
                random_damage = random()
                num_one_ten = (random_damage * 10) + 1
                round_damage = int(num_one_ten) + crit
                character_health = character_health - round_damage
                print("Failed to counter. The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
                if character_health <= 0:
                    print()
                    print(name, "fought bravely till the end...you killed", counter, "enemies!")
                    exit()
                else:
                    continue
        elif action == "spell" or action == "Spell":
            if spell > 0:
                random_number = random()
                num_one_twenty = (random_number * 20) + 1
                round_num = int(num_one_twenty)
                if round_num >= enemy_armor:
                    random_damage = random()
                    damage = (random_damage * damage_die) + 1
                    spell_damage = int(damage) * 2
                    enemy_health = enemy_health - int(spell_damage)
                    spell = spell - 1
                    print("Your spell hits, causing the enemy to stagger.", name, "did", int(spell_damage), "damage. You have", spell, "spells left.")
                    if enemy_health <= 0:
                        print()
                        print(name, title, "has killed the Gladiator!")
                        break
                    else:
                        continue
                else:
                    spell = spell - 1
                    print(name, "your spell missed. You have", spell, "spells left.")
            elif spell == 0:
                print("You have no spells.")
                continue
        elif action == "Status" or action == "status":
            print("AC is", character_armor, ", current Health is", character_health, ", and max damage is", damage_die, ".") 
            print("You also have", health_potion, "Health Potions,", parry, "parry manuevers, and", spell, "spells.")
            continue
        else:
            continue
        
        if enemy_health <= 0:
            print()
            print(name, title,"has killed the Gladiator!")
            break
        else:
            pass

        print("The Gladiator swings at", name, "!")

        random_number = random()
        num_one_twenty = (random_number * 20) + 1
        round_num = int(num_one_twenty) + int(enemy_strength)
        if round_num >= character_armor:
            if parry > 0:
                defense = input("The Gladiator is going to hit you, will you parry? Yes or No?")
                if defense == "yes" or defense == "Yes":
                    random_number = random()
                    num_one_twenty = (random_number * 20) + 1
                    round_num = int(num_one_twenty)
                    if round_num > 10:
                        random_damage = random()
                        damage = (random_damage * damage_die) + 1
                        parry_damage = (int(damage) / 2) + 1
                        enemy_health = enemy_health - round(parry_damage)
                        parry = parry - 1
                        print("You successfully parried the attack.", name, "did", round(parry_damage), "damage. You have", parry, "parries left.")
                        if enemy_health <= 0:
                            print()
                            print(name, title, "has killed the Gladiator!")
                            print()
                            break
                        else:
                            continue
                    else:
                        print(name, "failed to parry. You still have", parry, "parries left.")
                        random_damage = random()
                        num_one_ten = (random_damage * 10) + 1
                        round_damage = int(num_one_ten)
                        character_health = character_health - round_damage
                        print("The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
                elif defense == "no" or defense == "No":
                    random_damage = random()
                    num_one_ten = (random_damage * 10) + 1
                    round_damage = int(num_one_ten)
                    character_health = character_health - round_damage
                    print("The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
                else:
                    random_damage = random()
                    num_one_ten = (random_damage * 10) + 1
                    round_damage = int(num_one_ten)
                    character_health = character_health - round_damage
                    print(name, "tried to parry but must have misstepped.")
                    print("The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
            else:
                random_damage = random()
                num_one_ten = (random_damage * 10) + 1
                round_damage = int(num_one_ten)
                character_health = character_health - round_damage
                print("The Gladiator hits you!", name, "was damaged for", round_damage, "health points, your health is", character_health)
        else:
            print("The Gladiator misses", name)

        if character_health <= 0:
            print()
            print(name, title, "DIED!!!!")
            break
        else:
            pass

    if character_health <=0:
        print()
        print(name, "fought bravely till the end...you killed", counter, "enemies!")
        print("Game Over.")
        exit()
    else:
        pass
    