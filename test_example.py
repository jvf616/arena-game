class BaseUser:
    def prompt(self, text):
        raise NotImplementedError()


class ActualUser(BaseUser):
    def prompt(self, text):
        return input(text)


class FakeUser(BaseUser):
    def __init__(self, responses):
        self.responses = responses
    
    def prompt(self, text):
        if self.responses:
            print(text)
            return self.responses.pop(0)
        else:
            return input(text)



def main():
    what_user = input("user type? ")
    user_types = {
        "base": BaseUser(),
        "actual": ActualUser(),
        "fake": FakeUser(["yes", "hello", "blah"]),
    }
    user = user_types[what_user]

    print(user.prompt("yes or no? "))

    print(user.prompt("say 'hello' "))

    print(user.prompt("say something crazy! "))

    print(user.prompt("say something else! "))


if __name__ == "__main__":
    main()
