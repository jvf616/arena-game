import sys


def end():
    print("Game ended")
    sys.exit(1)


def get_character_name():
    return input("Choose character name:")


def introduce_rules(name):
    print()
    print("Welcome to the arena", name, "!") 
    print("Before you enter learn about the classes and actions.(enter/return to continue)")
    skip = input("If you're already an experienced player, type Skip.")
    if skip == "skip" or skip == "Skip":
        pass
    else:
        print()
        print("A class determines your Armor Class, Health Points, Damage Die,") 
        input("amount of Health Potions, times you can Parry, and amount of Spells.")
        print()
        print("In Combat you have three actions, and two special attacks:")
        print("Actions include: Attack with damage die, use a Health Potion, or Counter an attack.")
        input("Special attacks include Spells and Parry.")
        print()
        print("A Counter is if the enemy rolls lower than your Armor Class, you hit them with extra damage.") 
        input("But if the enemy hits you, they hit with extra damage as well.")
        print()
        print("Spells are a special attack that does double damage.") 
        input("The spell must beat the enemy armor class to hit and you loose a spell each time it's used.")
        print()
        print("Parry is a special attack that allows you to deflect an incoming attack and do half damage.")
        input("The Parry is only used if successful. You get a 50/50 chance of success.")
        print()
        print("Similar to D&D, this game uses a 20 sided die to determine if you hit someone") 
        print("with an attack or spell or successfully parry. Damage is determined by damage die,")
        input("which include an 8, 10, and 12 sided die. They are different for each class.")
        print()
        print("All input options will be upper case! You don't need to capitalize your input.") 
        input("Use enter/return when instructed, just be mindful each time you press enter/return.")


def pick_character_class():
    # create possible character class choices
    character_classes = [
        Character(
            title="The Soldier",
            character_armor=16,
            character_health=30,
            health_potion=4,
            damage_die=10,
            parry=3,
            spell=0,
            crit=4,
        ),
        Character(
            title="The Warlock",
            character_armor=15,
            character_health=35,
            health_potion=2,
            damage_die=12,
            parry=0,
            spell=4,
            crit=5,
        )
    ]

    character_class_choices = {}
    for cls in character_classes:
        character_class_choices[cls.title] = cls

    while True:
        print("Class options are:", ", ".join(character_class_choices))
        chosen_class_title = input("Choose your class: ").title()
        if chosen_class_title not in character_class_choices:
            print("Invalid choice: ", chosen_class_title)
            continue
        chosen_class = character_class_choices[chosen_class_title]
        if not confirm_class_choice(chosen_class):
            print("Choose again")
            continue
        return chosen_class


def confirm_class_choice(chosen_class):
    # TODO: print out class information to player
    return input("Confirm choice: ").lower() == "yes"


class Character:
    def __init__(
        self,
        title,
        character_armor,
        character_health,
        health_potion,
        damage_die,
        parry,
        spell,
        crit,
    ):
        self.title = title
        self.character_armor = character_armor
        self.character_health = character_health
        self.health_potion = health_potion
        self.damage_die = damage_die
        self.parry = parry
        self.spell = spell
        self.crit = crit

    def __repr__(self):
        # return the string representation for this class
        # I'm using an f-string (makes for super easy string formatting)
        return f"Character('{self.title}')"