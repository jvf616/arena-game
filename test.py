from game.core.combat import combat
from game.core.characters import Character
from game.core.seige import Army, Catapult, seige_combat

# name = "Jack"

# player = Character(
#             name=name,
#             title="Barbarian",
#             character_armor=13,
#             character_health=35,
#             max_health=35,
#             health_potion=3,
#             damage_die=12,
#             parry=4,
#             parry_max=4,
#             spell=0,
#             spell_max=0,
#             crit=5,
#             die_modifier=0,
#             gold=0
#         )

# enemy_one = Character(
#         title="Gladiator",
#         character_armor=15,
#         character_health=50,
#         max_health=50,
#         health_potion=3,
#         damage_die=12,
#         parry=3,
#         parry_max=3,
#         spell=2,
#         spell_max=2,
#         crit=5,
#         die_modifier=3,
#         gold=0
#     )

# new_combat(name, player, enemy_one)

army = Army(
    soldiers=500,
    defense=15,
    offense=5,
    archers=20,
    mages=30,
    mage_spells=5,
    catapult_amount=3,
    catapult_ammo=1
)

drakona = Army(
    soldiers=600,
    defense=15,
    offense=5,
    archers=20,
    mages=30,
    mage_spells=5,
    catapult_amount=3,
    catapult_ammo=1
)

army_catapult_one = Catapult(
    health=50,
    strength=12,
)

army_catapult_two = Catapult(
    health=50,
    strength=12,
)

army_catapult_three = Catapult(
    health=50,
    strength=12,
)

enemy_catapult_one = Catapult(
    health=50,
    strength=12,
)

enemy_catapult_two = Catapult(
    health=50,
    strength=12,
)

enemy_catapult_three = Catapult(
    health=50,
    strength=12,
)


seige_combat(army, drakona, army_catapult_one, army_catapult_two, army_catapult_three, enemy_catapult_one, enemy_catapult_two, enemy_catapult_three)