from game.core.characters import pick_character_class
from game.core.dialogue import introduce_rules
from game.levels.arena import play as play_arena, DEAD, MASTER, VILLAGE
from game.levels.village import play as play_village, EPILOGUE, ARENA, LEADER
from game.levels.master import play as play_master, EMPEROR
from game.levels.epilogue import play as play_epilogue
from game.levels.endless import play as play_endless
from game.levels.emperor import play as play_emperor, EPILOGUE
from game.levels.leader import play as play_leader, EPILOGUE, EMPEROR, ENDLESS

introduce_rules()
player = pick_character_class()

def play(player):
    print()
    start = input("Would you like to play Endless or Story:").lower()
    if start == "endless":
        play_endless(player)
    else:
        play_story(player)


def play_story(player):
    # introduce_rules()
    # player = pick_character_class()
    while True:
        outcome = play_arena(player)

        if outcome == DEAD:
            ...  # say game over
            return
        elif outcome == VILLAGE:
            second_outcome = play_village(player)
            if second_outcome == EPILOGUE:
                play_epilogue(player)
                return
            elif second_outcome == ARENA:
                continue
            elif second_outcome == LEADER:
                third_outcome = play_leader(player)
                if third_outcome == EMPEROR:
                    fourth_outcome = play_emperor(player)
                    if fourth_outcome == EPILOGUE:
                        play_epilogue(player)
                        return
                    else:
                        return ValueError(f"play emperor returned unknown outcome {fourth_outcome}")
                elif third_outcome == EPILOGUE:
                    play_epilogue(player)
                    return
                elif third_outcome == ENDLESS:
                    play_endless(player)
            else:
                return ValueError(f"play village returned unknown outcome {second_outcome}")
        elif outcome == MASTER: 
            second_outcome = play_master(player)
            if second_outcome == EMPEROR:
                third_outcome = play_emperor(player)
                if third_outcome == EPILOGUE:
                    play_epilogue
                    return
                else:
                    return ValueError(f"play emperor returned unknown outcome {third_outcome}")
            else:
                print("something went wrong")
                return ValueError(f"play master returned unknown outcome {second_outcome}")
        else:
            raise ValueError(f"play arena returned unknown outcome {outcome}")


if __name__ == "__main__":
    play(player)