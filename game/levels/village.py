from game.core.dice import dtwenty, roll, healing_die, parry_hit

from game.core.characters import Character, pick_character_class

from game.core.combat import combat, level_up, defense

from game.core.dialogue import introduce_rules, leader_or_savior, tavern, introduce_level_two_rules

from game.core.shopping import shopping

from game.core.multiple import three_enemies

LEADER = 7
EPILOGUE = 4
ARENA = 6

def leader_or_savior(name, player, old_man):
    print()
    print("As you were fighting, you didn't notice that the villagers gathered around to watch.")
    print("As the Bandit Leader lays dead at your feat, the villagers celebrate!")
    if old_man.character_health <= 0:
        print()
        print("The tavern keeper comes up to you and says 'Thank you for getting rid of our troubles, they won't be missed.'")
        protector_or_not = input("'However, they won't be the last thug to threaten us, will you stay to protect us? Yes or No?'").lower()
        if protector_or_not == "yes" or protector_or_not == "no":
            print()
            print("Before you say anything, the pack of bandits arrive and seeing their Leader dead, one of them says")
            new_leader = input("'You killed our leader, and by our laws that means you're our leader now, do you accept? Yes or No'").lower()
            if new_leader == "yes" or new_leader == "y":
                print()
                print("The bandits cheer while the villagers are shocked!")
                print("You take your new crew of bandits and begin ransacking the village!")
                print("During your pillaging, you kill the tavern keeper and a few village citizens, stealing as much gold as possible")
                print("The Champion of the Arena has become...The Bandit Leader! Scourge of the countryside!")
                print("So concludes another chapter in your epic tale!")
                return EPILOGUE
            else:
                print()
                print(name, player.title, "says no to their laws!")
                save_the_village = input("The tavern keeper asks again 'Will you protect us?' Yes or No?").lower()
                if save_the_village == "yes" or save_the_village == "y":
                    print()
                    print("The villagers cheer yet again, and together you and them chase out the remaining bandits from the village.")
                    print("Congratulations,", name, "! You are now the Savior of the Village!")
                    return LEADER
                else:
                    print()
                    print("You owe these people nothing, so you decide to continue your journey, not knowing where it will take you...")
                    print("Unnfortunately, it took you right into the path of the Royal Guard, who arrested you and took you back to the city for execution.")
                    print("Instead of death, they decide to put you back into the arena.")
                    print("Congratulations! You are back where you started...")
                    return ARENA
        else: 
            print()
            print("Before you say anything, the pack of bandits arrive and seeing their Leader dead, one of them says")
            new_leader = input("'You killed our leader, and by our laws that means you're our leader now, do you accept? Yes or No'").lower()
            if new_leader == "yes" or new_leader == "y":
                print()
                print("The bandits cheer while the villagers are shocked!")
                print("You take your new crew of bandits and begin ransacking the village!")
                print("During your pillaging, you kill the tavern keeper and a few village citizens, stealing as much gold as possible")
                print("The Champion of the Arena has become...The Bandit Leader! Scourge of the countryside!")
                print("So concludes another chapter in your epic tale!")
                return EPILOGUE
            else:
                print()
                print(name, player.title, "says no to their laws!")
                save_the_village = input("The tavern keeper asks again 'Will you protect us?' Yes or No?").lower()
                if save_the_village == "yes" or save_the_village == "y":
                    print()
                    print("The villagers cheer yet again, and together you and them chase out the remaining bandits from the village.")
                    print("Congratulations,", name, "! You are now the Savior of the Village!")
                    print("So concludes another chapter in your epic tale!")
                    return LEADER
                else:
                    print()
                    print("You owe these people nothing, so you decide to continue your journey, not knowing where it will take you...")
                    print("Unnfortunately, it took you right into the path of the Royal Guard, who arrested you and took you back to the city for execution.")
                    print("Instead of death, they decide to put you back into the arena.")
                    print("Congratulations! You are back where you started...")
                    return ARENA
    else:
        print()
        print("The old man comes up to you and says 'Thank you for getting rid of our troubles, they surely won't be missed'")
        protector_or_not = input("'However, they won't be the last thug to threaten us, will you stay to protect us? Yes or No'").lower()
        if protector_or_not == "yes" or protector_or_not == "no":
            print()
            print("Before you say anything, the pack of bandits arrive and seeing their Leader dead, one of them says")
            new_leader = input("'You killed our leader, and by our laws that means you're our leader now, do you accept? Yes or No'").lower()
            if new_leader == "yes" or new_leader == "y":
                print()
                print("The bandits cheer while the villagers are shocked!")
                print("You take your new crew of bandits and begin ransacking the village!")
                print("During your pillaging, you kill the old man, the tavern keeper and a few village citizens, stealing as much gold as possible")
                print("The Champion of the Arena has become...The Bandit Leader! Scourge of the countryside!")
                print("So concludes another chapter in your epic tale!")
                return EPILOGUE
            else:
                print()
                print(name, player.title, "says no to their laws!")
                save_the_village = input("The old man asks again 'Will you protect us?' Yes or No?").lower()
                if save_the_village == "yes" or save_the_village == "y":
                    print()
                    print("The villagers cheer yet again, and together you and them chase out the remaining bandits from the village.")
                    print("Congratulations,", name, "! You are now the Savior of the Village!")
                    print("So concludes another chapter in your epic tale!")
                    return LEADER
                else:
                    print()
                    print("You owe these people nothing, so you decide to continue your journey, not knowing where it will take you...")
                    print("Unnfortunately, it took you right into the path of the Royal Guard, who arrested you and took you back to the city for execution.")
                    print("Instead of death, they decide to put you back into the arena.")
                    print("Congratulations! You are back where you started...")
                    return ARENA
        else: 
            print()
            print("Before you say anything, the pack of bandits arrive and seeing their Leader dead, one of them says")
            new_leader = input("'You killed our leader, and by our laws that means you're our leader now, do you accept? Yes or No'").lower()
            if new_leader == "yes" or new_leader == "y":
                print()
                print("The bandits cheer while the villagers are shocked!")
                print("You take your new crew of bandits and begin ransacking the village!")
                print("During your pillaging, you kill the old man, the tavern keeper and a few village citizens, stealing as much gold as possible")
                print("The Champion of the Arena has become...The Bandit Leader! Scourge of the countryside!")
                print("So concludes another chapter in your epic tale!")
                return EPILOGUE
            else:
                print()
                print(name, player.title, "says no to their laws!")
                save_the_village = input("The old man asks again 'Will you protect us?' Yes or No?").lower()
                if save_the_village == "yes" or save_the_village == "y":
                    print()
                    print("The villagers cheer yet again, and together you and them chase out the remaining bandits from the village.")
                    print("Congratulations,", name, "! You are now the Savior of the Village!")
                    print("So concludes another chapter in your epic tale!")
                    return LEADER
                else:
                    print()
                    print("You owe these people nothing, so you decide to continue your journey, not knowing where it will take you...")
                    print("Unnfortunately, it took you right into the path of the Royal Guard, who arrested you and took you back to the city for execution.")
                    print("Instead of death, they decide to put you back into the arena.")
                    print("Congratulations! You are back where you started...")
                    return ARENA

def play(player):
    introduce_level_two_rules(player)
    
    print()
    print("The Champion of the Arena travels through the night on horseback, leaving the Arena behind them.")
    print("They find an abandoned barn, deciding to take shelter and spend the night.")
    input("Once awake, the Champion finds that the horse vanished, and an old man looking down at them.(hit enter/return to continue)")
    print()
    
    name = player.name
    player.character_health = player.max_health
    player.parry = player.parry_max
    player.spell = player.spell_max

    print("You tell the man your name and title:", name, "the", player.title)

    print()
    print("Really? You're", name, "the", player.title, "? Aren't you the Champion of the Arena?")
    print("The royal guard came looking for you last night, good thing I didn't know you were here.")
    input("Follow me, I can help you in your journey!(hit enter/return to continue)")

    old_man = Character(
        title="Old Man",
        character_armor=5,
        character_health=10,
        max_health=10,
        health_potion=0,
        damage_die=4,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=2,
        die_modifier=0,
        gold=0,
    )

    enemy = Character(
        title="Royal Guard",
        character_armor=16,
        character_health=80,
        max_health=80,
        health_potion=0,
        damage_die=10,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=4,
        die_modifier=5,
        gold=0
    )


    while True:
        print()
        print(name, "thinks to themself...is this person really going to help them?")
        decision = input("Do you take his offer? Will you Follow or Kill him?").lower()
        if decision == "kill":
            print("'What are you doing", name, "?!?'")
            combat(name, player, old_man)
            print("As the Old Man lay dead, you see in the distance his house, and a little girl watching you kill him.")
            print()
            decision_two = input("Do you Run or do you Kill the witness:").lower()
            if decision_two == "run":
                print()
                print("You decide to not kill any more innocents, you run away, following the road away from the city.")
                break
            elif decision_two == "kill":
                print()
                print("You go up to the house and kick in the door, finding the Old Man's family")
                print("'They murdered Pa! You monster!' the little girl screams.")
                monster = input("Are you really going to kill a defenseless family?Yes or No?").lower()
                if monster == "no" or monster == "n":
                    print(name, "regains their senses and runs away, regretting their recent choice and heads North away from the city")
                    break
                elif monster == "yes" or monster == "y":
                    print()
                    print(name, "mercilessly kills the family, their screams heard throughout the valley.")
                    player.parry = player.parry_max
                    player.spell = player.spell_max
                    level_up(name, player)
                    print()
                    print("You decide after murdering the family that you should also rob them")
                    rob = input("Do you rob the dead family? Yes or No?").lower()
                    if rob == "yes" or rob == "y":
                        player.gold = roll(75) + 25
                        print()
                        print("Congratulations, you found", player.gold, "gold pieces and a health potion!")
                    elif rob == "no" or rob == "n":
                        print()
                        print("You figure you should get out of here sooner than later.")
                    else:
                        player.gold = roll(25) + 5
                        print()
                        print("You think for a moment, and realize their is a small pouch of gold and healing potion right next to the entrance on your way out.")
                        print("Congratulations, you found", player.gold, "gold pieces!")
                    print()
                    print("Unfortunately for the Champion, the screams alert the Royal Guard to their location!")
                    input("After killing the family, the Royal Guard enters the home and immediatley attack you!(hit enter/return to continue)")
                    
                    combat(name, player, enemy)
                    
                    print()
                    print("You leave the crime scene, making it look like the Royal Guard killed the family.")
                    print(name, "washes the blood of themselves in a nearby river, and drinks a healing potion they found in the house")
                    round_healing = healing_die()
                    player.character_health += round_healing
                    print("You healed", round_healing, ".", name, "has a total health of", player.character_health, ".")
                else:
                    print(name, "hesitated, shocked at what they recently did, not noticing the little girl take a knife and stab you!")
                    print("How was this possible! A little girl killed", name, "the", player.title, "DIED!!! Game Over.")
                    exit()
            break
        elif decision == "follow":
            print()
            print(name, "follows the Old Man into his home, meeting the Old Man's family.")
            print("The Old Man says 'Our village is under seige by bandits, and the Royal Guard has done nothing.'")
            print("'We need the help of a Hero, a Champion! I can help train you to be better than before.'")
            while True:
                print("'Will you,", name, "the", player.title, ", help in our time of need'?")
                level_increase = input ("Yes or No?").lower()
                if level_increase == "yes" or level_increase == "y":
                    print()
                    print(name, "spends a week training with the Old Man...")
                    level_up(name, player)
                    print()
                    parry_max = player.parry
                    spell_max = player.spell
                    player.gold += 50
                    print("The Old Man points towards the village, and gives you 50 gold pieces.")
                    break
                elif level_increase == "no" or level_increase == "n":
                    print()
                    print("The Old Man says 'That is a shame...then get off my property.'")
                    break
                else:
                    continue 
            break   
        else:
            print("'Well don't just stand there' says the Old Man, 'Follow me to my home.'")
            continue

    print()
    print("Eventually,", name, "the", player.title, "reaches a medium sized village.")
    print("The village is peaceful and busy, with the villagers all attending to their individual business.")
    print("You see many shops and vendors as well as a local tavern. You decide to visit one of these locations.")

    shopping(name, player)

    #tavern
    tavern(name, player)

    #bandits 

    enemy_one = Character(
        title="Tall Bandit",
        character_armor=8,
        character_health=15,
        max_health=15,
        health_potion=0,
        damage_die=10,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=4,
        die_modifier=0,
        gold=0
    )

    enemy_two = Character(
        title="Sinister Bandit",
        character_armor=8,
        character_health=15,
        max_health=15,
        health_potion=0,
        damage_die=10,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=4,
        die_modifier=0,
        gold=0
    )

    enemy_three = Character(
        title="Large Bandit",
        character_armor=8,
        character_health=15,
        max_health=15,
        health_potion=0,
        damage_die=10,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=4,
        die_modifier=0,
        gold=0
    )

    enemies = 3

    print()
    print("As you leave the bar, you notice three brutes walk in shoving people out of their way.")
    print("One of them gives you a sharp look, 'What are you looking at...'")
    input("'I ain't seen your pretty face here, who you supposed to be?(enter/return to continue)")
    print()
    print("You tell them who you are:", name, "the", player.title, ", Champion of the Arena!")
    print("'Well,", name, ", we here run this town beside our leader, so show us the proper respect and give us your gold.'")
    print("You realize that these are obviously bandits.")
    surrender_or_attack = input("Do you Attack them or give them your Gold?").lower()
    if surrender_or_attack == "gold":
        print()    
        if player.gold > 0:
            player.gold = 0
            print("You give them all your gold, afraid they might kill you.")
            print("You have", player.gold, "gold left.")
            print("You flee the tavern, then the village, never to return as you are a coward.")
            exit()
        else:
            print("But wait, you don't have any more gold!")
            print()
            print("'Well then,' says the Bandit, 'I guess you'll pay us in blood'")
            # multiple combat
            three_enemies(name, player, enemy_one, enemy_two, enemy_three, enemies)
    elif surrender_or_attack == "attack":
        print()
        print("You swing at them, and so begins a tavern brawl!")
        #multiple combat
        three_enemies(name, player, enemy_one, enemy_two, enemy_three, enemies)
    else:
        print()
        print("'Gold is the only language I speak, so I'm just going have to take it.'")
        #multiple combat
        three_enemies(name, player, enemy_one, enemy_two, enemy_three, enemies)
    print()
    print("The Bandits lay dead at your feet, the patrons in the tavern cheer for you")
    print("The Tavern Keeper comes up to you and says 'Thank you, those three have been a  menace for sometime.'")
    print("'Can I offer you a place to stay for the night? A meal? It's on the house!'")
    free_stay = input("Do you Stay the night or do you find Another place to stay?").lower()
    if free_stay == "stay":
        print()
        print("You eat a free meal and sleep in the nicest room.")
        print(name, "has gotten the best rest they could ask for.")
        player.character_health = player.max_health
        player.parry = player.parry_max 
        player.spell = player.spell_max
        print(name, "has recovered their max health:", player.character_health, ", max parries:", player.parry, ", and max spells:", player.spell)
    elif free_stay == "another":
        print()
        print("You reject the offer, and try to find a place to stay the night.")
        print("Unfortunately, you couldn't find a room at any inn, so you sleep under a nearby tree.")
        print("Tossing and turning,", name, "did not get the most restful sleep.")
    
    #combat
    print("In the morning, you hear someone shouting your name...")
    wake_up = input("Do wake up from your sleep? Yes or No?").lower()
    if wake_up == "no" or wake_up == "n":
        print()
        print(name, "decided to sleep in, unfortunately that was a bad idea...")
        print("The Bandit Leader doesn't take kindly to you killing their subordinates.")
        print("They slit your throat, and you slowly die. Game Over!!!")
        exit()
    else:
        bandit_leader = Character(
            title="Bandit Leader",
            character_armor=17,
            character_health=75,
            max_health=75,
            health_potion=0,
            damage_die=12,
            parry=0,
            parry_max=0,
            spell=0,
            spell_max=0,
            crit=5,
            die_modifier=5,
            gold=0
        )
        print()
        print("They were shouting, hard to stay asleep with it.")
        print("You see the source of the shouting to be a massive stranger, you go up to them.")
        print("'You kill my bandits, you answer to me!!!'")
        defense(name, player, bandit_leader)
        combat(name, player, bandit_leader)

    #resolution
    return leader_or_savior(name, player, old_man)
