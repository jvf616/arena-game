from game.core.combat import combat

from game.core.characters import Character, pick_character_class

from game.core.dialogue import introduce_rules, before_the_fight

from game.core.obstacles import leave_the_arena, the_balcony_door

DEAD = 1
VILLAGE = 2
MASTER = 3

def left_right_center(name, player):
    print()
    decision = input("As you run down the hallway you see three doors, which do you take? Left, Right, or Center?").lower()
    if decision == "center":
        print()
        print("You just walked into a room covered in bones and blood. In front of", name, "is a bloodthristy lion.")
        a_center_decision = input("Do you Stand still, Run, or Fight?")
        if a_center_decision == "Run" or a_center_decision == "run":
            print("The Lion chases and pounces on", name, ". The Lion bites your neck, killing you, and then devours you.")
            print(name, player.title, "DIED!!! Game Over.")
            exit()
        elif a_center_decision == "Stand" or a_center_decision == "stand":
            print("The Lion rushes past", name, ", you can hear the guards from the left screaming. You take the right door to avoid the lion.")
        elif a_center_decision == "Fight" or a_center_decision == "fight":
            enemy = Character(
                        title="Lion",
                        character_armor=11,
                        character_health=30,
                        max_health=30,
                        health_potion=0,
                        damage_die=6,
                        parry=0,
                        parry_max=0,
                        spell=0,
                        spell_max=0,
                        crit=2,
                        die_modifier=2,
                        gold=0,
                    )
            combat(name, player, enemy)
    elif decision == "right":
        print("You keep running down the hallway.")
    elif decision == "left":
        print()
        print(name, "has exited the arena. But some part of you wants vengence against the Master of the Arena")
        decision_left = input("Do you Escape with your life or Return to the arena to kill the master?")
        if decision_left == "escape":
            print()
            decision_left_final = input("Do you continue on Foot or Steal a nearby horse?")
            if decision_left_final == "foot":
                print()
                print("The guards catch up to you, surround you, and kill you.")
                print(name, "the", player.title, "is still a champion...just a dead one as well. Game Over.")
                exit()
            elif decision_left_final == "steal":
                print()
                print(name, "swiftly passes the guards and leaves the city!")
                print("You are still a champion, but you can never return to the arena and only seek glory in other locations.")
                print()
                print("Conratulations,", name, "the", player.title, ". You survived the Arena!!!")
                return VILLAGE
        elif decision_left == "return":
            print()
            print(name, "the", player.title, "desires Vengence! And you shall have it or die!")
        else:
            print()
            print(name, "the", player.title, "thought long and hard about what to do. However, the guards surrounded you while you did it.", name, "was then executed.")
            print(name, "the", player.title, "is still a champion...just a dead one as well. Game Over.")
            exit()
    else:
        print()
        print("You tripped and fell on your sword. You bleed out and die.")
        print(name, "the", player.title, "are still a champion...just a dead one as well. Game Over.")
        exit()

def confronting_the_master(name, player):
    print()
    print("You enter the balcony and the Master of the Arena is staring at you, surprise in their eyes.")
    decision = input("Do you Spare them or do you get Vengence?").lower()
    if decision == "spare":
        print()
        print("As you walk away, letting go of hate, the Master of the Arena lunges towards you with a knife.")
        input("Use a Parry or cast a Spell to counter the attack!(press enter or return to see if you're successful)")
        if player.parry > 0 and player.spell > 0:
            print()
            input("You used the last of your strength to reverse the Master of the Arena's attack and send them flying off the balcony.")
            print()
            input("You look over and see the Master dead, with the crowd chearing for you!")
            print()
            print(name, "have defeated the Master of the Arena!", name, "the", player.title, "is the new Master of the Arena!")
            return MASTER
        else:
            print("you can't parry or cast a spell.")
            print()
            print("You were so close...but the Master got the jump on you and killed", name, "the", player.title, "! Game Over.")
            exit()
    elif  decision == "vengence":
        print()
        print("You have defeated the Master of the Arena!", name, "the", player.title, "is the new Master of the Arena!")
        print()
        return MASTER


def play(player):
    print()
    print("Welcome to the Arena, where those who enter can seek glory or death!")
    print("Defeat all enemies to achieve victory!")
    input("You enter a vast gladitorial arena, with a chearing crowd and the Master of the Arena watching.")
    print()

    name = player.name

    enemy = Character(
                title="Gladiator",
                character_armor=8,
                character_health=15,
                max_health=15,
                health_potion=0,
                damage_die=10,
                parry=0,
                parry_max=0,
                spell=0,
                spell_max=0,
                crit=4,
                die_modifier=0,
                gold=0,
            )

    before_the_fight(name, player, enemy)

    #first combat
    combat(name, player, enemy)

    enemy = Character(
                title="Gladiator",
                character_armor=12,
                character_health=25,
                max_health=25,
                health_potion=0,
                damage_die=10,
                parry=0,
                parry_max=0,
                spell=0,
                spell_max=0,
                crit=4,
                die_modifier=2,
                gold=0,
            )

    before_the_fight(name, player, enemy)

    #second combat
    combat(name, player, enemy)

    enemy = Character(
                title="Champion",
                character_armor=15,
                character_health=35,
                max_health=35,
                health_potion=0,
                damage_die=10,
                parry=0,
                parry_max=0,
                spell=0,
                spell_max=0,
                crit=4,
                die_modifier=4,
                gold=0,
            )

    before_the_fight(name, player, enemy)

    #third combat
    combat(name, player, enemy)

    print()
    print("But wait...the Master of the Arena does not approve. They have ordered your execution!")

    leave_the_arena(name, player)

    outcome = left_right_center(name, player)
    
    if outcome == VILLAGE:
        return VILLAGE

    print()
    print("You keep running forward until you get too the Master of the Arena's balcony door.")

    enemy = Character(
                title="Guard",
                character_armor=13,
                character_health=50,
                max_health=50,
                health_potion=0,
                damage_die=10,
                parry=0,
                parry_max=0,
                spell=0,
                spell_max=0,
                crit=4,
                die_modifier=4,
                gold=0,
            )

    the_balcony_door(name, player, enemy)

    outcome = confronting_the_master(name, player)

    if outcome == MASTER:
        return MASTER
