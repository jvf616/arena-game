from game.core.combat import endless

from game.core.characters import Character


def play(player):
    print()
    print("Welcome to Endless Combat, where those who enter can seek glory will only find death!")
    input("If you ever forget your character details during combat, type in Status(hit enter/return to continue)")
    print()
    ready = input("Are you prepared to die? Yes or No?").lower()
    if ready == "yes" or ready == "y":
        print()
        print("A gladiator has entered the arena,", player.name, ", prepare for combat!")
    elif ready == "no" or ready == "n":
        print()
        print("Then you are a coward", player.name, "...and too bad, your fate is sealed!")
    else:
        print()
        print("Can't decide what to do? Don't worry,", player.name, ", a decision will be made for you...")
        print("A Gladiator has entered the arena,", player.name, ", prepare for combat!")



    enemy = Character(
                title="Gladiator",
                character_armor=4,
                character_health=10,
                max_health=10,
                health_potion=0,
                damage_die=10,
                parry=0,
                parry_max=0,
                spell=0,
                spell_max=0,
                crit=4,
                die_modifier=0,
                gold=0
            )

    endless(player, enemy)
