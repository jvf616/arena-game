from game.core.combat import level_up, dragon_combat
from game.core.characters import Character



#Intro to epilogue
def play(player):
    
    name = player.name
    player.character_health = player.max_health
    player.parry = player.parry_max
    player.spell = player.spell_max

    level_up(name, player)

    print()
    print("Years pass by, and", name, "has lived a wonderful life, embracing the choices they made in life.")
    print("After returning home, you find it burning and nearly destroyed.")
    input("What could have done this? What could bring such disaster and destruction?")

    print()
    print("'A Dragon!!!' shouts a nearby citizen.")
    print("They run up to you and say 'It came soaring through the sky in a blaze of fire!")
    input("'So many are dead and injured, I saw the dragon fly towards the mountains.'")

    print()
    print("You will not let this dragon destroy everything you've built.")
    print(name, "decides to go to the dragons lair and slay the beast yourself!")
    input("You gather your gear, grab your finest stead, and travel towards the mountain.")
#travel to dragon
    print()
    print("Arriving at the entrance of the cave, you hear a roar from the dragon inside.")
    print("You jump of your horse and you watch it flee into the nearby woods.")
    input("You enter the lair, unafraid and ready for the fight of their life.")
#reaach lair of dragon
    # print("Inside of the lair, you see three tunnels infront of you.")
    # tunnel = input("Do you go Left, Right, or Center?").lower()
    # if tunnel == "left":
    #     print()
    #     print("You go left, and find yourself in a dark cavern.")
    #     print("You light a torch and wake up two baby dragons")

#fight dragon

    dragon = Character(
        title="Dragon",
        character_armor=20,
        character_health=100,
        max_health=100,
        health_potion=2,
        damage_die=20,
        parry=0,
        parry_max=0,
        spell=1,
        spell_max=1,
        crit=5,
        die_modifier=5,
        gold=0
    )

    print()
    print("You walk into a massive cavern, glowing with massive amounts of gold and treasure inside.")
    print("As you walk towrds the center of the cavern, a massive dragon appears from behind the hoard of riches.")
    print("The dragon breaths a fireball behind you, blocking the way out.")
    print("All of your training has prepared you for this, and you charge!")

    dragon_combat(name, player, dragon)

    print()
    print("With the Dragon slain, you are able to use its hoard to rebuild your home with a lot left over.")
    print(name, "decides that it is time to end adventuring, and spend the rest of their life in leisure")
    print("Congratulations, you have won the game! Thanks for playing.")



#player wins game