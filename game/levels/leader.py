from game.core.characters import Character

from game.core.combat import combat, level_up, defense, attack

from game.core.dialogue import introduce_level_three_rules

from game.core.multiple import three_enemies

from game.core.seige import seige_combat, Army, Catapult

EMPEROR = 5
ENDLESS = 7
EPILOGUE = 4

def play(player):
    name = player.name
    player.character_health = player.max_health
    player.parry = player.parry_max
    player.spell = player.spell_max

    level_up(name, player)

    introduce_level_three_rules(player)

    print()
    print("As the new leader of the village,", name, "has continued to protect the fellow villagers.")
    print("For months, your name as protector of the village has spread, and your deeds have not gone unnoticed.")
    input("On an average day, one of the villagers comes up to you and informs you of a dire situation.")

    # print()
    # print("Once the guard leaves, you see the flier, with a drawing of your face.")
    # print("Below, you see the statement 'Wanted:", name, "the", player.title, "Reward for information to whereabouts.'")
    # input("This worries you, and you tear down the flier before anyone can see it.")

    print()
    print("'", name, ", I'm sorry to tell you that the Emperor is looking for you, fliers have been posted in town.'")
    print("'The other villagers and I are worried the Emperor may arrest you or worse, execute you.'")
    input("'The village agrees, we will protect you, as you've done more than the Emperor ever did...'")

    enemies = 3

    enemy_one = Character(
        title="Royal Guard",
        character_armor=12,
        character_health=20,
        max_health=20,
        health_potion=0,
        damage_die=10,
        parry=2,
        parry_max=2,
        spell=0,
        spell_max=0,
        crit=4,
        die_modifier=2,
        gold=0
    )

    enemy_two = Character(
        title="Royal Guard",
        character_armor=12,
        character_health=20,
        max_health=20,
        health_potion=0,
        damage_die=10,
        parry=0,
        parry_max=0,
        spell=2,
        spell_max=2,
        crit=4,
        die_modifier=2,
        gold=0
    )

    enemy_three = Character(
        title="Royal Guard",
        character_armor=12,
        character_health=20,
        max_health=20,
        health_potion=0,
        damage_die=10,
        parry=1,
        parry_max=1,
        spell=1,
        spell_max=1,
        crit=4,
        die_modifier=2,
        gold=0
    )


    print()
    print("The next day", name, "gets a knock on the door, and as you open the door you are grabbed by four Royal Guards.")
    print("'You are under arrest, ", name, ", we are taking you back to the arena under orders of Emperor Maximus!'")
    fight_or_surrender = input("Do you Surrender to them or do you Fight them?").lower()
    if fight_or_surrender == "surrender":
        print()
        print(name, "goes with the Royal Guard to await certain death...")
        return ENDLESS
    else:
        print()
        print(name, "will not go back quietly, you get out of the Royal Guards grip!")
        print("'Fine,' says the lead royal guard, 'kill them and be done with it!'")
        input("Three of the four royal guards unsheathe their swords but you get the firt attack...")
        three_enemies(name, player, enemy_one, enemy_two, enemy_three, enemies)

        enemy = Character(
            title="Lead Royal Guard",
            character_armor=15,
            character_health=30,
            max_health=30,
            health_potion=0,
            damage_die=10,
            parry=2,
            parry_max=2,
            spell=2,
            spell_max=2,
            crit=5,
            die_modifier=4,
            gold=0
        )

        print()
        print("'You are making a big mistake, if we don't return the Emperor will send an army after you!'")
        print("'This village and yourself will never hold back an army, you're all as good as dead.'")
        input("The lead Royal Guard lunges at you!!!")
        defense(name, player, enemy)
        combat(name, player, enemy)
    

    player.character_health = player.max_health
    player.parry = player.parry_max
    player.spell = player.spell_max

    print()
    print("As the Royal Guard lay dead at your feet, the villagers rush to your aid.")
    print("'I heard what the Royal Guard said, we need to prepare for the Emperor's army!'")
    input("You agree, as well as the whole village, to prepare for war...")

    print()
    print("You spend two days preparing for the Emperor's army, and the villagers train in physical and magical combat.")
    print("The villagers were also able to construct three catapults")
    input("On the third day, The Emperor and the Master of the Arena arrived with an army in tow...")

    print()
    print("The Master of the Arena shouts 'No one escapes the arena!'")
    print("'You should have returned, now you've doomed this entire town!'")
    input("The Emperor signals his army to attack the village, but you get the first attack.")

    village = Army(
        soldiers=200,
        defense=15,
        offense=5,
        archers=20,
        mages=30,
        mage_spells=5,
        catapult_amount=3,
        catapult_ammo=1
    )

    emperor_army = Army(
        soldiers=300,
        defense=15,
        offense=5,
        archers=20,
        mages=30,
        mage_spells=5,
        catapult_amount=3,
        catapult_ammo=1
    )

    army_catapult_one = Catapult(
        health=50,
        strength=12,
    )

    army_catapult_two = Catapult(
        health=50,
        strength=12,
    )

    army_catapult_three = Catapult(
        health=50,
        strength=12,
    )

    enemy_catapult_one = Catapult(
        health=50,
        strength=12,
    )

    enemy_catapult_two = Catapult(
        health=50,
        strength=12,
    )

    enemy_catapult_three = Catapult(
        health=50,
        strength=12,
    )


    seige_combat(village, emperor_army, army_catapult_one, army_catapult_two, army_catapult_three, enemy_catapult_one, enemy_catapult_two, enemy_catapult_three)

    if village.soldiers < 100:
        
        enemies = 3

        soldier_one = Character(
            title="Soldier",
            character_armor=15,
            character_health=25,
            max_health=25,
            health_potion=0,
            damage_die=10,
            parry=2,
            parry_max=2,
            spell=0,
            spell_max=0,
            crit=4,
            die_modifier=3,
            gold=0
        )

        soldier_two = Character(
            title="Soldier",
            character_armor=15,
            character_health=25,
            max_health=25,
            health_potion=0,
            damage_die=10,
            parry=0,
            parry_max=0,
            spell=2,
            spell_max=2,
            crit=4,
            die_modifier=3,
            gold=0
        )

        soldier_three = Character(
            title="Soldier",
            character_armor=15,
            character_health=25,
            max_health=25,
            health_potion=0,
            damage_die=10,
            parry=1,
            parry_max=1,
            spell=1,
            spell_max=1,
            crit=4,
            die_modifier=3,
            gold=0
        )
        
        print()
        print("The Emperor's army has broken through your defense lines, the village is in full retreat!")
        input("You and a few other villagers hold your ground to let others escape, preparing for the enemy to strike...")
        three_enemies(name, player, soldier_one, soldier_two, soldier_three, enemies)

        brute = Character(
            title="Brute",
            character_armor=15,
            character_health=75,
            max_health=75,
            health_potion=3,
            damage_die=12,
            parry=2,
            parry_max=2,
            spell=2,
            spell_max=2,
            crit=5,
            die_modifier=4,
            gold=0
        )

        print()
        print("You are surrounded by enemy soldiers, with nowhere to escape.")
        print("A massive brute approaches you, and behind them the Emperor and Master of the Arena.")
        input("The brute charges at you getting in the first swing...")
        defense(name, player, brute)
        combat(name, player, brute)

        print()
        print("You take the vanquished enemy's sword and throw it into the Master's chest.")
        print("With everyone shocked,", name, "sprints towards the Emperor and holds him hostage.")
        plead = input("The Emperor pleads for his life, due you ignore his plead? Yes or No?").lower()
        if plead == "no" or plead == "n":
            print()
            print("You release the Emperor, telling him never to return and leave this village alone.")
            print("Since being watched by his loyal soldiers, the Emperor agrees and chooses to let you be.")
            input("The village cheers, and the Emperor's army leaves never to return.")
            print()
            print("Congrtulations!", name, "survived the seige and protected the village!")
            return EPILOGUE    
        else:
            print()
            print(name, "kills the Emperor, and the remaining soldiers strike you down without the hostage.")
            print(name, "the", player.title, "died. Game Over...")
            exit()
    elif emperor_army.soldiers < 100:
        champion = Character(
            title="Champion",
            character_armor=15,
            character_health=75,
            max_health=75,
            health_potion=3,
            damage_die=12,
            parry=2,
            parry_max=2,
            spell=2,
            spell_max=2,
            crit=5,
            die_modifier=4,
            gold=0
        )
        
        print()
        print("The village sends the Emperor's army fleeing, but you decide to follow and kill the invaders.")
        print("On horseback, you catch up to the Emperor and Master to find yourself against the Emperor's Champion.")
        combat(name, player, champion)

        print()
        print("You kill the Champion, and in your frenzy strike down the Master as well.")
        plead = input("The Emperor pleads for his life, do you ignore his plead? Yes or No?").lower()
        if plead == "no" or plead == "n":
            print()
            print("You let the Emperor live, telling him to never return.")
            print("The Meperor ignores this and brings the rest of his army to kill you and the rest of the villagers.")
            print(name, "the", player.title, " DIED!!! Game Over...")
            exit()
        else:
            print()
            print("You slay the Emperor where they stand, and decide to take the throne for yourself.")
            print("Taking the Emperor's head as proof, you and all the villagers go into the city straight to the throne room.")
            input("By the laws of the city,", name, "the", player.title, "is crowned the new Emperor.")
            print()
            print("Congratulations! You were a Champion, a Leader, and now an Emperor!")
            print("What lies in store for you as Emperor?")
            return EMPEROR