from game.core.characters import Character

from game.core.combat import combat, level_up, defense, attack

from game.core.dialogue import introduce_level_three_rules

from game.core.multiple import three_enemies

from game.core.seige import seige_combat, Army, Catapult

EPILOGUE = 4

def play(player):
    name = player.name
    player.character_health = player.max_health
    player.parry = player.parry_max
    player.spell = player.spell_max

    level_up(name, player)

    introduce_level_three_rules(player)

    print()
    print("Once", name, "killed Emperor Maximus and took his throne, thier rule has dawned a new age of prosperity.")
    print("The entire city has rallied behind you, believing you to be the best emperor in a century.")
    input("A few days before the five year anniversary of your rule, a strange messanger enters your halls...")

    messanger = Character(
        title="Messanger",
        character_armor=5,
        character_health=5,
        max_health=5,
        health_potion=0,
        damage_die=1,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=1,
        die_modifier=0,
        gold=0
    )

    print()
    print("'I am a servant of the great and powerful Drakona! I come with a message!'")
    print("'Surrender your kingdom or you shall feel Drakona's wrath!'")
    surrender = input("Do you give up all that you've fought for? Yes or No?").lower()
    if surrender == "yes" or surrender == "y":
        print()
        print("'My liege, you can't' says one of your knights.")
        print("'You must stay, I've heard of Drakona, you can't let them take the city!'")
        end_game = input("Are you sure you want to leave your people to die or become enslaved? Yes or No?").lower()
        if end_game == "yes" or end_game == "y":
            print()
            print(name, "the", player.title, "has surrendered to Drakona and left the city.")
            print("Their name and legacy now tarnished,", name, "was never seen again.")
            print("You eventually died and were forgotten...Game Over!")
            exit()
        else:
            print()
            print(name, "decides not to let this messanger tell them what to do!")
            messanger_fate = input("Do you let the messanger Live or Kill them?").lower()
            if messanger_fate == "live":
                print()
                print(name, "orders the messanger to leave at once, and tell Drakona to never come to your kingdom.")
                print("'You'll regret this Emperor", name, "! You'll all burn'")
                input("The messanger leaves, and continue planning on your five year celebration...")
            elif messanger_fate == "kill":
                print()
                print(name, "will not be disrespected in their court, and you leap from your thrown towards the messanger.")
                combat(name, player, messanger)
                input("With the messanger dead, you continue planning your five year celebration...")
            else:
                print()
                print("'I will not wait any longer, your fate is sealed, Drakona shall destroy you!'")
                print("The messanger throws something on the ground and in a puff of smoke disappears.")
                input("With the messanger gone, you continue planning your five year celebration...")
    else:
        print()
        print(name, "decides not to let this messanger tell them what to do!")
        messanger_fate = input("Do you let the messanger Live or Kill them?").lower()
        if messanger_fate == "live":
            print()
            print(name, "orders the messanger to leave at once, and tell Drakona to never come to your kingdom.")
            print("'You'll regret this Emperor", name, "! You'll all burn'")
            input("The messanger leaves, and continue planning on your five year celebration...")
        elif messanger_fate == "kill":
            print()
            print(name, "will not be disrespected in their court, and you leap from your thrown towards the messanger.")
            combat(name, player, messanger)
            input("With the messanger dead, you continue planning your five year celebration...")
        else:
            print()
            print("'I will not wait any longer, your fate is sealed, Drakona shall destroy you!'")
            print("The messanger throws something on the ground and in a puff of smoke disappears.")
            input("With the messanger gone, you continue planning your five year celebration...")

    print()
    print("A few days later, it is your five year anniversary of you being Emperor.")
    print("Every person in the city is celebrating your name, making it a most joyous occasion.")
    input("But something stirs, a knot in your stomach makes you feel like something is coming...")

    print()
    print("The bells ring throughout the city, the sign that your city is under attack!")
    print("The citizens seek shelter while your knights and soldiers gear up for battle.")
    input("All who are ready to fight, including you, rush to the city walls.")

    print()
    print(name, "sees a massive army, led by who you believe to be Drakona.")
    print("'You should have fled, for I will burn this city to the ground! ATTACK!!!' shouts Drakona")
    input("Prepare for a seige, and unleash the first attack...")

    army = Army(
        soldiers=200,
        defense=15,
        offense=5,
        archers=20,
        mages=30,
        mage_spells=5,
        catapult_amount=3,
        catapult_ammo=1
    )

    drakona_army = Army(
        soldiers=300,
        defense=15,
        offense=5,
        archers=20,
        mages=30,
        mage_spells=5,
        catapult_amount=3,
        catapult_ammo=1
    )

    army_catapult_one = Catapult(
        health=50,
        strength=12,
    )

    army_catapult_two = Catapult(
        health=50,
        strength=12,
    )

    army_catapult_three = Catapult(
        health=50,
        strength=12,
    )

    enemy_catapult_one = Catapult(
        health=50,
        strength=12,
    )

    enemy_catapult_two = Catapult(
        health=50,
        strength=12,
    )

    enemy_catapult_three = Catapult(
        health=50,
        strength=12,
    )


    seige_combat(army, drakona_army, army_catapult_one, army_catapult_two, army_catapult_three, enemy_catapult_one, enemy_catapult_two, enemy_catapult_three)

    enemies = 3

    enemy_one = Character(
        title="Drakona Soldier",
        character_armor=12,
        character_health=20,
        max_health=20,
        health_potion=0,
        damage_die=10,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=4,
        die_modifier=2,
        gold=0
    )

    enemy_two = Character(
        title="Drakona Soldier",
        character_armor=12,
        character_health=20,
        max_health=20,
        health_potion=0,
        damage_die=10,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=4,
        die_modifier=2,
        gold=0
    )

    enemy_three = Character(
        title="Drakona Soldier",
        character_armor=12,
        character_health=20,
        max_health=20,
        health_potion=0,
        damage_die=10,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=4,
        die_modifier=2,
        gold=0
    )

    if army.soldiers < 100:
        enemies = 3

        enemy_one = Character(
            title="Drakona Soldier",
            character_armor=12,
            character_health=20,
            max_health=20,
            health_potion=0,
            damage_die=10,
            parry=0,
            parry_max=0,
            spell=0,
            spell_max=0,
            crit=4,
            die_modifier=2,
            gold=0
        )

        enemy_two = Character(
            title="Drakona Soldier",
            character_armor=12,
            character_health=20,
            max_health=20,
            health_potion=0,
            damage_die=10,
            parry=0,
            parry_max=0,
            spell=0,
            spell_max=0,
            crit=4,
            die_modifier=2,
            gold=0
        )

        enemy_three = Character(
            title="Drakona Soldier",
            character_armor=12,
            character_health=20,
            max_health=20,
            health_potion=0,
            damage_die=10,
            parry=0,
            parry_max=0,
            spell=0,
            spell_max=0,
            crit=4,
            die_modifier=2,
            gold=0
        )
        print("You take your remaining soldiers and meet the enemy head on inside the city.")
        input("The enemy charges, and you stand ready to fight!")
        three_enemies(name, player, enemy_one, enemy_two, enemy_three, enemies)

        print()
        print("More soldiers are flooding the city, you stand your ground.")
        input("These soldiers seem stronger than the last...")
        
        enemies = 3
        
        drakona_one = Character(
            title="Drakona Elite",
            character_armor=15,
            character_health=25,
            max_health=25,
            health_potion=0,
            damage_die=10,
            parry=4,
            parry_max=4,
            spell=0,
            spell_max=0,
            crit=4,
            die_modifier=2,
            gold=0
        )

        drakona_two = Character(
            title="Drakona Elite",
            character_armor=15,
            character_health=25,
            max_health=25,
            health_potion=0,
            damage_die=10,
            parry=0,
            parry_max=0,
            spell=4,
            spell_max=4,
            crit=4,
            die_modifier=2,
            gold=0
        )

        drakona_three = Character(
            title="Drakona Elite",
            character_armor=15,
            character_health=25,
            max_health=25,
            health_potion=0,
            damage_die=10,
            parry=2,
            parry_max=2,
            spell=2,
            spell_max=2,
            crit=4,
            die_modifier=2,
            gold=0
        )
        three_enemies(name, player, drakona_one, drakona_two, drakona_three, enemies)

        print()
        print("The soldiers stop flooding the city and stand still, banging their shields in rythym.")
        print("Drakona approaches you, fully clad in black armor, towering over you.")
        input("'We shall decide this between ourselves, I will not accept your surrender.'")

        drakona = Character(
            title="Drakona",
            character_armor=15,
            character_health=75,
            max_health=75,
            health_potion=4,
            damage_die=12,
            parry=4,
            parry_max=4,
            spell=4,
            spell_max=4,
            crit=5,
            die_modifier=5,
            gold=0
        )

        combat(name, player, drakona)

        print()
        print("Drakona lay dead at your feet, thier army flees from the city never to return.")
        print("The people and fellow soldiers cheer your name and celebrate your victory!")
        input("A Champion, a Master, Emperor, and now savior. Congratulations!")
    elif drakona_army.soldiers < 100:
        print("Drakona's army is in full retreat, but you will not let this fiend escape.")
        print("You grab your horse and remaining soldiers to eliminate any remaining enemy soldiers.")
        input("You are able to catch up to Drakona and some of their soldiers in a nearby forest.")

        print()
        print("Suddenly, from behind the trees jump out more Drakona soldiers, surrounding your soldiers.")
        print("Drakona says 'Hello", name, "the", player.title, ", you defeated my forces, but yet to defeat me.'")
        input("Unfortunately, you will not get the chance. Soldiers, ATTACK!!!")

        enemies = 3
        
        drakona_one = Character(
            title="Drakona Elite",
            character_armor=15,
            character_health=25,
            max_health=20,
            health_potion=0,
            damage_die=10,
            parry=4,
            parry_max=4,
            spell=0,
            spell_max=0,
            crit=4,
            die_modifier=2,
            gold=0
        )

        drakona_two = Character(
            title="Drakona Elite",
            character_armor=15,
            character_health=25,
            max_health=20,
            health_potion=0,
            damage_die=10,
            parry=0,
            parry_max=0,
            spell=4,
            spell_max=4,
            crit=4,
            die_modifier=2,
            gold=0
        )

        drakona_three = Character(
            title="Drakona Elite",
            character_armor=15,
            character_health=25,
            max_health=20,
            health_potion=0,
            damage_die=10,
            parry=2,
            parry_max=2,
            spell=2,
            spell_max=2,
            crit=4,
            die_modifier=2,
            gold=0
        )

        defense(name, player, drakona_one)
        defense(name, player, drakona_two)
        defense(name, player, drakona_three)
        three_enemies(name, player, drakona_one, drakona_two, drakona_three, enemies)
        
        drakona = Character(
            title="Drakona",
            character_armor=15,
            character_health=75,
            max_health=75,
            health_potion=4,
            damage_die=12,
            parry=4,
            parry_max=4,
            spell=4,
            spell_max=4,
            crit=5,
            die_modifier=5,
            gold=0
        )

        print()
        print("With the help of your knights, you killed all of Drakona's soldiers.")
        kill_drakona = input("All but Drakona remains, and will you let them Live or Kill them.").lower()
        if kill_drakona == "yes" or kill_drakona == "y":
            print()
            print("You leap with immense speed and get in a second attack")
            attack(name, player, drakona)
            combat(name, player, drakona)
        elif kill_drakona == "no" or kill_drakona == "n":
            print()
            print("'I will not accept this defeat, I will kill you or die trying!!'")
            print("Drakona jumps at you with astounishing speed.")
            defense(name, player, drakona)
            combat(name, player, drakona)
        else:
            print()
            print("Before you can give your decision, Drakona jumps at you!")
            defense(name, player, drakona)
            combat(name, player, drakona)
        
        print()
        print("With Drakona dead and their army decimated,", name, "returns to the city victorious.")
        print("The people and fellow soldiers cheer your name and celebrate your victory!")
        input("A Champion, a Master, Emperor, and now savior. Congratulations!")

    return EPILOGUE