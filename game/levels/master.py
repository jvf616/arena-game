from game.core.dice import dtwenty, roll, healing_die, parry_hit

from game.core.characters import Character

from game.core.combat import combat, level_up, defense

from game.core.dialogue import tavern, introduce_level_two_rules, tribute

from game.core.obstacles import spying

from game.core.shopping import shopping

from game.core.multiple import three_enemies

EMPEROR = 5

def confronting_the_emperor(name, player, royal_guard_one, royal_guard_two, royal_guard_three, enemies, champion):
    print()
    print("You find the front gate of the palace to be unguarded, what luck.")
    print("Walking straight into the thrown chamber, you find yourself surrounded by the Royal Guard.")
    input("Emperor Maximus enters the thrown room, making you realize he expected you to be here.")
    print()
    print("'I knew you survived once my assassin didn't return, only a matter of time before you tried to kill me.'")
    print("'I'm afraid, no matter what you sent as tribute, you were always going to die, for you killed my friend to become the Master.'")
    print("'You should have died in the arena, but now, you will die here,", name, "the", player.title, "!'")
    input("You are surrounded by the royal guard, it's time to fight or die!")
    three_enemies(name, player, royal_guard_one, royal_guard_two, royal_guard_three, enemies)
    print()
    print("You slay the Royal Guards, the Emperor gives a slow clap.")
    print("'Fine, kill my guards, and you may have killed the Master's Champion, but lets see you face mine!'")
    input("Walking in to the throne room, you see a massive brute in full clad armor, who then charges at you before you can attack!")
    print()
    defense(name, player, champion)
    combat(name, player, champion)
    print()
    print("The Emperor's face shows it all, he is completely helpless.")
    riches_or_power = input("'I'm sorry,' says the Emperor, 'I'll give you whatever you want: Riches, Power, anything!'").lower()
    if riches_or_power == "riches":
        print()
        print("'Of course, here is the key to the royal treasury'. The Emperor holds out a key.")
        print("As you reach for the key, Emperor Maximus pulls out a knife and slits your throat.")
        print(name, "the", player.title, "DIED!!! Game Over.")
        exit()
    if riches_or_power == "power":
        print()
        print("Of course, I can make you a Duke of this nearby village where you can have control of the entire populace.")
        print("You would just have to give up your title of Master of the Arena...")
        take_it_all = input("Do you want to be a Duke, or do you want real Power.").lower()
        if take_it_all == "duke":
            print()
            print("'Of course, here is the dead to the land'. The Emperor holds out a piece of parchment.")
            print("As you reach for the dead, Emperor Maximus pulls out a knife and slits your throat.")
            print(name, "the", player.title, "DIED!!! Game Over.")
            exit()
        else:
            print()
            print("You tell the Emperor, 'I want your power!'")
            print(name, "the", player.title, "takes their weapon and decapitates the Emperor's head!")
            print("A Champion, a Master, now", name, "the", player.title, "is now an Emperor!")
            print("Congratulations!!! So concludes another chapter in your epic tale!")
            return EMPEROR
    else:
        print()
        print("You tell the Emperor, 'I want your head!'")
        print(name, "the", player.title, "takes their weapon and decapitates the Emperor's head!")
        print("A Champion, a Master, now", name, "the", player.title, "is now an Emperor!")
        print("Congratulations!!! So concludes another chapter in your epic tale!")
        return EMPEROR

def play(player):
    introduce_level_two_rules(player)
    
    print()
    print("The roaring crowd, gladiators dying for your entertainment.")
    print("You were once the Champion of the Arena, now you are the Master of the Arena.")
    input("You used to fight for glory, now...you watch others do the same.")
    print()
    print("After the fight, you go to the lavish after party with the most influential people of the city.")
    print("While conversing with guests, a man with a crown on his head trailed by two guards walks up to you.")
    
    name = player.name
    player.character_health = player.max_health
    player.parry = player.parry_max
    player.spell = player.spell_max

    print("You tell the man your name and title:", name, "the", player.title)

    tribute(name, player)

    print()
    print("The next day goes as planned, with the Arena games entertaining everyone in the crowd.")
    input("After a long day you decide to go to sleep early, but something in you stirs...")
    print()
    print("You awake! And there in front of you is an assassin!")
    print("'The Emperor did not like your response to his tribute' they say.")
    print("'Once again, Champion, you have been sentenced to death!'")
    print(name, "grabs their weapon and gets in the first swing against the assassins!")

    #assassin combat

    enemy_one = Character(
        title="Assassin",
        character_armor=14,
        character_health=45,
        max_health=45,
        health_potion=0,
        damage_die=10,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=4,
        die_modifier=2,
        gold=0
    )

    combat(name, player, enemy_one)

    print()
    print("You catch your breath, the assassin lay dead at your feet.")
    print("Your rage builds, for you know who sent them...The Emperor!")
    input("This will not stand, for tomorrow, you will have vengence!")

    player.character_health = player.max_health
    player.parry = player.parry_max
    player.spell = player.spell_max 
    

    level_up(name, player)

    print()
    print(name, "is absent from the arena today, as they need to kill the Emperor.")
    print("Before you go, it comes to thought that you should prepare for this encounter.")
    input("To do this, you go into the city disguised, bringing a small amount of your riches.")

    player.gold = 200

    print()
    print("You currently have", player.gold, "gold!")
    print("(Use gold to spend on food and supplies. Getting food will end your shopping spree.)")

    shopping(name, player)

    tavern(name, player)

    guard_one = Character(
        title="Smelly Guard",
        character_armor=10,
        character_health=15,
        max_health=15,
        health_potion=0,
        damage_die=10,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=4,
        die_modifier=0,
        gold=0
    )

    guard_two = Character(
        title="Tall Guard",
        character_armor=10,
        character_health=15,
        max_health=15,
        health_potion=0,
        damage_die=10,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=4,
        die_modifier=0,
        gold=0
    )

    guard_three = Character(
        title="Wise Guard",
        character_armor=10,
        character_health=15,
        max_health=15,
        health_potion=0,
        damage_die=10,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=4,
        die_modifier=0,
        gold=0
    )

    enemies = 3

    spying(name, player, guard_one, guard_two, guard_three, enemies)


    royal_guard_one = Character(
        title="Royal Guard",
        character_armor=12,
        character_health=20,
        max_health=20,
        health_potion=0,
        damage_die=10,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=4,
        die_modifier=2,
        gold=0
    )

    royal_guard_two = Character(
        title="Royal Guard",
        character_armor=12,
        character_health=20,
        max_health=20,
        health_potion=0,
        damage_die=10,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=4,
        die_modifier=2,
        gold=0
    )

    royal_guard_three = Character(
        title="Royal Guard",
        character_armor=12,
        character_health=20,
        max_health=20,
        health_potion=0,
        damage_die=10,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=4,
        die_modifier=2,
        gold=0
    )

    enemies = 3

    champion = Character(
        title="Champion",
        character_armor=17,
        character_health=60,
        max_health=60,
        health_potion=0,
        damage_die=12,
        parry=0,
        parry_max=0,
        spell=0,
        spell_max=0,
        crit=5,
        die_modifier=3,
        gold=0
    )

    print()
    print("You find the front gate of the palace to be unguarded, what luck.")
    print("Walking straight into the thrown chamber, you find yourself surrounded by the Royal Guard.")
    input("Emperor Maximus enters the thrown room, making you realize he expected you to be here.")
    print()
    print("'I knew you survived once my assassin didn't return, only a matter of time before you tried to kill me.'")
    print("'I'm afraid, no matter what you sent as tribute, you were always going to die, for you killed my friend to become the Master.'")
    print("'You should have died in the arena, but now, you will die here,", name, "the", player.title, "!'")
    input("You are surrounded by the royal guard, it's time to fight or die!")
    three_enemies(name, player, royal_guard_one, royal_guard_two, royal_guard_three, enemies)
    print()
    print("You slay the Royal Guards, the Emperor gives a slow clap.")
    print("'Fine, kill my guards, and you may have killed the Master's Champion, but lets see you face mine!'")
    input("Walking in to the throne room, you see a massive brute in full clad armor, who then charges at you before you can attack!")
    print()
    defense(name, player, champion)
    combat(name, player, champion)
    print()
    print("The Emperor's face shows it all, he is completely helpless.")
    riches_or_power = input("'I'm sorry,' says the Emperor, 'I'll give you whatever you want: Riches, Power, anything!'").lower()
    if riches_or_power == "riches":
        print()
        print("'Of course, here is the key to the royal treasury'. The Emperor holds out a key.")
        print("As you reach for the key, Emperor Maximus pulls out a knife and slits your throat.")
        print(name, "the", player.title, "DIED!!! Game Over.")
        exit()
    if riches_or_power == "power":
        print()
        print("Of course, I can make you a Duke of this nearby village where you can have control of the entire populace.")
        print("You would just have to give up your title of Master of the Arena...")
        take_it_all = input("Do you want to be a Duke, or do you want real Power.").lower()
        if take_it_all == "duke":
            print()
            print("'Of course, here is the dead to the land'. The Emperor holds out a piece of parchment.")
            print("As you reach for the dead, Emperor Maximus pulls out a knife and slits your throat.")
            print(name, "the", player.title, "DIED!!! Game Over.")
            exit()
        else:
            print()
            print("You tell the Emperor, 'I want your power!'")
            print(name, "the", player.title, "takes their weapon and decapitates the Emperor's head!")
            print("A Champion, a Master, now", name, "the", player.title, "is now an Emperor!")
            print("Congratulations!!! So concludes another chapter in your epic tale!")
            return EMPEROR
    else:
        print()
        print("You tell the Emperor, 'I want your head!'")
        print(name, "the", player.title, "takes their weapon and decapitates the Emperor's head!")
        print("A Champion, a Master, now", name, "the", player.title, "is now an Emperor!")
        print("Congratulations!!! So concludes another chapter in your epic tale!")
        return EMPEROR
    # return confronting_the_emperor(name, player, royal_guard_one, royal_guard_two, royal_guard_three, enemies, champion)
