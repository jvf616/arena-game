from game.core.dice import dtwenty, healing_die, roll, critical_hit, spell_hit, parry_hit
from game.core.combat import defense, combat


def three_enemies(name, player, enemy_one, enemy_two, enemy_three, enemies):
    while True:
        if enemy_one.character_health > 0 and enemy_two.character_health > 0 and enemy_three.character_health > 0:
            print()
            print("You are fighting", enemies, "Enemies.")
            who_to_fight = input("Who are you going to attack, enemy One, Two, or Three?").lower()
            if who_to_fight == "one" or who_to_fight == "1":
                attack_three(name, player, enemy_one, enemy_two, enemy_three)
                if enemy_one.character_health <= 0:
                    print()
                    print(enemy_one.title, "has been killed!")
                    break
            elif who_to_fight == "two" or who_to_fight == "2":
                attack_three(name, player, enemy_two, enemy_one, enemy_three)
                if enemy_two.character_health <= 0:
                    print()
                    print(enemy_two.title, "has been killed!")
                    break
            elif who_to_fight == "three" or who_to_fight == "3":
                attack_three(name, player, enemy_three, enemy_one, enemy_two)
                if enemy_three.character_health <= 0:
                    print()
                    print(enemy_three.title, "has been killed!")
                    break
            else:
                print("You couldn't decide who to attack and stumbled...")
                defense(name, player, enemy_one)
                defense(name, player, enemy_two)
                defense(name, player, enemy_three)
    while True:
        if enemy_one.character_health <= 0:
            enemies = 2
            print()
            print("You are fighting", enemies, "Enemies.")
            who_to_fight = input("Who are you going to attack, enemy Two or Three?").lower()
            if who_to_fight == "two" or who_to_fight == "2":
                attack_two(name, player, enemy_two, enemy_three)
                if enemy_two.character_health <= 0:
                    print()
                    print(enemy_two.title, "has been killed!")
                    break
            elif who_to_fight == "three" or who_to_fight == "3":
                attack_two(name, player, enemy_three, enemy_two)
                if enemy_three.character_health <= 0:
                    print()
                    print(enemy_three.title, "has been killed!")
                    break
            else:
                print("You couldn't decide who to attack and stumbled...")
                defense(name, player, enemy_two)
                defense(name, player, enemy_three)
        elif enemy_two.character_health <= 0:
            enemies = 2
            print()
            print("You are fighting", enemies, "Enemies.")
            who_to_fight = input("Who are you going to attack, enemy One or Three?").lower()
            if who_to_fight == "one" or who_to_fight == "1":
                attack_two(name, player, enemy_one, enemy_three)
                if enemy_one.character_health <= 0:
                    print()
                    print(enemy_one.title, "has been killed!")
                    break
            elif who_to_fight == "three" or who_to_fight == "3":
                attack_two(name, player, enemy_three, enemy_one)
                if enemy_three.character_health <= 0:
                    print()
                    print(enemy_three.title, "has been killed!")
                    break
            else:
                print("You couldn't decide who to attack and stumbled...")
                defense(name, player, enemy_one)
                defense(name, player, enemy_three)
        elif enemy_three.character_health <= 0:
            enemies = 2
            print()
            print("You are fighting", enemies, "Enemies.")
            who_to_fight = input("Who are you going to attack, enemy One or Two?").lower()
            if who_to_fight == "one" or who_to_fight == "1":
                attack_two(name, player, enemy_one, enemy_two)
                if enemy_one.character_health <= 0:
                    print()
                    print(enemy_one.title, "has been killed!")
                    break
            elif who_to_fight == "two" or who_to_fight == "2":
                attack_two(name, player, enemy_two, enemy_one)
                if enemy_two.character_health <= 0:
                    print()
                    print(enemy_two.title, "has been killed!")
                    break
            else:
                print("You couldn't decide who to attack and stumbled...")
                defense(name, player, enemy_one)
                defense(name, player, enemy_two)
    while True:
        if enemy_one.character_health <= 0 and enemy_two.character_health <= 0:
            enemies = 1
            print()
            print(enemies, "more Enemy left.")
            combat(name, player, enemy_three)
            break
        elif enemy_one.character_health <= 0 and enemy_three.character_health <= 0:
            enemies = 1
            print()
            print(enemies, "more Enemy left.")
            combat(name, player, enemy_two)
            break
        elif enemy_two.character_health <= 0 and enemy_three.character_health <= 0:
            enemies = 1
            print()
            print(enemies, "more Enemy left.")
            combat(name, player, enemy_one)
            break




# def three_enemies(name, player, enemy_one, enemy_two, enemy_three, enemies):
#     while True:
#         if enemy_one.character_health > 0 and enemy_two.character_health > 0 and enemy_three.character_health > 0:
#             print()
#             print("You are fighting", enemies, "Enemies.")
#             who_to_fight = input("Who are you going to attack, enemy One, Two, or Three?").lower()
#             if who_to_fight == "one" or who_to_fight == "1":
#                 attack(name, player, enemy_one)
#                 if enemy_one.character_health <= 0:
#                     print()
#                     print(enemy_one.title, "has been killed!")
#                     break
#                 else:
#                     defense(name, player, enemy_one)
#                     defense(name, player, enemy_two)
#                     defense(name, player, enemy_three)
#             elif who_to_fight == "two" or who_to_fight == "2":
#                 attack(name, player, enemy_two)
#                 if enemy_two.character_health <= 0:
#                     print()
#                     print(enemy_two.title, "has been killed!")
#                     break
#                 else:
#                     defense(name, player, enemy_one)
#                     defense(name, player, enemy_two)
#                     defense(name, player, enemy_three)
#             elif who_to_fight == "three" or who_to_fight == "3":
#                 attack(name, player, enemy_three)
#                 if enemy_three.character_health <= 0:
#                     print()
#                     print(enemy_three.title, "has been killed!")
#                     break
#                 else:
#                     defense(name, player, enemy_one)
#                     defense(name, player, enemy_two)
#                     defense(name, player, enemy_three)
#             else:
#                 print("You couldn't decide who to attack and stumbled...")
#                 defense(name, player, enemy_one)
#                 defense(name, player, enemy_two)
#                 defense(name, player, enemy_three)
#     while True:
#         if enemy_one.character_health <= 0:
#             enemies = 2
#             print()
#             print("You are fighting", enemies, "Bandits.")
#             who_to_fight = input("Who are you going to attack, enemy Two or Three?").lower()
#             if who_to_fight == "two" or who_to_fight == "2":
#                 attack(name, player, enemy_two)
#                 if enemy_two.character_health <= 0:
#                     print()
#                     print(enemy_two.title, "has been killed!")
#                     break
#                 else:
#                     defense(name, player, enemy_two)
#                     defense(name, player, enemy_three)
#             elif who_to_fight == "three" or who_to_fight == "3":
#                 attack(name, player, enemy_three)
#                 if enemy_three.character_health <= 0:
#                     print()
#                     print(enemy_three.title, "has been killed!")
#                     break
#                 else:
#                     defense(name, player, enemy_two)
#                     defense(name, player, enemy_three)
#             else:
#                 print("You couldn't decide who to attack and stumbled...")
#                 defense(name, player, enemy_two)
#                 defense(name, player, enemy_three)
#         elif enemy_two.character_health <= 0:
#             enemies = 2
#             print()
#             print("You are fighting", enemies, "Bandits.")
#             who_to_fight = input("Who are you going to attack, enemy One or Three?").lower()
#             if who_to_fight == "one" or who_to_fight == "1":
#                 attack(name, player, enemy_one)
#                 if enemy_one.character_health <= 0:
#                     print()
#                     print(enemy_one.title, "has been killed!")
#                     break
#                 else:
#                     defense(name, player, enemy_one)
#                     defense(name, player, enemy_three)
#             elif who_to_fight == "three" or who_to_fight == "3":
#                 attack(name, player, enemy_three)
#                 if enemy_three.character_health <= 0:
#                     print()
#                     print(enemy_three.title, "has been killed!")
#                     break
#                 else:
#                     defense(name, player, enemy_one)
#                     defense(name, player, enemy_three)
#             else:
#                 print("You couldn't decide who to attack and stumbled...")
#                 defense(name, player, enemy_one)
#                 defense(name, player, enemy_three)
#         elif enemy_three.character_health <= 0:
#             enemies = 2
#             print()
#             print("You are fighting", enemies, "Bandits.")
#             who_to_fight = input("Who are you going to attack, enemy One or Two?").lower()
#             if who_to_fight == "one" or who_to_fight == "1":
#                 attack(name, player, enemy_one)
#                 if enemy_one.character_health <= 0:
#                     print()
#                     print(enemy_one.title, "has been killed!")
#                     break
#                 else:
#                     defense(name, player, enemy_one)
#                     defense(name, player, enemy_two)
#             elif who_to_fight == "two" or who_to_fight == "2":
#                 attack(name, player, enemy_two)
#                 if enemy_two.character_health <= 0:
#                     print()
#                     print(enemy_two.title, "has been killed!")
#                     break
#                 else:
#                     defense(name, player, enemy_one)
#                     defense(name, player, enemy_two)
#             else:
#                 print("You couldn't decide who to attack and stumbled...")
#                 defense(name, player, enemy_one)
#                 defense(name, player, enemy_two)
#     while True:
#         if enemy_one.character_health <= 0 and enemy_two.character_health <= 0:
#             enemies = 1
#             print()
#             print(enemies, "more bandit left.")
#             combat(name, player, enemy_three)
#             break
#         elif enemy_one.character_health <= 0 and enemy_three.character_health <= 0:
#             enemies = 1
#             print()
#             print(enemies, "more bandit left.")
#             combat(name, player, enemy_two)
#             break
#         elif enemy_two.character_health <= 0 and enemy_three.character_health <= 0:
#             enemies = 1
#             print()
#             print(enemies, "more bandit left.")
#             combat(name, player, enemy_one)
#             break







def attack_three(name, player, enemy, enemy_two, enemy_three):
    for attack in range(1,4):
        if attack == 4:
            print()
            print("While deep in thought, you were quickly attacked!")
            break
        else:
            print()
            action = input("Will you Attack, Heal, Counter, or cast a Spell?(Use enter/return to attack)").lower()
            if action == "attack" or action == "":
                twenty_sided_die = roll(20)
                round_num = twenty_sided_die + player.die_modifier
                if twenty_sided_die == 20:
                    crit_damage = critical_hit(player.damage_die, player.crit)
                    enemy.character_health = enemy.character_health - crit_damage
                    print("You rolled a natural 20. That's a critical hit!", name, "did", crit_damage, "damage.")
                    if enemy.character_health <= 0:
                        print()
                        print(name, "the", player.title,"has killed the", enemy.title, "!")
                        defense(name, player, enemy_two)
                        defense(name, player, enemy_three)
                        break
                    else:
                        defense(name, player, enemy)
                        defense(name, player, enemy_two)
                        defense(name, player, enemy_three)
                        break
                elif twenty_sided_die == 1:
                    print(name, "rolled a natural 1. You left yourself open for", enemy.title, "to get an additional attack that can't be parried!")
                    round_num = dtwenty(enemy.die_modifier)
                    if round_num >= player.character_armor:
                        round_damage = roll(enemy.damage_die)
                        player.character_health = player.character_health - round_damage
                        print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                        defense(name, player, enemy)
                        defense(name, player, enemy_two)
                        defense(name, player, enemy_three)
                        break
                    else:
                        print("The", enemy.title, "misses", name)
                        defense(name, player, enemy)
                        defense(name, player, enemy_two)
                        defense(name, player, enemy_three)
                        break
                elif round_num >= enemy.character_armor:
                    round_damage = roll(player.damage_die)
                    enemy.character_health = enemy.character_health - round_damage
                    print("You rolled a", round_num, ". That's a hit!", name, "did", round_damage, "damage.")
                    if enemy.character_health <= 0:
                        print()
                        print(name, "the", player.title,"has killed the", enemy.title, "!")
                        defense(name, player, enemy_two)
                        defense(name, player, enemy_three)
                        break
                    else:
                        defense(name, player, enemy)
                        defense(name, player, enemy_two)
                        defense(name, player, enemy_three)
                        break
                else:
                    print("You rolled a", round_num, ". You missed!")
                    defense(name, player, enemy)
                    defense(name, player, enemy_two)
                    defense(name, player, enemy_three)
                    break
            elif action == "heal":
                if player.health_potion > 0:
                    round_healing = healing_die()
                    player.character_health += round_healing
                    player.health_potion = player.health_potion - 1
                    print("You healed", round_healing, ".", name, "has a total health of", player.character_health, ". Your total Health Potions are", player.health_potion)
                    defense(name, player, enemy)
                    defense(name, player, enemy_two)
                    defense(name, player, enemy_three)
                    break
                elif player.health_potion == 0:
                    print("You have used all of your Healing Potions")
                    continue
            elif action == "counter":
                round_num = dtwenty(enemy.die_modifier)
                if round_num < player.character_armor:
                    crit_damage = critical_hit(player.damage_die, player.crit)
                    enemy.character_health = enemy.character_health - crit_damage
                    print(enemy.title, "rolled a", round_num, ".", name, "successfully countered the enemy attack! You did", crit_damage, "damage.")
                    if enemy.character_health <= 0:
                        print()
                        print(name, "the", player.title, "has killed the", enemy.title, "!")
                        defense(name, player, enemy_two)
                        defense(name, player, enemy_three)
                        break
                    else:
                        defense(name, player, enemy_two)
                        defense(name, player, enemy_three)
                        break#continue
                else:
                    round_damage = critical_hit(enemy.damage_die, enemy.crit)
                    player.character_health = player.character_health - round_damage
                    print("Failed to counter. The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                    if player.character_health <= 0:
                        print()
                        print(name, "the", player.title, "DIED!!!! Game Over.")
                        exit()
                    else:
                        defense(name, player, enemy_two)
                        defense(name, player, enemy_three)
                        break#continue
            elif action == "spell":
                if player.spell > 0:
                    round_num = dtwenty(player.die_modifier)
                    if round_num >= enemy.character_armor:
                        spell_damage = spell_hit(player.damage_die)
                        enemy.character_health = enemy.character_health - spell_damage
                        player.spell = player.spell - 1
                        print("Your spell hits, causing the enemy to stagger.", name, "did", spell_damage, "damage. You have", player.spell, "spells left.")
                        if enemy.character_health <= 0:
                            print()
                            print(name, "the", player.title, "has killed", enemy.title, "!")           
                            defense(name, player, enemy_two)
                            defense(name, player, enemy_three)
                            break
                        else:
                            defense(name, player, enemy_two)
                            defense(name, player, enemy_three)
                            break#continue
                    else:
                        player.spell = player.spell - 1
                        print(name, "your spell missed. You have", player.spell, "spells left.")
                        defense(name, player, enemy)
                        defense(name, player, enemy_two)
                        defense(name, player, enemy_three)
                        break
                elif player.spell == 0:
                    print("You have no spells.")
                    continue
            elif action == "status":
                print("AC is", player.character_armor, ", current Health is", player.character_health, ", and max damage is", player.damage_die, ".") 
                print("You also have", player.health_potion, "Health Potions,", player.parry, "parry manuevers, and", player.spell, "spells.")
                continue
            else:
                continue
            if enemy.character_health <= 0:
                print()
                print(name, "the", player.title,"has killed the", enemy.title, "!")
                defense(name, player, enemy_two)
                defense(name, player, enemy_three)
                break
            # else:
            #     pass



def attack_two(name, player, enemy, enemy_two):
    for attack in range(1,4):
        if attack == 4:
            print()
            print("While deep in thought, you were quickly attacked!")
            break
        else:
            print()
            action = input("Will you Attack, Heal, Counter, or cast a Spell?(Use enter/return to attack)").lower()
            if action == "attack" or action == "":
                twenty_sided_die = roll(20)
                round_num = twenty_sided_die + player.die_modifier
                if twenty_sided_die == 20:
                    crit_damage = critical_hit(player.damage_die, player.crit)
                    enemy.character_health = enemy.character_health - crit_damage
                    print("You rolled a natural 20. That's a critical hit!", name, "did", crit_damage, "damage.")
                    if enemy.character_health <= 0:
                        print()
                        print(name, "the", player.title,"has killed the", enemy.title, "!")
                        defense(name, player, enemy_two)
                        break
                    else:
                        defense(name, player, enemy)
                        defense(name, player, enemy_two)
                        break
                elif twenty_sided_die == 1:
                    print(name, "rolled a natural 1. You left yourself open for", enemy.title, "to get an additional attack that can't be parried!")
                    round_num = dtwenty(enemy.die_modifier)
                    if round_num >= player.character_armor:
                        round_damage = roll(enemy.damage_die)
                        player.character_health = player.character_health - round_damage
                        print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                        defense(name, player, enemy)
                        defense(name, player, enemy_two)
                        break
                    else:
                        print("The", enemy.title, "misses", name)
                        defense(name, player, enemy)
                        defense(name, player, enemy_two)
                        break
                elif round_num >= enemy.character_armor:
                    round_damage = roll(player.damage_die)
                    enemy.character_health = enemy.character_health - round_damage
                    print("You rolled a", round_num, ". That's a hit!", name, "did", round_damage, "damage.")
                    if enemy.character_health <= 0:
                        print()
                        print(name, "the", player.title,"has killed the", enemy.title, "!")
                        defense(name, player, enemy_two)
                        break
                    else:
                        defense(name, player, enemy)
                        defense(name, player, enemy_two)
                        break
                else:
                    print("You rolled a", round_num, ". You missed!")
                    defense(name, player, enemy)
                    defense(name, player, enemy_two)
                    break
            elif action == "heal":
                if player.health_potion > 0:
                    round_healing = healing_die()
                    player.character_health += round_healing
                    player.health_potion = player.health_potion - 1
                    print("You healed", round_healing, ".", name, "has a total health of", player.character_health, ". Your total Health Potions are", player.health_potion)
                    defense(name, player, enemy)
                    defense(name, player, enemy_two)
                    break
                elif player.health_potion == 0:
                    print("You have used all of your Healing Potions")
                    continue
            elif action == "counter":
                round_num = dtwenty(enemy.die_modifier)
                if round_num < player.character_armor:
                    crit_damage = critical_hit(player.damage_die, player.crit)
                    enemy.character_health = enemy.character_health - crit_damage
                    print(enemy.title, "rolled a", round_num, ".", name, "successfully countered the enemy attack! You did", crit_damage, "damage.")
                    if enemy.character_health <= 0:
                        print()
                        print(name, "the", player.title, "has killed the", enemy.title, "!")
                        defense(name, player, enemy_two)
                        break
                    else:
                        defense(name, player, enemy_two)
                        break#continue
                else:
                    round_damage = critical_hit(enemy.damage_die, enemy.crit)
                    player.character_health = player.character_health - round_damage
                    print("Failed to counter. The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                    if player.character_health <= 0:
                        print()
                        print(name, "the", player.title, "DIED!!!! Game Over.")
                        exit()
                    else:
                        defense(name, player, enemy_two)
                        break#continue
            elif action == "spell":
                if player.spell > 0:
                    round_num = dtwenty(player.die_modifier)
                    if round_num >= enemy.character_armor:
                        spell_damage = spell_hit(player.damage_die)
                        enemy.character_health = enemy.character_health - spell_damage
                        player.spell = player.spell - 1
                        print("Your spell hits, causing the enemy to stagger.", name, "did", spell_damage, "damage. You have", player.spell, "spells left.")
                        if enemy.character_health <= 0:
                            print()
                            print(name, "the", player.title, "has killed", enemy.title, "!")           
                            defense(name, player, enemy_two)
                            break
                        else:
                            defense(name, player, enemy_two)
                            break#continue
                    else:
                        player.spell = player.spell - 1
                        print(name, "your spell missed. You have", player.spell, "spells left.")
                        defense(name, player, enemy)
                        defense(name, player, enemy_two)
                        break
                elif player.spell == 0:
                    print("You have no spells.")
                    continue
            elif action == "status":
                print("AC is", player.character_armor, ", current Health is", player.character_health, ", and max damage is", player.damage_die, ".") 
                print("You also have", player.health_potion, "Health Potions,", player.parry, "parry manuevers, and", player.spell, "spells.")
                continue
            else:
                continue
            if enemy.character_health <= 0:
                print()
                print(name, "the", player.title,"has killed the", enemy.title, "!")
                defense(name, player, enemy_two)
                break




