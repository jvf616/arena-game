from game.core.dice import dtwenty, roll

class Army:
    def __init__(
        self, 
        soldiers,
        defense,
        offense,
        archers,
        mages,
        mage_spells,
        catapult_amount,
        catapult_ammo,       
    ):
        self.soldiers = soldiers
        self.defense = defense
        self.offense = offense
        self.archers = archers
        self.mages = mages
        self.mage_spells = mage_spells
        self.catapult_amount = catapult_amount
        self.catapult_ammo = catapult_ammo

class Catapult:
    def __init__(
        self,
        health,
        strength,
    ):
        self.health = health
        self.strength = strength

def catapult_attack(army, catapult):
    catapult_strike = dtwenty(army.offense)
    if catapult_strike >= catapult.strength:
        damage = roll(20) + army.offense
        catapult.health -= damage
        print("A hit!", damage, "damage")
    else:
        print("A miss!")
        pass

def catapult_combat(army, enemy, catapult):
    if army.catapult_amount == 3:
        catapult_attack(army, catapult)
        catapult_attack(army, catapult)
        catapult_attack(army, catapult)
        if catapult.health <= 0:
            enemy.catapult_amount -= 1
            print("Catapult was destroyed!")
    elif army.catapult_amount == 2:
        catapult_attack(army, catapult)
        catapult_attack(army, catapult)
        if catapult.health <= 0:
            enemy.catapult_amount -= 1
            print("Catapult was destroyed!")
    elif army.catapult_amount == 1:
        catapult_attack(army, catapult)
        if catapult.health <= 0:
            enemy.catapult_amount -= 1
            print("Catapult was destroyed!")
    else: 
        print("There are no more catapults.")
        pass


def catapults(army, enemy, catapult_one, catapult_two, catapult_three):
    if catapult_one.health > 0 and catapult_two.health > 0 and catapult_three.health > 0:
        print()
        print("There are", enemy.catapult_amount, "enemy catapults.")
        which_catapult = input("Which catapult do you attack? One, Two, or Three?").lower()
        if which_catapult == "one" or which_catapult == "1":
            catapult_combat(army, enemy, catapult_one)
        elif which_catapult == "two" or which_catapult == "2":
            catapult_combat(army, enemy, catapult_two)
        elif which_catapult == "three" or which_catapult == "3":
            catapult_combat(army, enemy, catapult_three)
    elif catapult_one.health <= 0 and catapult_two.health <= 0 and catapult_three.health <= 0:
        print()
        print("There are no more enemy catapults, so you aim for the remaining army.")
        catapult_strike = army.catapult_amount * dtwenty(army.offense)
        if catapult_strike >= (enemy.defense - 5):
            damage = roll(20) + army.offense
            enemy.soldiers -= damage
            print("A hit! You killed", damage, "soldiers")
        else:
            print("A miss!")
            pass
    elif catapult_one.health <= 0 and catapult_two.health <= 0:
        print()
        print("There is one enemy catapult left.")
        catapult_combat(army, enemy, catapult_three)
    elif catapult_one.health <= 0 and catapult_three.health <= 0:
        print()
        print("There is one enemy catapult left.")
        catapult_combat(army, enemy, catapult_two)
    elif catapult_two.health <= 0 and catapult_three.health <= 0:
        print()
        print("There is one enemy catapult left.")
        catapult_combat(army, enemy, catapult_one)
    elif catapult_one.health <= 0:
        print()
        print("There are", enemy.catapult_amount, "enemy catapults.")
        which_catapult = input("Which catapult do you attack? Two or Three?").lower()
        if which_catapult == "two" or which_catapult == "2":
            catapult_combat(army, enemy, catapult_two)
        elif which_catapult == "three" or which_catapult == "3":
            catapult_combat(army, enemy, catapult_three)
    elif catapult_two.health <= 0:
        print()
        print("There are", enemy.catapult_amount, "enemy catapults.")
        which_catapult = input("Which catapult do you attack? One or Three?").lower()
        if which_catapult == "one" or which_catapult == "1":
            catapult_combat(army, enemy, catapult_one)    
        elif which_catapult == "three" or which_catapult == "3":
            catapult_combat(army, enemy, catapult_three)
    elif catapult_three.health <= 0:
        print()
        print("There are", enemy.catapult_amount, "enemy catapults.")
        which_catapult = input("Which catapult do you attack? One or Two?").lower()
        if which_catapult == "one" or which_catapult == "1":
            catapult_combat(army, enemy, catapult_one)
        elif which_catapult == "two" or which_catapult == "2":
            catapult_combat(army, enemy, catapult_two)

def mage_attack_catapult(army, enemy, catapult):
    army.mage_spells -= 1
    twenty_sided_die = roll(20)
    mages = twenty_sided_die + army.offense
    if twenty_sided_die == 1:
        print("You rolled a natural 1, none of your mages spells hit their mark.")
    elif twenty_sided_die == 20:
        damage = army.mages + army.offense
        catapult.health -= damage
        print("You rolled a natural 20, all of your mages spells hit their mark and damaged the catapult for", damage, ".")                        
    elif mages >= enemy.defense:
        damage = roll(army.mages) + army.offense
        catapult.health -= damage
        print("You rolled a", mages, ". Most of your mages aim is true and damaged the catapult for", damage, ".")
    else:
        damage = int((roll(army.mages) + army.offense) / 2)
        catapult.health -= damage
        print("You rolled a", mages, ". Some of your mages aim is true and damaged the catapult for", damage, ".")
    if army.catapult_ammo < 1:
        army.catapult_ammo += 0.5
    if catapult.health <= 0:
        enemy.catapult_amount -= 1
        print("Enemy catapult destroyed!")



def mage_vs_catapult(army, enemy, catapult_one, catapult_two, catapult_three):
    if catapult_one.health > 0 and catapult_two.health > 0 and catapult_three.health > 0:
        print()
        print("There are", enemy.catapult_amount, "enemy catapults.")
        which_catapult = input("Which catapult do you attack? One, Two, or Three?").lower()
        if which_catapult == "one" or which_catapult == "1":
            mage_attack_catapult(army, enemy, catapult_one)
        elif which_catapult == "two" or which_catapult == "2":
            mage_attack_catapult(army, enemy, catapult_two)
        elif which_catapult == "three" or which_catapult == "3":
            mage_attack_catapult(army, enemy, catapult_three)
    elif catapult_one.health <= 0 and catapult_two.health <= 0 and catapult_three.health <= 0:
        print()
        print("There are no more catapults, so your mages attack the soldiers remaining.")
    elif catapult_one.health <= 0 and catapult_two.health <= 0:
        print()
        print("There is one enemy catapult left.")
        mage_attack_catapult(army, enemy, catapult_three)
    elif catapult_one.health <= 0 and catapult_three.health <= 0:
        print()
        print("There is one enemy catapult left.")
        mage_attack_catapult(army, enemy, catapult_two)
    elif catapult_two.health <= 0 and catapult_three.health <= 0:
        print()
        print("There is one enemy catapult left.")
        mage_attack_catapult(army, enemy, catapult_one)
    elif catapult_one.health <= 0:
        print()
        print("There are", enemy.catapult_amount, "enemy catapults.")
        which_catapult = input("Which catapult do you attack? Two or Three?").lower()
        if which_catapult == "two" or which_catapult == "2":
            mage_attack_catapult(army, enemy, catapult_two)
        elif which_catapult == "three" or which_catapult == "3":
            mage_attack_catapult(army, enemy, catapult_three)
    elif catapult_two.health <= 0:
        print()
        print("There are", enemy.catapult_amount, "enemy catapults.")
        which_catapult = input("Which catapult do you attack? One or Three?").lower()
        if which_catapult == "one" or which_catapult == "1":
            mage_attack_catapult(army, enemy, catapult_one)    
        elif which_catapult == "three" or which_catapult == "3":
            mage_attack_catapult(army, enemy, catapult_three)
    elif catapult_three.health <= 0:
        print()
        print("There are", enemy.catapult_amount, "enemy catapults.")
        which_catapult = input("Which catapult do you attack? One or Two?").lower()
        if which_catapult == "one" or which_catapult == "1":
            mage_attack_catapult(army, enemy, catapult_one)
        elif which_catapult == "two" or which_catapult == "2":
            mage_attack_catapult(army, enemy, catapult_two)


def seige_combat(army, enemy, army_catapult_one, army_catapult_two, army_catapult_three, enemy_catapult_one, enemy_catapult_two, enemy_catapult_three):
    while True:
        print()
        if army.catapult_ammo == 1 and army.catapult_amount > 0:
            print("Your catapults are ready and loaded.")
        action = input("Do you unleash your Archers, Mages, or Catapults?(Use enter/return for archers)").lower()
        if action == "archers" or action == "":
            twenty_sided_die = roll(20)
            archers = twenty_sided_die + army.offense
            if twenty_sided_die == 1:
                print("You rolled a natural 1, none of your archers hit their mark.")
            elif twenty_sided_die == 20:
                damage = army.archers + army.offense
                enemy.soldiers -= damage
                print("You rolled a natural 20, all of your archers hit their mark and killed", damage, "enemy soldiers.")
            elif archers >= enemy.defense:
                damage = roll(army.archers) + army.offense
                enemy.soldiers -= damage
                print("You rolled a", archers, ". Most of your archers aim is true and killed", damage, "enemy soldiers.")
            else:
                damage = int((roll(army.archers) + army.offense) / 2)
                enemy.soldiers -= damage
                print("You rolled a", archers, ". Some of your archers aim is true and killed", damage, "enemy soldiers.")
            if army.catapult_ammo < 1:
                army.catapult_ammo += 0.5
        elif action == "mages":
            print()
            if army.mage_spells > 0:
                catapult_or_soldier = input("Do the mages attack the enemy Catapults or Soldiers?").lower()
                if catapult_or_soldier == "soldiers":
                    army.mage_spells -= 1
                    twenty_sided_die = roll(20)
                    mages = twenty_sided_die + army.offense
                    if twenty_sided_die == 1:
                        print("You rolled a natural 1, none of your mages spells hit their mark.")
                    elif twenty_sided_die == 20:
                        damage = army.mages + army.offense
                        enemy.soldiers -= damage
                        print("You rolled a natural 20, all of your mages spells hit their mark and killed", damage, "enemy soldiers.")
                    elif mages >= enemy.defense:
                        damage = roll(army.mages) + army.offense
                        enemy.soldiers -= damage
                        print("You rolled a", mages, ". Most of your mages aim is true and killed", damage, "enemy soldiers.")
                    else:
                        damage = int((roll(army.mages) + army.offense) / 2)
                        enemy.soldiers -= damage
                        print("You rolled a", mages, ". Some of your mages aim is true and killed", damage, "enemy soldiers.")
                    if army.catapult_ammo < 1:
                        army.catapult_ammo += 0.5
                if catapult_or_soldier == "catapults":
                    mage_vs_catapult(army, enemy, enemy_catapult_one, enemy_catapult_two, enemy_catapult_three)
            else:
                print("Your mages are too weak to cast anymore spells...")
                continue
        elif action == "catapults":
            if army.catapult_ammo < 1:
                print()
                print("Your catapults are still reloading")
            elif army.catapult_amount == 0: 
                print()
                print("Your catapults are destroyed.")
                continue
            else:
                catapults(army, enemy, enemy_catapult_one, enemy_catapult_two, enemy_catapult_three)
                army.catapult_ammo -= 1
        elif action == "status":
            if army.catapult_ammo == 1:
                print("You have", army.soldiers, "soldiers left.")
                print("The mages have", army.mage_spells, "spells left.")
                print("There are", army.catapult_amount, "catapults left and are loaded")
                continue
            else:
                print("You have", army.soldiers, "soldiers left.")
                print("The mages have", army.mage_spells, "spells left.")
                print("There are", army.catapult_amount, "catapults left and are not loaded")
                continue
        else:
            continue
        if enemy.soldiers < 100:
            print()
            print("The enemy army is in full retreat!")
            break
        print()
        print("The enemy army attacks")
        if enemy.catapult_amount > 0:
            if enemy.catapult_ammo == 1:
                if army.catapult_amount == 3:
                    if army_catapult_one.health > 0:
                        print("The enemy army fires their catapults at your catapult one...")
                        catapult_combat(enemy, army, army_catapult_one)
                        enemy.catapult_ammo -= 1
                        continue
                elif army.catapult_amount == 2:
                    if army_catapult_two.health > 0:
                        print("The enemy army fires their catapults at your catapult two...")
                        catapult_combat(enemy, army, army_catapult_two)
                        enemy.catapult_ammo -= 1
                        continue
                elif army.catapult_amount == 1:
                    if army_catapult_three.health > 0:
                        print("The enemy army fires their catapults at your catapult three...")
                        catapult_combat(enemy, army, army_catapult_three)
                        enemy.catapult_ammo -= 1
                        continue
                elif army.catapult_amount == 0:
                    print("You have no more catapults, so the enemy aims for the remaining army.")
                    catapult_strike = dtwenty(enemy.offense)
                    if catapult_strike >= (army.defense - 5):
                        damage = enemy.catapult_amount * dtwenty(enemy.offense)
                        army.soldiers -= damage
                        print("A hit! They killed", damage, "soldiers")
                        enemy.catapult_ammo -= 1
                        continue
                    else:
                        print("A miss!")
                        enemy.catapult_ammo -= 1
                        continue
            else:
                enemy.catapult_ammo += 0.5
        else:
            pass
        if enemy.mage_spells > 0:
            use_spell = roll(6)
            if use_spell >= 4:
                enemy.mage_spells -= 1
                twenty_sided_die = roll(20)
                mages = twenty_sided_die + enemy.offense
                if twenty_sided_die == 1:
                    print("They rolled a natural 1, none of their mages spells hit their mark.")
                    continue
                elif twenty_sided_die == 20:
                    damage = enemy.mages + enemy.offense
                    army.soldiers -= damage
                    print("They rolled a natural 20, all of their mages spells hit their mark and killed", damage, "of your soldiers.")
                    continue
                elif mages >= army.defense:
                    damage = roll(enemy.mages) + enemy.offense
                    army.soldiers -= damage
                    print("They rolled a", mages, ". Most of their mages aim is true and killed", damage, "of your soldiers.")
                    continue
                else:
                    damage = int((roll(enemy.mages) + enemy.offense) / 2)
                    army.soldiers -= damage
                    print("They rolled a", mages, ". Some of their mages aim is true and killed", damage, "of your soldiers.")
                    continue
            else:
                pass
        twenty_sided_die = roll(20)
        archers = twenty_sided_die + enemy.offense
        if twenty_sided_die == 1:
            print("They rolled a natural 1, none of their archers hit their mark.")
        elif twenty_sided_die == 20:
            damage = enemy.archers + enemy.offense
            army.soldiers -= damage
            print("They rolled a natural 20, all of their archers hit their mark and killed", damage, "soldiers.")
        elif archers >= army.defense:
            damage = roll(enemy.archers) + enemy.offense
            army.soldiers -= damage
            print("They rolled a", archers, ". Most of their archers aim is true and killed", damage, "soldiers.")
        else:
            damage = int((roll(enemy.archers) + enemy.offense) / 2)
            army.soldiers -= damage
            print("They rolled a", archers, ". Some of their archers aim is true and killed", damage, "soldiers.")
        if army.soldiers < 100:
            print()
            print("The gates are broken, the enemy army is in the city!")
            break
        


