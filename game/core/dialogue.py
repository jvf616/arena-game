from game.core.dice import dtwenty


#for all levels
def introduce_rules():
    print()
    print("Welcome adventurer!") 
    print("Before you begin learn about the classes and actions.")
    skip = input("If you're already an experienced player, type Skip or s.").lower()
    if skip == "skip" or skip == "s":
        pass
    else:
        print()
        print("A class determines your Armor Class, Health Points, Damage Die,") 
        input("amount of Health Potions, times you can Parry, and amount of Spells.")
        print()
        print("In Combat you have three actions, and two special attacks:")
        print("Actions include: Attack with damage die, use a Health Potion, or Counter an attack.")
        input("Special attacks include Spells and Parry.")
        print()
        print("A Counter is if the enemy rolls lower than your Armor Class, you hit them with extra damage.") 
        input("But if the enemy hits you, they hit with extra damage as well.")
        print()
        print("Spells are a special attack that does double damage.") 
        input("The spell must beat the enemy armor class to hit and you loose a spell each time it's used.")
        print()
        print("Parry is a special attack that allows you to deflect an incoming attack and do half damage.")
        input("The Parry is only used if successful. You get a 50/50 chance of success.")
        print()
        print("Similar to D&D, this game uses a 20 sided die to determine if you hit someone") 
        print("with an attack or spell or successfully parry. Damage is determined by damage die,")
        input("which include an 8, 10, and 12 sided die. They are different for each class.")
        print()
        print("All input options will be upper case! You don't need to capitalize your input.") 
        input("Use enter/return when instructed, just be mindful each time you press enter/return.")


#for the Arena
def before_the_fight(name, player, enemy):
    while True:
        ready = input("Are you ready to fight? Yes or No?").lower()
        if ready == "yes" or ready == "y":
            print()
            print("The", enemy.title, "has entered the arena,", name, ", prepare for combat!")
            break
        elif ready == "no" or ready == "n":
            print()
            print("Then you are a coward", name, "...there will be no glory for you!")
            exit()
        elif ready == "status":
            print("AC is", player.character_armor, ", current Health is", player.character_health, ", and max damage is", player.damage_die, ".") 
            print("You also have", player.health_potion, "Health Potions,", player.parry, "parry manuevers, and", player.spell, "spells.")
            continue
        else:
            print()
            print("Can't decide what to do? Don't worry,", name, ", a decision will be made for you...")
            print("The", enemy.title, "has entered the arena,", name, ", prepare for combat!")
            break


def confronting_the_master(name, player):
    print()
    print("You enter the balcony and the Master of the Arena is staring at you, surprise in their eyes.")
    decision = input("Do you Spare them or do you get Vengence?").lower()
    if decision == "spare":
        print()
        print("As you walk away, letting go of hate, the Master of the Arena lunges towards you with a knife.")
        input("Use a Parry or cast a Spell to counter the attack!(press enter or return to see if you're successful)")
        if player.parry > 0 and player.spell > 0:
            print()
            input("You used the last of your strength to reverse the Master of the Arena's attack and send them flying off the balcony.")
            print()
            input("You look over and see the Master dead, with the crowd chearing for you!")
            print()
            print(name, "have defeated the Master of the Arena!", name, "the", player.title, "is the new Master of the Arena!")
            print("Congrtulations! You have won the Arena. Now present the next champion.")
            print("(Continue the story by typing in 'python mster.py')")
            print()
            exit()
        else:
            print("you can't parry or cast a spell.")
            print()
            print("You were so close...but the Master got the jump on you and killed", name, "the", player.title, "! Game Over.")
            exit()
    elif  decision == "vengence":
        print()
        print("You have defeated the Master of the Arena!", name, "the", player.title, "is the new Master of the Arena!")
        print()
        print("Congrtulations! You have won the Arena. Now present the next champion.")




#for level two

def introduce_level_two_rules(player):
    print()
    print("Congratulations!! You've made it to level two", player.name, "!")
    print("There are new mechanics introduced to the game.")
    skip = input("If you're already an experienced player, type Skip or s.").lower()
    if skip == "skip" or skip == "s":
        pass
    else:
        print()
        print("Level two of the game introduces three new aspects that will be encountered:") 
        input("They are Shopping with gold, combating multiple enemies at once, and leveling up.")
        print()
        print("For shopping, you will earn a certain amount of gold during the game.")
        print("Use gold to buy equipment at the Blacksmith or at the Alchemist to increase your stats.")
        input("After you are done with your purchases, you can buy food at the local tavern to increase Health Points.")
        print()
        print("During combat, you may be attacked by multiple enemies at once.") 
        print("Simply choose which enemy to attack, and then continue combat as normal.")
        input("Just know that each enemy will get a swing at you until that enemy is dead.")
        print()
        print("Lastly, you will get to level up in this part of the game where you will increase all attributes") 
        input("You will also get to choose if you want to increase your amount of parries or spells.")

def introduce_level_three_rules(player):
    print()
    print("Congratulations!! You've made it to level three", player.name, "!")
    print("There are new mechanics introduced to the game.")
    skip = input("If you're already an experienced player, type Skip or s.").lower()
    if skip == "skip" or skip == "s":
        pass
    else:
        print()
        print("Level three introduces combat with an army, where you command specific soldiers")
        input("Seige combat has three types of actions: Catapults, Mages, and Archers")
        print()
        print("You have three catapults that attack in unison, taking two turns to reload after firing.")
        input("They first target the opposing enemies catapult, and once they're destroyed will target soldiers.")
        print()
        print("Mages can attack both soldiers and catapults, but have a limited amount of ammo")
        input("Lastly, Archers attack only soldiers but never run out of ammo.")

def tavern(name, player):
    print()
    print("You walk over to a tavern and sit down by the bar, when the bartender asks 'What can I get ya?'")
    for tavern_feast in range(1,5):
        if tavern_feast == 4:
            print()
            print("You've had your fill and are satisfied.")
            break
        else:
            food_or_drink = input("You think for a bit...Food sounds good but so does a Drink. What will you have?").lower()
            if food_or_drink == "food":
                print()
                print("'I got some grub ready, that'll be 10 gold pieces'")
                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                confirm_purchase = input("Do you want to purchase this delicous meal? Yes or No?").lower()
                if confirm_purchase == "yes" or confirm_purchase == "y":
                    if player.gold >= 10:
                        player.gold -= 10
                        print()
                        print("'Here ya go.' Despite the food's look it tastes really great.")
                        player.character_health += 5
                        print("Congratulations! you've gained 5 health points. Your health is now at", player.character_health, "HP.")
                        print("You have", player.gold, "gold left.")
                        continue
                    else:
                        print()
                        free_meal = input("'Do ya think ya deserve a free meal?'").lower()
                        if free_meal == "yes":
                            print()
                            print("You try to use your talking skills to get a free meal.")
                            input("Roll to see if you can convince them to sell it to you for less(hit enter/return to continue)")
                            charisma_check = dtwenty(player.die_modifier)
                            print(name, "rolled a", charisma_check, ".")
                            if charisma_check >= 20:
                                print()
                                print("'Ok, I'll give it to you for 5 gold'")
                                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                                discounted_meal = input("Do you want to purchase this delicous meal? Yes or No?").lower()
                                if discounted_meal == "yes":
                                    if player.gold >= 5:
                                        print()
                                        print("'Here ya go.' Despite the foods look it tastes really great.")
                                        player.character_health += 5
                                        player.gold -= 5
                                        print("Congratulations! you've gained 5 health points. Your health is now at", player.character_health, "HP.")
                                        print("You have", player.gold, "gold left.")
                                    else:
                                        print()
                                        print("'I ain't no charity, get out a here freeloader!'")
                                        break
                            else:
                                print()
                                print("'I ain't no charity, get out a here freeloader!'")
                                break
                        else:
                            print()
                            print("'Ya thought right, I'll ask again though.'")
                            continue
                else:
                    print()
                    changed_mind = input("'Not hungry? Maybe a good drink is what you need.' Yes or No?").lower()
                    if changed_mind == "yes" or changed_mind =="y":
                        print()
                        print("'I've got the finest mead, that'll be 5 gold pieces'")
                        print("You look into your pocket to see you have", player.gold, "gold pieces.")
                        confirm_purchase = input("Do you want to purchase this refreshing beverage? Yes or No?").lower()
                        if confirm_purchase == "yes" or confirm_purchase == "y":
                            if player.gold >= 5:
                                player.gold -= 5
                                print()
                                print("'Here ya go.' The mead soothes the soul.")
                                print("You have", player.gold, "gold left.")
                                continue
                            else:
                                print()
                                free_drink = input("'Do ya think ya deserve a free drink?'").lower()
                                if free_drink == "yes" or free_drink == "no":
                                    print()
                                    print("You try to use your talking skills to get a free drink.")
                                    input("Roll to see if you can convince them to sell it to you for less(hit enter/return to continue)")
                                    charisma_check = dtwenty(player.die_modifier)
                                    print(name, "rolled a", charisma_check, ".")
                                    if charisma_check >= 20:
                                        print()
                                        print("'Ok, I'll give it to you for 3 gold'")
                                        print("You look into your pocket to see you have", player.gold, "gold pieces.")
                                        discount_drink = input("Do you buy the discounted drink? Yes or No?").lower()
                                        if discount_drink == "yes" or discount_drink == "y":
                                            player.gold -= 3
                                            print()
                                            print("'Here ya go.' The mead soothes the soul.")
                                            print("You have", player.gold, "gold left.")
                                            continue
                                        else:
                                            print()
                                            print("'If ya ain't going to buy anything then I don't want ya here.'")
                                            break
                                    else:
                                        print()
                                        print("'I ain't no charity, get out a here freeloader!'")
                                        break
                                else:
                                    print()
                                    print("'Ya thought right, I'll ask again though.'")
                                    continue        
                        else:
                            print()
                            print("'If ya don't want anything then leave the bar.'")
                            break
                    else:
                        print()
                        print("'If ya don't want anything then leave the bar.'")
                        break
            elif food_or_drink == "drink":
                print()
                print("'I've got the finest mead, that'll be 5 gold pieces'")
                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                confirm_purchase = input("Do you want to purchase this refreshing beverage? Yes or No?").lower()
                if confirm_purchase == "yes" or confirm_purchase == "y":
                    if player.gold >= 5:
                        player.gold -= 5
                        print()
                        print("'Here ya go.' The mead soothes the soul.")
                        print("You have", player.gold, "gold left.")
                        continue
                    else:
                        print()
                        free_drink = input("'Do ya think ya deserve a free drink?'").lower()
                        if free_drink == "yes" or free_drink == "y":
                            print()
                            print("You try to use your talking skills to get a free meal.")
                            input("Roll to see if you can convince them to sell it to you for less(hit enter/return to continue)")
                            charisma_check = dtwenty(player.die_modifier)
                            print(name, "rolled a", charisma_check, ".")
                            if charisma_check >= 20:
                                print()
                                print("'Ok, I'll give it to you for 3 gold'")
                                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                                discount_drink = input("Do you want to purchase this refreshing beverage? Yes or No?").lower()
                                if discount_drink == "yes" or discount_drink == "y":
                                    if player.gold >= 3:
                                        player.gold -= 3
                                        print()
                                        print("'Here ya go.' The mead soothes the soul.")
                                        print("You have", player.gold, "gold left.")
                                    else:
                                        print()
                                        print("'I ain't no charity, get out a here freeloader!'")
                                        break
                            else:
                                print()
                                print("'I ain't no charity, get out a here freeloader!'")
                                break
                        else:
                            print()
                            print("'Ya thought right, I'll ask again though.'")
                            continue
                else:
                    print()
                    changed_mind = input("'Not thirsty? Maybe a good meal is what you need.' Yes or No?").lower()
                    if changed_mind == "yes" or changed_mind == "y":
                        print()
                        print("'I've got some grub ready, that'll be 10 gold pieces'")
                        print("You look into your pocket to see you have", player.gold, "gold pieces.")
                        confirm_purchase = input("Do you want to purchase this delicious meal? Yes or No?").lower()
                        if confirm_purchase == "yes" or confirm_purchase == "y":
                            if player.gold >= 10:
                                player.gold -= 10
                                print()
                                print("'Here ya go.' Despite the foods look it tastes really great.")
                                player.character_health += 5
                                print("Congratulations! you've gained 5 health points. Your health is now at", player.character_health, "HP.")
                                print("You have", player.gold, "gold left.")
                                continue
                            else:
                                print()
                                free_meal = input("'Do ya think ya deserve a free meal?'").lower()
                                if free_meal == "yes" or free_meal == "y":
                                    print()
                                    print("You try to use your talking skills to get a free meal.")
                                    input("Roll to see if you can convince them to sell it to you for less(hit enter/return to continue)")
                                    charisma_check = dtwenty(player.die_modifier)
                                    print(name, "rolled a", charisma_check, ".")
                                    if charisma_check >= 20:
                                        print()
                                        print("'Ok, I'll give it to you for 5 gold'")
                                        print("You look into your pocket to see you have", player.gold, "gold pieces.")
                                        discounted_meal = input("Do you buy the discounted meal? Yes or No?").lower()
                                        if discounted_meal == "yes" or discounted_meal == "y":
                                            player.gold -= 5
                                            print()
                                            print("'Here ya go.' Despite the foods look it tastes really great.")
                                            player.character_health += 5
                                            player.gold -= 5
                                            print("Congratulations! you've gained 5 health points. Your health is now at", player.character_health, "HP.")
                                            print("You have", player.gold, "gold left.")
                                            continue
                                        else:
                                            print()
                                            print("'If ya ain't going to buy anything then I don't want ya here.'")
                                            break
                                    else:
                                        print()
                                        print("'I ain't no charity, get out a here freeloader!'")
                                        break
                                else:
                                    print()
                                    print("'Ya thought right, I'll ask again though.'")
                                    continue        
                        else:
                            print()
                            print("'If ya don't want anything then leave the bar.'")
                            break
                    else:
                        print()
                        print("'If ya don't want anything then leave the bar.'")
                        break
            else:
                print()
                print("'If ya don't want anything, then give the seat to someone who will'.")
                break


#for level two: master.py
def tribute(name, player):
    print()
    print("'Ah, I see,", name, "the", player.title, ". Once a Champion...now a Master.'")
    print("'What aspirations you have, slaying the old Master of the Arena after they ordered your execution.'")
    input("'Some might say your new title was...ill gotten. But whose to say it wasn't rightfully won.'")
    print()
    print("You ask for their name and title, if they have so much to say about yours.")
    print("'Well, I am the emperor of this fine city, Emperor Maximus!' he says with at the top of his voice.")
    input("'And you need to show the proper respect, I'm here to ask why you haven't sent in your tribute?'")
    print()
    tribute = input("You were unaware that you needed to pay such tribute, do you pay the tribute? Yes or No?").lower()
    if tribute == "yes" or tribute == "y":
        print()
        print("You agree to pay the tribute, unaware of what it is...")
        print("'Marvelous' says the Emperor, 'I expect your tribute to arrive by tomorrow'")
        input("The Emperor exits with guards in tow, leaving you wondering what the tribute is supposed to be.")
        print()
        print("That night, you decide what to give as tribute?")
        what_to_give = input("You can give Gold, a Lion, or a Fighter. What will you give?").lower()
        if what_to_give == "gold":
            print()
            print("What an excellent idea! Everyone loves gold!")
            print("You decide to send gold as tribute, and continue on with your rest.")
        elif what_to_give == "lion":
            print()
            print("What an excellent idea! An exotic pet shall win his favor!")
            print("You decide to send a lion as tribute, and continue on with your rest.")
        elif what_to_give == "fighter":
            print()
            print("What an excellent idea! A new guard shall keep the emperor safe!")
            print("You decide to send a fighter as tribute, and continue on with your rest.")
        else:
            print()
            print("Why should you pay tribute to some rich, spoiled Emperor")
            print("You decide that you should not pay tribute, and continue on with your rest.")
    else:
        print()
        print("You decide to not pay tribute.")
        print("'I see', says the Emperor, 'I do hope you change your mind.'")
        input("The Emperor leaves with guards in tow.")



#For level two: village.py

def leader_or_savior(name, player, old_man):
    print()
    print("As you were fighting, you didn't notice that the villagers gathered around to watch.")
    print("As the Bandit Leader lays dead at your feat, the villagers celebrate!")
    if old_man.character_health <= 0:
        print()
        print("The tavern keeper comes up to you and says 'Thank you for getting rid of our troubles, they won't be missed.'")
        protector_or_not = input("'However, they won't be the last thug to threaten us, will you stay to protect us? Yes or No?'").lower()
        if protector_or_not == "yes" or protector_or_not == "no":
            print()
            print("Before you say anything, the pack of bandits arrive and seeing their Leader dead, one of them says")
            new_leader = input("'You killed our leader, and by our laws that means you're our leader now, do you accept? Yes or No'").lower()
            if new_leader == "yes" or new_leader == "y":
                print()
                print("The bandits cheer while the villagers are shocked!")
                print("You take your new crew of bandits and begin ransacking the village!")
                print("During your pillaging, you kill the tavern keeper and a few village citizens, stealing as much gold as possible")
                print("The Champion of the Arena has become...The Bandit Leader! Scourge of the countryside!")
                print("So concludes another chapter in your epic tale!")
                exit()
            else:
                print()
                print(name, player.title, "says no to their laws!")
                save_the_village = input("The tavern keeper asks again 'Will you protect us?' Yes or No?").lower()
                if save_the_village == "yes" or save_the_village == "y":
                    print()
                    print("The villagers cheer yet again, and together you and them chase out the remaining bandits from the village.")
                    print("Congratulations,", name, "! You are now the Savior of the Village!")
                    print("So concludes another chapter in your epic tale!")
                    exit()
                else:
                    print()
                    print("You owe these people nothing, so you decide to continue your journey, not knowing where it will take you...")
                    print("Unnfortunately, it took you right into the path of the Royal Guard, who arrested you and took you back to the city for execution.")
                    print("Instead of death, they decide to put you back into the arena and strip you of any possesions.")
                    print("Congratulations! You are back where you started...")
                    exit()
        else: 
            print()
            print("Before you say anything, the pack of bandits arrive and seeing their Leader dead, one of them says")
            new_leader = input("'You killed our leader, and by our laws that means you're our leader now, do you accept? Yes or No'").lower()
            if new_leader == "yes" or new_leader == "y":
                print()
                print("The bandits cheer while the villagers are shocked!")
                print("You take your new crew of bandits and begin ransacking the village!")
                print("During your pillaging, you kill the tavern keeper and a few village citizens, stealing as much gold as possible")
                print("The Champion of the Arena has become...The Bandit Leader! Scourge of the countryside!")
                print("So concludes another chapter in your epic tale!")
                exit()
            else:
                print()
                print(name, player.title, "says no to their laws!")
                save_the_village = input("The tavern keeper asks again 'Will you protect us?' Yes or No?").lower()
                if save_the_village == "yes" or save_the_village == "y":
                    print()
                    print("The villagers cheer yet again, and together you and them chase out the remaining bandits from the village.")
                    print("Congratulations,", name, "! You are now the Savior of the Village!")
                    print("So concludes another chapter in your epic tale!")
                    exit()
                else:
                    print()
                    print("You owe these people nothing, so you decide to continue your journey, not knowing where it will take you...")
                    print("Unnfortunately, it took you right into the path of the Royal Guard, who arrested you and took you back to the city for execution.")
                    print("Instead of death, they decide to put you back into the arena and strip you of any possesions.")
                    print("Congratulations! You are back where you started...")
                    exit()
    else:
        print()
        print("The old man comes up to you and says 'Thank you for getting rid of our troubles, they surely won't be missed'")
        protector_or_not = input("'However, they won't be the last thug to threaten us, will you stay to protect us? Yes or No'").lower()
        if protector_or_not == "yes" or protector_or_not == "no":
            print()
            print("Before you say anything, the pack of bandits arrive and seeing their Leader dead, one of them says")
            new_leader = input("'You killed our leader, and by our laws that means you're our leader now, do you accept? Yes or No'").lower()
            if new_leader == "yes" or new_leader == "y":
                print()
                print("The bandits cheer while the villagers are shocked!")
                print("You take your new crew of bandits and begin ransacking the village!")
                print("During your pillaging, you kill the old man, the tavern keeper and a few village citizens, stealing as much gold as possible")
                print("The Champion of the Arena has become...The Bandit Leader! Scourge of the countryside!")
                print("So concludes another chapter in your epic tale!")
                exit()
            else:
                print()
                print(name, player.title, "says no to their laws!")
                save_the_village = input("The old man asks again 'Will you protect us?' Yes or No?").lower()
                if save_the_village == "yes" or save_the_village == "y":
                    print()
                    print("The villagers cheer yet again, and together you and them chase out the remaining bandits from the village.")
                    print("Congratulations,", name, "! You are now the Savior of the Village!")
                    print("So concludes another chapter in your epic tale!")
                    exit()
                else:
                    print()
                    print("You owe these people nothing, so you decide to continue your journey, not knowing where it will take you...")
                    print("Unnfortunately, it took you right into the path of the Royal Guard, who arrested you and took you back to the city for execution.")
                    print("Instead of death, they decide to put you back into the arena and strip you of any possesions.")
                    print("Congratulations! You are back where you started...")
                    exit()
        else: 
            print()
            print("Before you say anything, the pack of bandits arrive and seeing their Leader dead, one of them says")
            new_leader = input("'You killed our leader, and by our laws that means you're our leader now, do you accept? Yes or No'").lower()
            if new_leader == "yes" or new_leader == "y":
                print()
                print("The bandits cheer while the villagers are shocked!")
                print("You take your new crew of bandits and begin ransacking the village!")
                print("During your pillaging, you kill the old man, the tavern keeper and a few village citizens, stealing as much gold as possible")
                print("The Champion of the Arena has become...The Bandit Leader! Scourge of the countryside!")
                print("So concludes another chapter in your epic tale!")
                exit()
            else:
                print()
                print(name, player.title, "says no to their laws!")
                save_the_village = input("The old man asks again 'Will you protect us?' Yes or No?").lower()
                if save_the_village == "yes" or save_the_village == "y":
                    print()
                    print("The villagers cheer yet again, and together you and them chase out the remaining bandits from the village.")
                    print("Congratulations,", name, "! You are now the Savior of the Village!")
                    print("So concludes another chapter in your epic tale!")
                    exit()
                else:
                    print()
                    print("You owe these people nothing, so you decide to continue your journey, not knowing where it will take you...")
                    print("Unnfortunately, it took you right into the path of the Royal Guard, who arrested you and took you back to the city for execution.")
                    print("Instead of death, they decide to put you back into the arena and strip you of any possesions.")
                    print("Congratulations! You are back where you started...")
                    exit()