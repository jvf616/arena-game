class Character:
    def __init__(
        self,
        title,
        character_armor,
        max_health,
        character_health,
        health_potion,
        damage_die,
        parry,
        parry_max,
        spell,
        spell_max,
        crit,
        die_modifier,
        gold,
        name="",
    ):
        self.title = title
        self.character_armor = character_armor
        self.character_health = character_health
        self.max_health = max_health
        self.health_potion = health_potion
        self.damage_die = damage_die
        self.parry = parry
        self.parry_max = parry_max
        self.spell = spell
        self.spell_max = spell_max
        self.crit = crit
        self.die_modifier = die_modifier
        self.gold = gold
        self.name = name

    @property
    def formal_name(self):
        if self.name:
            return f"{self.name} the {self.title}"
        else:
            return self.title

    def __repr__(self):
        # return the string representation for this class
        # I'm using an f-string (makes for super easy string formatting)
        return f"Character('{self.title}')"

def pick_character_class():
    print()
    name = input("What is your name?")

    # create possible character class choices
    character_classes = [
        Character(
            name=name,
            title="Soldier",
            character_armor=16,
            character_health=30,
            max_health=30,
            health_potion=4,
            damage_die=10,
            parry=3,
            parry_max=3,
            spell=0,
            spell_max=0,
            crit=4,
            die_modifier=0,
            gold=0
        ),
        Character(
            name=name,
            title="Barbarian",
            character_armor=13,
            character_health=35,
            max_health=35,
            health_potion=3,
            damage_die=12,
            parry=4,
            parry_max=4,
            spell=0,
            spell_max=0,
            crit=5,
            die_modifier=0,
            gold=0
        ),
        Character(
            name=name,
            title="Shield Master",
            character_armor=17,
            character_health=25,
            max_health=25,
            health_potion=3,
            damage_die=8,
            parry=4,
            parry_max=4,
            spell=0,
            spell_max=0,
            crit=3,
            die_modifier=0,
            gold=0
        ),
        Character(
            name=name,
            title="Wizard",
            character_armor=13,
            character_health=25,
            max_health=25,
            health_potion=4,
            damage_die=10,
            parry=0,
            parry_max=0,
            spell=8,
            spell_max=8,
            crit=4,
            die_modifier=0,
            gold=0
        ),
        Character(
            name=name,
            title="Sorcerer",
            character_armor=14,
            character_health=30,
            max_health=30,
            health_potion=3,
            damage_die=8,
            parry=0,
            parry_max=0,
            spell=6,
            spell_max=6,
            crit=3,
            die_modifier=0,
            gold=0
        ),
        Character(
            name=name,
            title="Warlock",
            character_armor=15,
            character_health=35,
            max_health=35,
            health_potion=2,
            damage_die=12,
            parry=0,
            parry_max=0,
            spell=4,
            spell_max=4,
            crit=5,
            die_modifier=0,
            gold=0
        ),
        Character(
            name=name,
            title="Cleric",
            character_armor=16,
            character_health=30,
            max_health=30,
            health_potion=4,
            damage_die=8,
            parry=2,
            parry_max=2,
            spell=4,
            spell_max=4,
            crit=3,
            die_modifier=0,
            gold=0
        ),
        Character(
            name=name,
            title="Paladin",
            character_armor=17,
            character_health=25,
            max_health=25,
            health_potion=3,
            damage_die=10,
            parry=3,
            parry_max=3,
            spell=3,
            spell_max=3,
            crit=4,
            die_modifier=0,
            gold=0
        ),
        Character(
            name=name,
            title="Battle Mage",
            character_armor=15,
            character_health=30,
            max_health=30,
            health_potion=2,
            damage_die=12,
            parry=4,
            parry_max=4,
            spell=2,
            spell_max=2,
            crit=5,
            die_modifier=0,
            gold=0
        ),
        Character(
            name=name,
            title="Fey",
            character_armor=20,
            character_health=100,
            max_health=100,
            health_potion=10,
            damage_die=20,
            parry=10,
            parry_max=10,
            spell=10,
            spell_max=10,
            crit=10,
            die_modifier=5,
            gold=0
        ),
    ]

    character_class_choices = {}
    for cls in character_classes:
        character_class_choices[cls.title] = cls

    while True:
        print()
        print("Class options are:", ", ".join(character_class_choices))
        print()
        chosen_class_title = input("Which class will you choose: ").title()
        if chosen_class_title not in character_class_choices:
            print("Invalid choice, think carefully: ", chosen_class_title)
            continue
        player = character_class_choices[chosen_class_title]
        if not confirm_class_choice(player):
            print("Choose again")
            continue

        print(f"Welcome, {player.formal_name}!")

        return player

def confirm_class_choice(player):
    print("AC is", player.character_armor, ", Health is", player.character_health, ", and max damage is", player.damage_die, ".") 
    print("You also have", player.health_potion, "Health Potions,", player.parry, "parry manuevers, and", player.spell, "spells.")
    return input("Confirm choice: Yes or No?").lower() == "yes"

# Character(
#     title="The Gladiator",
#     character_armor=8,
#     character_health=15,
#     max_health=15,
#     health_potion=0,
#     damage_die=10,
#     parry=0,
#     spell=0,
#     crit=4,
#     die_modifier=0,
#     gold=0
# ),

# Character(
#     title="The Gladiator",
#     character_armor=12,
#     character_health=25,
#     max_health=25,
#     health_potion=0,
#     damage_die=10,
#     parry=0,
#     spell=0,
#     crit=4,
#     die_modifier=2,
#     gold=0
# ),

# Character(
#     title="The Champion",
#     character_armor=15,
#     character_health=35,
#     max_health=35,
#     health_potion=0,
#     damage_die=10,
#     parry=0,
#     spell=0,
#     crit=4,
#     die_modifier=4,
#     gold=0
# ),

# Character(
#     title="The Lion",
#     character_armor=11,
#     character_health=30,
#     max_health=30,
#     health_potion=0,
#     damage_die=6,
#     parry=0,
#     spell=0,
#     crit=2,
#     die_modifier=2,
#     gold=0
# ),

# Character(
#     title="The Guards",
#     character_armor=13,
#     character_health=50,
#     max_health=50,
#     health_potion=0,
#     damage_die=10,
#     parry=0,
#     spell=0,
#     crit=4,
#     die_modifier=0,
#     gold=0
# ),



