from random import random

def roll(die_modifier):
    random_number = random()
    num = (random_number * die_modifier) + 1
    round_num = int(num)
    return round_num


#d20
def dtwenty(modifier):
    return roll(20) + modifier

# random_number = random()
# num_one_twenty = (random_number * 20) + 1
# round_num = int(num_one_twenty)
# print(round_num)

#healing dice: 2d4 + 2
def healing_die():
    return roll(4) + roll(4) + 2

# random_healing = random()
# first_num_one_four = (random_healing * 4) + 1
# second_num_one_four = (random_healing * 4) + 1
# round_healing = int(first_num_one_four) + int(second_num_one_four) + 2
# print(round_healing)

def critical_hit(damage_die, crit):
    damage = roll(damage_die)
    crit_damage = damage + crit
    return crit_damage

def spell_hit(damage_die):
    damage = roll(damage_die)
    spell_damage = damage * 2
    return spell_damage

def parry_hit(damage_die):
    damage = roll(damage_die)
    parry_hit = (damage / 2) + 1
    parry_damage = round(parry_hit)
    return parry_damage


