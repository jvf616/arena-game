from game.core.dice import roll, dtwenty

class Blacksmith_items:
    def __init__(
        self,
        normal_sword,
        normal_sword_cost,
        great_sword,
        great_sword_cost,
        shield,
        shield_cost,
        armor,
        armor_cost,
    ):
        self.normal_sword = normal_sword
        self.normal_sword_cost = normal_sword_cost
        self.great_sword = great_sword
        self.great_sword_cost = great_sword_cost
        self.shield = shield
        self.shield_cost = shield_cost
        self.armor = armor
        self.armor_cost = armor_cost







# [
#     {
#         "cost": int,
#         "quantity": int,
#         "item": Item,
#     },
# ]
def shopping(name, player):
    print()
    for town_activities in range(1, 4):
        if town_activities == 3:
            print()
            print("The hour is late and you are tired and hungry.", name, "decides to stop shopping for the day.")
            break
        activities = input("Do you visit a Vendor or do you get Food?").lower()
        if activities == "vendor":
            print()
            print("You look around, and two shops look interesting to you: a Blacksmith and an Alchemist.")
            shops = input("Which shop do you go to?").lower()
            if shops == "blacksmith":
                smithy = Blacksmith_items(
                        normal_sword=1,
                        normal_sword_cost=35,
                        great_sword=1,
                        great_sword_cost=75,
                        shield=1,
                        shield_cost=50,
                        armor=1,
                        armor_cost=60,
                )
                blacksmith(name, player, smithy)
                continue
            elif shops == "alchemist":
                alchemy = Alchemist_items(
                    health_potions=3,
                    health_potions_cost=20,
                    scrolls=3,
                    scrolls_cost=30,
                )
                alchemist(name, player, alchemy)
                continue
        elif activities == "food":
            break
        else:
            break



def blacksmith(name, player, smithy):
    print()
    print(name, "enters the Blacksmith shop and sees armor and weaponry covering the walls.")
    print("The Blacksmith sees", name, "and asks 'How can I help you today?'")
    while True:
        sword_shield_armor = input("I have Swords, a Shield, or Armor for sale, which would you like to look at?").lower()
        if sword_shield_armor == "swords":
            print()
            print("'The better the sword, the more damage you can do!'")
            print("You look into your pocket to see you have", player.gold, "gold pieces.")
            print("'I have", smithy.normal_sword, "Normal Sword for", smithy.normal_sword_cost, "gold and", smithy.great_sword, "Great Sword for", smithy.great_sword_cost, "gold.'")
            sword = input("'Which would you like to buy?'").lower()
            if sword == "normal sword":
                if smithy.armor <= 0:
                    print()
                    print("'I'm sorry, but I need to add finishing details to the sword, but I do have other items'")
                    continue
                else:
                    print()
                    print("'That will be", smithy.normal_sword_cost, "gold pieces' says the Blacksmith.")
                    haggle = input("Do you Buy the sword or do you Haggle?").lower()
                    if haggle == "buy":
                        if player.gold >= smithy.normal_sword_cost:
                            player.damage_die += 1
                            player.gold -= smithy.normal_sword_cost
                            smithy.normal_sword -= 1
                            print()
                            print("Congratulations, you bought a Normal Sword: your damage die increased by 1")
                            print("You currently have", player.gold, "gold.")
                            buy_more = input("The Blacksmith says 'Would you like to make another purchase? Yes or No?'").lower()
                            if buy_more == "yes" or buy_more == "y":
                                print()
                                print("'Excellent!'")
                                continue
                            elif buy_more == "no" or buy_more == "n":
                                print()
                                print("'Thank you for your purchase, have a nice day!")
                                break
                        else:
                            print()
                            print("You only have", player.gold, "gold pieces, that's not enough.")
                            print("'Come back when you have more gold to spend'")
                            break
                    if haggle == "haggle":
                        print()
                        print("You try to use your talking skills to get a better deal.")
                        input("Roll to see if you can convince him to sell it to you for less(hit enter/return to continue)")
                        charisma_check = dtwenty(player.die_modifier)
                        print(name, "rolled a", charisma_check, ".")
                        if charisma_check == 22:
                            print()
                            print("'You are very convincing, it's on the house!'")
                            player.damage_die += 1
                            smithy.normal_sword -= 1
                            print()
                            print("Congratulations,", name, "got a Normal Sword: your damage die increased by 1")
                            print("You currently have", player.gold, "gold.")
                            print()
                            buy_more = input("The Blacksmith says 'Would you like to make another purchase? Yes or No?'").lower()
                            if buy_more == "yes" or buy_more == "y":
                                print()
                                print("'Excellent!'")
                                continue
                            elif buy_more == "no" or buy_more == "n":
                                print()
                                print("'Thank you for your purchase, have a nice day!")
                                break
                        elif charisma_check >= 18:
                            smithy.normal_sword_cost -= int(smithy.normal_sword_cost / 2)
                            print()
                            print("'You make a convincing argument, I can sell it for", smithy.normal_sword_cost, ".'")
                            print("You look into your pocket to see you have", player.gold, "gold pieces.")
                            sale = input("'Do we have a deal? Yes or No?").lower()
                            if sale == "yes" or sale == "y":
                                if player.gold >= smithy.normal_sword_cost:
                                    player.damage_die += 1
                                    player.gold -= smithy.normal_sword_cost
                                    smithy.normal_sword -= 1
                                    print()
                                    print("Congratulations, you bought a Normal Sword: your damage die increased by 1")
                                    print("You currently have", player.gold, "gold.")
                                    print()
                                    buy_more = input("The Blacksmith says 'Would you like to make another purchase? Yes or No?'").lower()
                                    if buy_more == "yes" or buy_more == "y":
                                        print()
                                        print("'Excellent!'")
                                        continue
                                    elif buy_more == "no" or buy_more == "n":
                                        print()
                                        print("'Thank you for your purchase, have a nice day!")
                                        break
                                else:
                                    print()
                                    print("You only have", player.gold, "gold pieces, that's not enough.")
                                    print("'Come back when you have more gold to spend!'")
                                    break
                            elif sale == "no" or sale == "no":
                                print()
                                print("'Maybe I can interest you in something else?")
                                continue
                            else:
                                print()
                                print("You took to long to answer, the Blacksmith takes the deal off the table.")
                                continue
                        elif charisma_check >= 13:
                            smithy.normal_sword_cost -= int(smithy.normal_sword_cost / 4)
                            print()
                            print("'You make a convincing argument, I can sell it for", smithy.normal_sword_cost, ".'")
                            print("You look into your pocket to see you have", player.gold, "gold pieces.")
                            sale = input("'Do we have a deal? Yes or No?").lower()
                            if sale == "yes" or sale == "y":
                                if player.gold >= smithy.normal_sword_cost:
                                    player.damage_die += 1
                                    player.gold -= smithy.normal_sword_cost
                                    smithy.normal_sword -= 1
                                    print()
                                    print("Congratulations, you bought a Normal Sword: your damage die increased by 1")
                                    print("You currently have", player.gold, "gold.")
                                    print()
                                    buy_more = input("The Blacksmith says 'Would you like to make another purchase? Yes or No?'").lower()
                                    if buy_more == "yes" or buy_more == "y":
                                        print()
                                        print("'Excellent!'")
                                        continue
                                    elif buy_more == "no" or buy_more == "n":
                                        print()
                                        print("'Thank you for your purchase, have a nice day!")
                                        break
                                else:
                                    print()
                                    print("You only have", player.gold, "gold pieces, that's not enough.")
                                    print("'Come back when you have more gold to spend!'")
                                    break
                            elif sale == "no" or sale == "n":
                                print()
                                print("'Maybe I can interest you in something else?")
                                continue
                            else:
                                print()
                                print("You took to long to answer, the Blacksmith takes the deal off the table.")
                                continue
                        else:
                            print()
                            print("You only have", player.gold, "gold pieces, that's not enough.")
                            print("'Come back when you have more gold to spend!'")
                            break
            elif sword == "great sword":
                if smithy.great_sword <= 0:
                    print()
                    print("'I'm sorry, but I need to add finishing details to the great sword, but I do have other items'")
                    continue
                else:
                    print()
                    print("'That will be", smithy.great_sword_cost, "gold pieces' says the Blacksmith.")
                    haggle = input("Do you Buy the sword or do you Haggle?").lower()
                    if haggle == "buy":
                        if player.gold >= smithy.great_sword_cost:
                            player.damage_die += 2
                            player.gold -= smithy.great_sword_cost
                            smithy.great_sword -= 1
                            print()
                            print("Congratulations, you bought a Great Sword: your damage die increased by 2")
                            print("You currently have", player.gold, "gold.")
                            buy_more = input("The Blacksmith says 'Would you like to make another purchase? Yes or No?'").lower()
                            if buy_more == "yes" or buy_more == "y":
                                print()
                                print("'Excellent!'")
                                continue
                            elif buy_more == "no" or buy_more == "n":
                                print()
                                print("'Thank you for your purchase, have a nice day!")
                                break
                        else:
                            print()
                            print("You only have", player.gold, "gold pieces, that's not enough.")
                            print("'Come back when you have more gold to spend'")
                            break
                    if haggle == "haggle":
                        print()
                        print("You try to use your talking skills to get a better deal.")
                        input("Roll to see if you can convince him to sell it to you for less(hit enter/return to continue)")
                        charisma_check = dtwenty(player.die_modifier)
                        print(name, "rolled a", charisma_check, ".")
                        if charisma_check == 22:
                            print()
                            print("'You are very convincing, it's on the house!'")
                            player.damage_die += 2
                            smithy.great_sword -= 1
                            print()
                            print("Congratulations,", name, "got a Great Sword: your damage die increased by 2")
                            print("You currently have", player.gold, "gold.")
                            print()
                            buy_more = input("The Blacksmith says 'Would you like to make another purchase? Yes or No?'").lower()
                            if buy_more == "yes" or buy_more == "y":
                                print()
                                print("'Excellent!'")
                                continue
                            elif buy_more == "no" or buy_more == "n":
                                print()
                                print("'Thank you for your purchase, have a nice day!")
                                break
                        elif charisma_check >= 18:
                            smithy.great_sword_cost -= int(smithy.great_sword_cost / 2)
                            print()
                            print("'You make a convincing argument, I can sell it for", smithy.great_sword_cost, ".'")
                            print("You look into your pocket to see you have", player.gold, "gold pieces.")
                            sale = input("'Do we have a deal? Yes or No?").lower()
                            if sale == "yes" or sale == "y":
                                if player.gold >= smithy.great_sword_cost:
                                    player.damage_die += 2
                                    player.gold -= smithy.great_sword_cost
                                    smithy.great_sword -= 1
                                    print()
                                    print("Congratulations, you bought a Great Sword: your damage die increased by 2")
                                    print("You currently have", player.gold, "gold.")
                                    print()
                                    buy_more = input("The Blacksmith says 'Would you like to make another purchase? Yes or No?'").lower()
                                    if buy_more == "yes" or buy_more == "y":
                                        print()
                                        print("'Excellent!'")
                                        continue
                                    elif buy_more == "no" or buy_more == "n":
                                        print()
                                        print("'Thank you for your purchase, have a nice day!")
                                        break
                                else:
                                    print()
                                    print("You only have", player.gold, "gold pieces, that's not enough.")
                                    print("'Come back when you have more gold to spend!'")
                                    break
                            elif sale == "no" or sale == "n":
                                print()
                                print("'Maybe I can interest you in something else?'")
                                continue
                            else:
                                print()
                                print("You took to long to answer, the Blacksmith takes the deal off the table.")
                                continue
                        elif charisma_check >= 13:
                            smithy.great_sword_cost -= int(smithy.great_sword_cost / 4)
                            print()
                            print("'You make a convincing argument, I can sell it for", smithy.great_sword_cost, ".'")
                            print("You look into your pocket to see you have", player.gold, "gold pieces.")
                            sale = input("'Do we have a deal? Yes or No?'").lower()
                            if sale == "yes" or sale == "y":
                                if player.gold >= smithy.great_sword_cost:
                                    player.damage_die += 2
                                    player.gold -= smithy.great_sword_cost
                                    smithy.great_sword -= 1
                                    print()
                                    print("Congratulations, you bought a Great Sword: your damage die increased by 1")
                                    print("You currently have", player.gold, "gold.")
                                    print()
                                    buy_more = input("The Blacksmith says 'Would you like to make another purchase? Yes or No?'").lower()
                                    if buy_more == "yes" or buy_more == "y":
                                        print()
                                        print("'Excellent!'")
                                        continue
                                    elif buy_more == "no" or buy_more == "n":
                                        print()
                                        print("'Thank you for your purchase, have a nice day!")
                                        break
                                else:
                                    print()
                                    print("You only have", player.gold, "gold pieces, that's not enough.")
                                    print("'Come back when you have more gold to spend!'")
                                    break
                            elif sale == "no" or sale == "n":
                                print()
                                print("'Maybe I can interest you in something else?")
                                continue
                            else:
                                print()
                                print("You took to long to answer, the Blacksmith takes the deal off the table.")
                                continue
                        else:
                            print()
                            print("You failed tho convince the Blacksmith to mark down the price and became aggitated.")
                            print("'Come back when you have more gold to spend!'")
                            break
                    else:
                        print()
                        print("'Maybe you need to rethink your choices'")
                        continue
            else:
                print()
                print("'Maybe you need to rethink your choices'")
                continue
        elif sword_shield_armor == "shield":
            if smithy.shield <= 0:
                print()
                print("'I'm sorry, but I need to add finishing details to the shield, but I do have other items'")
                continue
            else:
                print()
                print("'With a good shield, you can parry the most deadly blows!'")
                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                print("'I have", smithy.shield, "for", smithy.shield_cost, "gold.'")
                shield = input("'Would you like to buy a shield?' Yes or No?").lower()
                if shield == "yes" or shield == "y":
                    print()
                    print("'That will be", smithy.shield_cost, "gold pieces' says the Blacksmith.")
                    haggle = input("Do you Buy the shield or do you Haggle?").lower()
                    if haggle == "buy":
                        if player.gold >= smithy.shield_cost:
                            player.parry += 2
                            player.gold -= smithy.shield_cost
                            smithy.shield -= 1
                            print()
                            print("Congratulations, you bought a shield: your parries increased by 2")
                            print("You currently have", player.gold, "gold.")
                            print()
                            buy_more = input("The Blacksmith says 'Would you like to make another purchase? Yes or No?'").lower()
                            if buy_more == "yes" or buy_more == "y":
                                print()
                                print("'Excellent!'")
                                continue
                            elif buy_more == "no" or buy_more == "n":
                                print()
                                print("'Thank you for your purchase, have a nice day!'")
                                break
                        else:
                            print()
                            print("You only have", player.gold, "gold pieces, that's not enough.")
                            print("'Come back when you have more gold to spend'")
                            break
                    if haggle == "haggle":
                        print()
                        print("You try to use your talking skills to get a better deal.")
                        input("Roll to see if you can convince him to sell it to you for less(hit enter/return to continue)")
                        charisma_check = dtwenty(player.die_modifier)
                        print(name, "rolled a", charisma_check, ".")
                        if charisma_check == 22:
                            print()
                            print("'You are very convincing, it's on the house!'")
                            player.parry += 2
                            smithy.shield -= 1
                            print()
                            print("Congratulations,", name, "got a shield: your parries increased by 2")
                            print("You currently have", player.gold, "gold.")
                            print()
                            buy_more = input("The Blacksmith says 'Would you like to make another purchase? Yes or No?'").lower()
                            if buy_more == "yes" or buy_more == "y":
                                print()
                                print("'Excellent!'")
                                continue
                            elif buy_more == "no" or buy_more == "n":
                                print()
                                print("'Thank you for your purchase, have a nice day!")
                                break
                        elif charisma_check >= 18:
                            smithy.shield_cost -= int(smithy.shield_cost / 2)
                            print()
                            print("'You make a convincing argument, I can sell it for", smithy.shield_cost, ".'")
                            print("You look into your pocket to see you have", player.gold, "gold pieces.")
                            sale = input("'Do we have a deal? Yes or No?").lower()
                            if sale == "yes" or sale == "y":
                                if player.gold >= smithy.shield_cost:
                                    player.parry += 2
                                    player.gold -= smithy.shield_cost
                                    smithy.shield -= 1
                                    print()
                                    print("Congratulations, you bought a shield: your parries increased by 2")
                                    print("You currently have", player.gold, "gold.")
                                    print()
                                    buy_more = input("The Blacksmith says 'Would you like to make another purchase? Yes or No?'").lower()
                                    if buy_more == "yes" or buy_more == "y":
                                        print()
                                        print("'Excellent!'")
                                        continue
                                    elif buy_more == "no" or buy_more == "n":
                                        print()
                                        print("'Thank you for your purchase, have a nice day!")
                                        break
                                else:
                                    print()
                                    print("You only have", player.gold, "gold pieces, that's not enough.")
                                    print("'Come back when you have more gold to spend!'")
                                    break
                            elif sale == "no" or sale == "n":
                                print()
                                print("'Maybe I can interest you in something else?")
                                continue
                            else:
                                print()
                                print("You took to long to answer, the Blacksmith takes the deal off the table.")
                                continue
                        elif charisma_check >= 13:
                            smithy.shield_cost -= int(smithy.shield_cost / 4)
                            print()
                            print("'You make a convincing argument, I can sell it for", smithy.shield_cost, ".'")
                            print("You look into your pocket to see you have", player.gold, "gold pieces.")
                            sale = input("'Do we have a deal? Yes or No?").lower()
                            if sale == "yes" or sale == "y":
                                if player.gold >= smithy.shield_cost:
                                    player.parry += 2
                                    player.gold -= smithy.shield_cost
                                    smithy.shield -= 1
                                    print()
                                    print("Congratulations, you bought a shield: your parries increased by 2")
                                    print("You currently have", player.gold, "gold.")
                                    print()
                                    buy_more = input("The Blacksmith says 'Would you like to make another purchase? Yes or No?'").lower()
                                    if buy_more == "yes" or buy_more == "y":
                                        print()
                                        print("'Excellent!'")
                                        continue
                                    elif buy_more == "no" or buy_more == "n":
                                        print()
                                        print("'Thank you for your purchase, have a nice day!")
                                        break
                                else:
                                    print()
                                    print("You only have", player.gold, "gold pieces, that's not enough.")
                                    print("'Come back when you have more gold to spend!'")
                                    break
                            elif sale == "no" or sale == "n":
                                print()
                                print("'Maybe I can interest you in something else?")
                                continue
                            else:
                                print()
                                print("You took to long to answer, the Blacksmith takes the deal off the table.")
                                continue
                        else:
                            print()
                            print("You failed tho convince the Blacksmith to mark down the price and became aggitated.")
                            print("'Come back when you have more gold to spend!'")
                            break
                    else:
                        print()
                        print("'Maybe you need to rethink your choices'")
                        continue
                else:
                    print()
                    print("'Maybe you need to rethink your choices'")
                    continue
        elif sword_shield_armor == "armor":
            if smithy.armor <= 0:
                print()
                print("'I'm sorry, but I need to add finishing details to the armor, but I do have other items'")
                continue
            else:
                print()
                print("'A strong piece of armor can protect you from most attacks!'")
                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                print("'I have", smithy.armor, "for", smithy.armor_cost, "gold.'")
                armor = input("'Would you like to buy armor?' Yes or No?").lower()
                if armor == "yes" or armor == "y":
                    print()
                    print("'That will be", smithy.armor_cost, "gold pieces' says the Blacksmith.")
                    haggle = input("Do you Buy the armor or do you Haggle?").lower()
                    if haggle == "buy":
                        if player.gold >= smithy.armor_cost:
                            player.character_armor += 1
                            player.gold -= smithy.armor_cost
                            smithy.armor -= 1
                            print()
                            print("Congratulations, you bought armor: your AC increased by 1")
                            print("You currently have", player.gold, "gold.")
                            print()
                            buy_more = input("The Blacksmith says 'Would you like to make another purchase? Yes or No?'").lower()
                            if buy_more == "yes" or buy_more == "y":
                                print()
                                print("'Excellent!'")
                                continue
                            elif buy_more == "no" or buy_more == "n":
                                print()
                                print("'Thank you for your purchase, have a nice day!")
                                break
                        else:
                            print()
                            print("You only have", player.gold, "gold pieces, that's not enough.")
                            print("'Come back when you have more gold to spend'")
                            break
                    if haggle == "haggle":
                        print()
                        print("You try to use your talking skills to get a better deal.")
                        input("Roll to see if you can convince him to sell it to you for less(hit enter/return to continue)")
                        charisma_check = dtwenty(player.die_modifier)
                        print(name, "rolled a", charisma_check, ".")
                        if charisma_check == 22:
                            print()
                            print("'You are very convincing, it's on the house!'")
                            player.character_armor += 1
                            smithy.armor -= 1
                            print()
                            print("Congratulations,", name, "got armor: your AC increased by 1")
                            print("You currently have", player.gold, "gold.")
                            print()
                            buy_more = input("The Blacksmith says 'Would you like to make another purchase? Yes or No?'").lower()
                            if buy_more == "yes" or buy_more == "y":
                                print()
                                print("'Excellent!'")
                                continue
                            elif buy_more == "no" or buy_more == "n":
                                print()
                                print("'Thank you for your purchase, have a nice day!")
                                break
                            continue
                        elif charisma_check >= 18:
                            smithy.armor_cost -= int(smithy.armor_cost / 2)
                            print()
                            print("'You make a convincing argument, I can sell it for", smithy.armor_cost, ".'")
                            print("You look into your pocket to see you have", player.gold, "gold pieces.")
                            sale = input("'Do we have a deal? Yes or No?").lower()
                            if sale == "yes" or sale == "y":
                                if player.gold >= smithy.armor_cost:
                                    player.character_armor += 1
                                    player.gold -= smithy.armor_cost
                                    smithy.armor -= 1
                                    print()
                                    print("Congratulations, you bought armor: your AC increased by 1")
                                    print("You currently have", player.gold, "gold.")
                                    print()
                                    buy_more = input("The Blacksmith says 'Would you like to make another purchase? Yes or No?'").lower()
                                    if buy_more == "yes" or buy_more == "y":
                                        print()
                                        print("'Excellent!'")
                                        continue
                                    elif buy_more == "no" or buy_more == "n":
                                        print()
                                        print("'Thank you for your purchase, have a nice day!")
                                        break
                                else:
                                    print()
                                    print("You only have", player.gold, "gold pieces, that's not enough.")
                                    print("'Come back when you have more gold to spend!'")
                                    break
                            elif sale == "no":
                                print()
                                print("'Maybe I can interest you in something else?")
                                continue
                            else:
                                print()
                                print("You took to long to answer, the Blacksmith takes the deal off the table.")
                                continue
                        elif charisma_check >= 13:
                            smithy.armor_cost -= int(smithy.armor_cost / 4)
                            print()
                            print("'You make a convincing argument, I can sell it for", smithy.armor_cost, ".'")
                            print("You look into your pocket to see you have", player.gold, "gold pieces.")
                            sale = input("'Do we have a deal? Yes or No?").lower()
                            if sale == "yes" or sale == "y":
                                if player.gold >= smithy.armor_cost:
                                    player.character_armor += 1
                                    player.gold -= smithy.armor_cost
                                    smithy.armor -= 1
                                    print()
                                    print("Congratulations, you bought armor: your AC increased by 1")
                                    print("You currently have", player.gold, "gold.")
                                    print()
                                    buy_more = input("The Blacksmith says 'Would you like to make another purchase? Yes or No?'").lower()
                                    if buy_more == "yes" or buy_more == "y":
                                        print()
                                        print("'Excellent!'")
                                        continue
                                    elif buy_more == "no" or buy_more == "n":
                                        print()
                                        print("'Thank you for your purchase, have a nice day!")
                                        break
                                else:
                                    print()
                                    print("You only have", player.gold, "gold pieces, that's not enough.")
                                    print("'Come back when you have more gold to spend!'")
                                    break
                            elif sale == "no" or sale == "n":
                                print()
                                print("'Maybe I can interest you in something else?")
                                continue
                            else:
                                print()
                                print("You took to long to answer, the Blacksmith takes the deal off the table.")
                                continue
                        else:
                            print()
                            print("You failed tho convince the Blacksmith to mark down the price and became aggitated.")
                            print("'Come back when you have more gold to spend!'")
                            break
                    else:
                        print()
                        print("'Maybe you need to rethink your choices'")
                        continue
                else:
                    print()
                    print("'Maybe you need to rethink your choices'")
                    continue
        else:
            print()
            print("'If you aren't interested but change your mind feel free to come back.'")
            break
        
                        

class Alchemist_items:
    def __init__(
        self,
        health_potions,
        health_potions_cost,
        scrolls,
        scrolls_cost,
    ):
        self.health_potions = health_potions
        self.health_potions_cost = health_potions_cost
        self.scrolls = scrolls
        self.scrolls_cost = scrolls_cost

def alchemist(name, player, alchemy):
    print()
    print(name, "enters the Alchemist's shop and sees potions and magical items.")
    print("The Alchemist sees", name, "and asks 'How can I help you today?'") 
    while True:
        potion_scroll = input("'I have the most potent of Potions and the most powerful Scrolls, what would you like to buy?'").lower()
        if alchemy.health_potions <= 0:
            print()
            print("I'm sorry, I am out of prepared potions, I'll have more soon.")
            continue
        else:
            if potion_scroll == "potions":
                print()
                print("'Health potions can heal you when you are seconds away from death.'")
                amount = input("'I currently have a few potions ready. Would you like One, Two, or Three?'").lower()
                if amount == "one" or amount == "1":
                    print()
                    print("You look into your pocket to see you have", player.gold, "gold pieces.")
                    print("'I have", (alchemy.health_potions - 2), "for", alchemy.health_potions_cost, "gold.'")
                    health_potions = input("'Would you like to buy 1 health potion? Yes or No?'").lower()
                    if health_potions == "yes" or health_potions == "y":
                        print()
                        print("'That will be", alchemy.health_potions_cost, "gold pieces' says the Alchemist.")
                        haggle = input("Do you Buy the health potion or do you Haggle?").lower()
                        if haggle == "buy":
                            if player.gold >= alchemy.health_potions_cost:
                                player.health_potion += 1
                                player.gold -= alchemy.health_potions_cost
                                alchemy.health_potions -= 1
                                print()
                                print("Congratulations, you bought 1 health potion: you now have", player.health_potion, "health potions")
                                print("You currently have", player.gold, "gold.")
                                buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                if buy_more == "yes" or buy_more == "y":
                                    print()
                                    print("'Excellent!'")
                                    continue
                                elif buy_more == "no" or buy_more == "n":
                                    print()
                                    print("'Thank you for your purchase, have a nice day!")
                                    break
                            else:
                                print()
                                print("You only have", player.gold, "gold pieces, that's not enough.")
                                print("'Come back when you have more gold to spend'")
                                break
                        elif haggle == "haggle":
                            print()
                            print("You try to use your talking skills to get a better deal.")
                            input("Roll to see if you can convince him to sell it to you for less(hit enter/return to continue)")
                            charisma_check = dtwenty(player.die_modifier)
                            print(name, "rolled a", charisma_check, ".")
                            if charisma_check == 22:
                                print()
                                print("'You are very convincing, it's on the house!'")
                                player.health_potion += 1
                                alchemy.health_potions -= 1
                                print()
                                print("Congratulations,", name, "bought 1 health potions: you now have", player.health_potion, "health potions.")
                                print("You currently have", player.gold, "gold.")
                                print()
                                buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                if buy_more == "yes" or buy_more == "y":
                                    print()
                                    print("'Excellent!'")
                                    continue
                                elif buy_more == "no" or buy_more == "n":
                                    print()
                                    print("'Thank you for your purchase, have a nice day!")
                                    break
                            elif charisma_check >= 18:
                                alchemy.health_potions_cost -= int(alchemy.health_potions_cost / 2)
                                print()
                                print("'You make a convincing argument, I can sell it for", alchemy.health_potions_cost , ".'")
                                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                                sale = input("'Do we have a deal? Yes or No?").lower()
                                if sale == "yes" or sale == "y":
                                    if player.gold >= alchemy.health_potions_cost:
                                        player.health_potion += 1
                                        player.gold -= alchemy.health_potions_cost 
                                        alchemy.health_potions -= 1
                                        print()
                                        print("Congratulations, you bought 1 health potion: you now have", player.health_potion, "health potions")
                                        print("You currently have", player.gold, "gold.")
                                        print()
                                        buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                        if buy_more == "yes" or buy_more == "y":
                                            print()
                                            print("'Excellent!'")
                                            continue
                                        elif buy_more == "no" or buy_more == "n":
                                            print()
                                            print("'Thank you for your purchase, have a nice day!")
                                            break
                                    else:
                                        print()
                                        print("You only have", player.gold, "gold pieces, that's not enough.")
                                        print("'Come back when you have more gold to spend!'")
                                        break
                                elif sale == "no" or sale == "n":
                                    print()
                                    print("'Maybe I can interest you in something else?")
                                    continue
                                else:
                                    print()
                                    print("You took to long to answer, the Alchemist takes the deal off the table.")
                                    continue
                            elif charisma_check >= 13:
                                alchemy.health_potions_cost -= int(alchemy.health_potions_cost / 4)
                                print()
                                print("'You make a convincing argument, I can sell it for", alchemy.health_potions_cost, ".'")
                                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                                sale = input("'Do we have a deal? Yes or No?").lower()
                                if sale == "yes" or sale == "y":
                                    if player.gold >= alchemy.health_potions_cost:
                                        player.health_potion += 1
                                        player.gold -= alchemy.health_potions_cost 
                                        alchemy.health_potions -= 1
                                        print()
                                        print("Congratulations, you bought 1 health potion: you now have", player.health_potion, "health potions")
                                        print("You currently have", player.gold, "gold.")
                                        print()
                                        buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                        if buy_more == "yes" or buy_more == "y":
                                            print()
                                            print("'Excellent!'")
                                            continue
                                        elif buy_more == "no" or buy_more == "n":
                                            print()
                                            print("'Thank you for your purchase, have a nice day!")
                                            break
                                    else:
                                        print()
                                        print("You only have", player.gold, "gold pieces, that's not enough.")
                                        print("'Come back when you have more gold to spend!'")
                                        break
                                elif sale == "no" or sale == "n":
                                    print()
                                    print("'Maybe I can interest you in something else?")
                                    continue
                                else:
                                    print()
                                    print("You took to long to answer, the Alchemist takes the deal off the table.")
                                    continue
                            else:
                                print()
                                print("You failed tho convince the Alchemist to mark down the price and became aggitated.")
                                print("'Come back when you have more gold to spend!'")
                                break
                        else:
                            print()
                            print("'Maybe you need to rethink your choices'")
                            continue 
                    else:
                        print()
                        print("'If you'd like to hear the options again.'")
                        continue   
                elif amount == "two" or amount == "2":
                    two_health_potions = alchemy.health_potions_cost * 2
                    print()
                    print("You look into your pocket to see you have", player.gold, "gold pieces.")
                    print("'I have", (alchemy.health_potions - 1), "for", two_health_potions, "gold.'")
                    health_potions = input("'Would you like to buy 2 health potions? Yes or No?'").lower()
                    if health_potions == "yes" or health_potions == "y":
                        print()
                        print("'That will be", two_health_potions, "gold pieces' says the Alchemist.")
                        haggle = input("Do you Buy the health potion or do you Haggle?").lower()
                        if haggle == "buy":
                            if player.gold >= two_health_potions:
                                player.health_potion += 2
                                player.gold -= two_health_potions
                                alchemy.health_potions -= 2
                                print()
                                print("Congratulations, you bought 2 health potions: you now have", player.health_potion, "health potions")
                                print("You currently have", player.gold, "gold.")
                                buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                if buy_more == "yes" or buy_more == "y":
                                    print()
                                    print("'Excellent!'")
                                    continue
                                elif buy_more == "no" or buy_more == "n":
                                    print()
                                    print("'Thank you for your purchase, have a nice day!")
                                    break
                            else:
                                print()
                                print("You only have", player.gold, "gold pieces, that's not enough.")
                                print("'Come back when you have more gold to spend'")
                                break
                        elif haggle == "haggle":
                            print()
                            print("You try to use your talking skills to get a better deal.")
                            input("Roll to see if you can convince him to sell them to you for less(hit enter/return to continue)")
                            charisma_check = dtwenty(player.die_modifier)
                            print(name, "rolled a", charisma_check, ".")
                            if charisma_check == 22:
                                print()
                                print("'You are very convincing, it's on the house!'")
                                player.health_potion += 2
                                alchemy.health_potions -= 2
                                print()
                                print("Congratulations,", name, "bought 2 health potions: you now have", player.health_potion, "health potions.")
                                print("You currently have", player.gold, "gold.")
                                print()
                                buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                if buy_more == "yes" or buy_more == "y":
                                    print()
                                    print("'Excellent!'")
                                    continue
                                elif buy_more == "no" or buy_more == "n":
                                    print()
                                    print("'Thank you for your purchase, have a nice day!")
                                    break
                            elif charisma_check >= 18:
                                two_health_potions -= int(two_health_potions / 2)
                                print()
                                print("'You make a convincing argument, I can sell them for", two_health_potions, ".'")
                                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                                sale = input("'Do we have a deal? Yes or No?").lower()
                                if sale == "yes" or sale == "y":
                                    if player.gold >= two_health_potions:
                                        player.health_potion += 2
                                        player.gold -= two_health_potions
                                        alchemy.health_potions -= 2
                                        print()
                                        print("Congratulations,", name, "bought 2 health potion: you now have", player.health_potion, "health potions")
                                        print("You currently have", player.gold, "gold.")
                                        print()
                                        buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                        if buy_more == "yes" or buy_more == "y":
                                            print()
                                            print("'Excellent!'")
                                            continue
                                        elif buy_more == "no" or buy_more == "n":
                                            print()
                                            print("'Thank you for your purchase, have a nice day!")
                                            break
                                    else:
                                        print()
                                        print("You only have", player.gold, "gold pieces, that's not enough.")
                                        print("'Come back when you have more gold to spend!'")
                                        break
                                elif sale == "no" or sale == "n":
                                    print()
                                    print("'Maybe I can interest you in something else?")
                                    continue
                                else:
                                    print()
                                    print("You took to long to answer, the Alchemist takes the deal off the table.")
                                    continue
                            elif charisma_check >= 13:
                                two_health_potions -= int(two_health_potions / 4)
                                print()
                                print("'You make a convincing argument, I can sell them for", two_health_potions, ".'")
                                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                                sale = input("'Do we have a deal? Yes or No?").lower()
                                if sale == "yes" or sale =="y":
                                    if player.gold >= two_health_potions:
                                        player.health_potion += 2
                                        player.gold -= two_health_potions 
                                        alchemy.health_potions -= 2
                                        print()
                                        print("Congratulations, you bought 2 health potion: you now have", player.health_potion, "health potions")
                                        print("You currently have", player.gold, "gold.")
                                        print()
                                        buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                        if buy_more == "yes" or buy_more == "y":
                                            print()
                                            print("'Excellent!'")
                                            continue
                                        elif buy_more == "no" or buy_more == "n":
                                            print()
                                            print("'Thank you for your purchase, have a nice day!")
                                            break
                                    else:
                                        print()
                                        print("You only have", player.gold, "gold pieces, that's not enough.")
                                        print("'Come back when you have more gold to spend!'")
                                        break
                                elif sale == "no" or sale == "n":
                                    print()
                                    print("'Maybe I can interest you in something else?")
                                    continue
                                else:
                                    print()
                                    print("You took to long to answer, the Alchemist takes the deal off the table.")
                                    continue
                            else:
                                print()
                                print("You failed tho convince the Alchemist to mark down the price and became aggitated.")
                                print("'Come back when you have more gold to spend!'")
                                break
                        else:
                            print()
                            print("'Maybe you need to rethink your choices'")
                            continue 
                    else:
                        print()
                        print("'If you'd like to hear the options again.'")
                        continue 
                elif amount == "three" or amount == "3":
                    three_health_potions = alchemy.health_potions_cost * 3
                    print()
                    print("You look into your pocket to see you have", player.gold, "gold pieces.")
                    print("'I have", alchemy.health_potions, "for", three_health_potions, "gold.'")
                    health_potions = input("'Would you like to buy 3 health potions? Yes or No?'").lower()
                    if health_potions == "yes" or health_potions == "y":
                        print()
                        print("'That will be", three_health_potions, "gold pieces' says the Alchemist.")
                        haggle = input("Do you Buy the health potion or do you Haggle?").lower()
                        if haggle == "buy":
                            if player.gold >= three_health_potions:
                                player.health_potion += 3
                                player.gold -= three_health_potions
                                alchemy.health_potions -= 3
                                print()
                                print("Congratulations, you bought 3 health potions: you now have", player.health_potion, "health potions")
                                print("You currently have", player.gold, "gold.")
                                print()
                                buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                if buy_more == "yes" or buy_more == "y":
                                    print()
                                    print("'Excellent!'")
                                    continue
                                elif buy_more == "no" or buy_more == "n":
                                    print()
                                    print("'Thank you for your purchase, have a nice day!")
                                    break
                            else:
                                print()
                                print("You only have", player.gold, "gold pieces, that's not enough.")
                                print("'Come back when you have more gold to spend'")
                                break
                        elif haggle == "haggle":
                            print()
                            print("You try to use your talking skills to get a better deal.")
                            input("Roll to see if you can convince him to sell it to you for less(hit enter/return to continue)")
                            charisma_check = dtwenty(player.die_modifier)
                            print(name, "rolled a", charisma_check, ".")
                            if charisma_check == 22:
                                print()
                                print("'You are very convincing, it's on the house!'")
                                player.health_potion += 3
                                alchemy.health_potions -= 3
                                print()
                                print("Congratulations,", name, "bought 2 health potions: you now have", player.health_potion, "health potions.")
                                print("You currently have", player.gold, "gold.")
                                print()
                                buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                if buy_more == "yes" or buy_more == "y":
                                    print()
                                    print("'Excellent!'")
                                    continue
                                elif buy_more == "no" or buy_more == "n":
                                    print()
                                    print("'Thank you for your purchase, have a nice day!")
                                    break
                            elif charisma_check >= 18:
                                three_health_potions -= int(three_health_potions / 2)
                                print()
                                print("'You make a convincing argument, I can sell them for", three_health_potions, ".'")
                                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                                sale = input("'Do we have a deal? Yes or No?").lower()
                                if sale == "yes" or sale == "y":
                                    if player.gold >= three_health_potions:
                                        player.health_potion += 3
                                        player.gold -= three_health_potions
                                        alchemy.health_potions -= 3
                                        print()
                                        print("Congratulations,", name, "bought 3 health potion: you now have", player.health_potion, "health potions")
                                        print("You currently have", player.gold, "gold.")
                                        print()
                                        buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                        if buy_more == "yes" or buy_more == "y":
                                            print()
                                            print("'Excellent!'")
                                            continue
                                        elif buy_more == "no" or buy_more == "n":
                                            print()
                                            print("'Thank you for your purchase, have a nice day!")
                                            break
                                    else:
                                        three_health_potions = (alchemy.health_potions_cost * 3)
                                        print()
                                        print("You only have", player.gold, "gold pieces, that's not enough.")
                                        print("'Come back when you have more gold to spend!'")
                                        break
                                elif sale == "no" or sale == "n":
                                    three_health_potions = (alchemy.health_potions_cost * 3)
                                    print()
                                    print("'Maybe I can interest you in something else?")
                                    continue
                                else:
                                    three_health_potions = (alchemy.health_potions_cost * 3)
                                    print()
                                    print("You took to long to answer, the Alchemist takes the deal off the table.")
                                    continue
                            elif charisma_check >= 13:
                                three_health_potions -= int(three_health_potions / 4)
                                print()
                                print("'You make a convincing argument, I can sell them for", three_health_potions, ".'")
                                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                                sale = input("'Do we have a deal? Yes or No?").lower()
                                if sale == "yes" or sale == "y":
                                    if player.gold >= three_health_potions:
                                        player.health_potion += 3
                                        player.gold -= three_health_potions 
                                        alchemy.health_potions -= 3
                                        print()
                                        print("Congratulations, you bought 3 health potion: you now have", player.health_potion, "health potions")
                                        print("You currently have", player.gold, "gold.")
                                        print()
                                        buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                        if buy_more == "yes" or buy_more == "y":
                                            print()
                                            print("'Excellent!'")
                                            continue
                                        elif buy_more == "no" or buy_more == "n":
                                            print()
                                            print("'Thank you for your purchase, have a nice day!")
                                            break
                                    else:
                                        print()
                                        print("You only have", player.gold, "gold pieces, that's not enough.")
                                        print("'Come back when you have more gold to spend!'")
                                        break
                                elif sale == "no" or sale == "n":
                                    print()
                                    print("'Maybe I can interest you in something else?")
                                    continue
                                else:
                                    print()
                                    print("You took to long to answer, the Alchemist takes the deal off the table.")
                                    continue
                            else:
                                print()
                                print("You failed tho convince the Alchemist to mark down the price and became aggitated.")
                                print("'Come back when you have more gold to spend!'")
                                break
                        else:
                            print()
                            print("'Maybe you need to rethink your choices'")
                            continue 
                    else:
                        print()
                        print("'If you'd like to hear the options again.'")
                        continue 
                else:
                    print()
                    print("I'm sorry, I missheard you, let me ask again.")
                    continue
        if potion_scroll == "scrolls":           
            if alchemy.scrolls <= 0:
                print()
                print("I'm sorry, I am out of prepared scrolls, I'll have more soon.")
                continue
            else:
                print()
                print("'Scrolls contain spells that can be used in combat.'")
                amount = input("'I currently have a few scrolls prepared. Would you like One, Two, or Three?'").lower()
                if amount == "one" or amount == "1":
                    one_scroll_cost = alchemy.scrolls_cost
                    print()
                    print("You look into your pocket to see you have", player.gold, "gold pieces.")
                    print("'I have", (alchemy.scrolls - 2), "for", alchemy.scrolls_cost, "gold.'")
                    scrolls = input("'Would you like to buy 1 scroll? Yes or No?'").lower()
                    if scrolls == "yes" or scrolls == "y":
                        print()
                        print("'That will be", alchemy.scrolls_cost, "gold pieces' says the Alchemist.")
                        haggle = input("Do you Buy the health potion or do you Haggle?").lower()
                        if haggle == "buy":
                            if player.gold >= alchemy.scrolls_cost:
                                player.spell += 1
                                player.gold -= alchemy.scrolls_cost
                                alchemy.scrolls -= 1
                                print()
                                print("Congratulations, you bought 1 scroll: you now have", player.spell, "spells")
                                print("You currently have", player.gold, "gold.")
                                print()
                                buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                if buy_more == "yes" or buy_more == "y":
                                    print()
                                    print("'Excellent!'")
                                    continue
                                elif buy_more == "no" or buy_more == "n":
                                    print()
                                    print("'Thank you for your purchase, have a nice day!")
                                    break
                            else:
                                print()
                                print("You only have", player.gold, "gold pieces, that's not enough.")
                                print("'Come back when you have more gold to spend'")
                                break
                        elif haggle == "haggle":
                            print()
                            print("You try to use your talking skills to get a better deal.")
                            input("Roll to see if you can convince him to sell it to you for less(hit enter/return to continue)")
                            charisma_check = dtwenty(player.die_modifier)
                            print(name, "rolled a", charisma_check, ".")
                            if charisma_check == 22:
                                print()
                                print("'You are very convincing, it's on the house!'")
                                player.spell += 1
                                alchemy.scrolls -= 1
                                print()
                                print("Congratulations,", name, "bought 1 scroll: you now have", player.spell, "spells.")
                                print("You currently have", player.gold, "gold.")
                                print()
                                buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                if buy_more == "yes" or buy_more == "y":
                                    print()
                                    print("'Excellent!'")
                                    continue
                                elif buy_more == "no" or buy_more == "n":
                                    print()
                                    print("'Thank you for your purchase, have a nice day!")
                                    break
                            elif charisma_check >= 18:
                                one_scroll_cost -= int(alchemy.scrolls_cost / 2)
                                print()
                                print("'You make a convincing argument, I can sell it for", alchemy.scrolls_cost , ".'")
                                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                                sale = input("'Do we have a deal? Yes or No?").lower()
                                if sale == "yes" or sale == "y":
                                    if player.gold >= alchemy.scrolls_cost:
                                        player.spell += 1
                                        player.gold -= alchemy.scrolls_cost 
                                        alchemy.scrolls -= 1
                                        print()
                                        print("Congratulations, you bought 1 scroll: you now have", player.spell, "spells")
                                        print("You currently have", player.gold, "gold.")
                                        print()
                                        buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                        if buy_more == "yes" or buy_more == "y":
                                            print()
                                            print("'Excellent!'")
                                            continue
                                        elif buy_more == "no" or buy_more == "n":
                                            print()
                                            print("'Thank you for your purchase, have a nice day!")
                                            break
                                    else:
                                        print()
                                        print("You only have", player.gold, "gold pieces, that's not enough.")
                                        print("'Come back when you have more gold to spend!'")
                                        break
                                elif sale == "no" or sale == "n":
                                    print()
                                    print("'Maybe I can interest you in something else?")
                                    continue
                                else:
                                    print()
                                    print("You took to long to answer, the Alchemist takes the deal off the table.")
                                    continue
                            elif charisma_check >= 13:
                                one_scroll_cost -= int(alchemy.scrolls_cost / 4)
                                print()
                                print("'You make a convincing argument, I can sell it for", alchemy.scrolls_cost, ".'")
                                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                                sale = input("'Do we have a deal? Yes or No?").lower()
                                if sale == "yes" or sale == "y":
                                    if player.gold >= alchemy.scrolls_cost:
                                        player.spell += 1
                                        player.gold -= alchemy.scrolls_cost 
                                        alchemy.scrolls -= 1
                                        print()
                                        print("Congratulations, you bought 1 scroll: you now have", player.spell, "spells")
                                        print("You currently have", player.gold, "gold.")
                                        print()
                                        buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                        if buy_more == "yes" or buy_more == "y":
                                            print()
                                            print("'Excellent!'")
                                            continue
                                        elif buy_more == "no" or buy_more == "n":
                                            print()
                                            print("'Thank you for your purchase, have a nice day!")
                                            break
                                    else:
                                        two_scrolls_cost = int(alchemy.scrolls_cost * 2)
                                        print()
                                        print("You only have", player.gold, "gold pieces, that's not enough.")
                                        print("'Come back when you have more gold to spend!'")
                                        break
                                elif sale == "no" or sale == "n":
                                    print()
                                    print("'Maybe I can interest you in something else?")
                                    continue
                                else:
                                    print()
                                    print("You took to long to answer, the Alchemist takes the deal off the table.")
                                    continue
                            else:
                                print()
                                print("You failed tho convince the Alchemist to mark down the price and became aggitated.")
                                print("'Come back when you have more gold to spend!'")
                                break
                        else:
                            print()
                            print("'Maybe you need to rethink your choices'")
                            continue 
                    else:
                        print()
                        print("'If you'd like to hear the options again.'")
                        continue   
                if amount == "two" or amount == "2":
                    two_scrolls_cost = alchemy.scrolls_cost * 2
                    print()
                    print("You look into your pocket to see you have", player.gold, "gold pieces.")
                    print("'I have", (alchemy.scrolls - 1), "for", two_scrolls_cost, "gold.'")
                    scrolls = input("'Would you like to buy 2 scrolls? Yes or No?'").lower()
                    if scrolls == "yes" or scrolls == "y":
                        print()
                        print("'That will be", two_scrolls_cost, "gold pieces' says the Alchemist.")
                        haggle = input("Do you Buy the health potion or do you Haggle?").lower()
                        if haggle == "buy":
                            if player.gold >= two_scrolls_cost:
                                player.spell += 2
                                player.gold -= two_scrolls_cost
                                alchemy.scrolls -= 2
                                print()
                                print("Congratulations, you bought 2 scrolls: you now have", player.spell, "spells")
                                print("You currently have", player.gold, "gold.")
                                print()
                                buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                if buy_more == "yes" or buy_more == "y":
                                    print()
                                    print("'Excellent!'")
                                    continue
                                elif buy_more == "no" or buy_more == "n":
                                    print()
                                    print("'Thank you for your purchase, have a nice day!")
                                    break
                            else:
                                print()
                                print("You only have", player.gold, "gold pieces, that's not enough.")
                                print("'Come back when you have more gold to spend'")
                                break
                        elif haggle == "haggle":
                            print()
                            print("You try to use your talking skills to get a better deal.")
                            input("Roll to see if you can convince him to sell it to you for less(hit enter/return to continue)")
                            charisma_check = dtwenty(player.die_modifier)
                            print(name, "rolled a", charisma_check, ".")
                            if charisma_check == 22:
                                print()
                                print("'You are very convincing, it's on the house!'")
                                player.spell += 2
                                alchemy.scrolls -= 2
                                print()
                                print("Congratulations,", name, "bought 2 scrolls: you now have", player.spell, "spells.")
                                print("You currently have", player.gold, "gold.")
                                print()
                                buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                if buy_more == "yes" or buy_more == "y":
                                    print()
                                    print("'Excellent!'")
                                    continue
                                elif buy_more == "no" or buy_more == "n":
                                    print()
                                    print("'Thank you for your purchase, have a nice day!")
                                    break
                            elif charisma_check >= 18:
                                two_scrolls_cost -= int(two_scrolls_cost / 2)
                                print()
                                print("'You make a convincing argument, I can sell them for", two_scrolls_cost, ".'")
                                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                                sale = input("'Do we have a deal? Yes or No?").lower()
                                if sale == "yes" or sale == "y":
                                    if player.gold >= two_scrolls_cost:
                                        player.spell += 2
                                        player.gold -= two_scrolls_cost 
                                        alchemy.scrolls -= 2
                                        print()
                                        print("Congratulations, you bought 2 scrolls: you now have", player.spell, "spells")
                                        print("You currently have", player.gold, "gold.")
                                        print()
                                        buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                        if buy_more == "yes" or buy_more == "y":
                                            print()
                                            print("'Excellent!'")
                                            continue
                                        elif buy_more == "no" or buy_more == "n":
                                            print()
                                            print("'Thank you for your purchase, have a nice day!")
                                            break
                                    else:
                                        print()
                                        print("You only have", player.gold, "gold pieces, that's not enough.")
                                        print("'Come back when you have more gold to spend!'")
                                        break
                                elif sale == "no" or sale == "n":
                                    print()
                                    print("'Maybe I can interest you in something else?")
                                    continue
                                else:
                                    print()
                                    print("You took to long to answer, the Alchemist takes the deal off the table.")
                                    continue
                            elif charisma_check >= 13:
                                two_scrolls_cost -= int(two_scrolls_cost / 4)
                                print()
                                print("'You make a convincing argument, I can sell them for", two_scrolls_cost, ".'")
                                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                                sale = input("'Do we have a deal? Yes or No?").lower()
                                if sale == "yes" or sale == "y":
                                    if player.gold >= two_scrolls_cost:
                                        player.spell += 2
                                        player.gold -= two_scrolls_cost 
                                        alchemy.scrolls -= 2
                                        print()
                                        print("Congratulations, you bought 2 scrolls: you now have", player.spell, "spells")
                                        print("You currently have", player.gold, "gold.")
                                        print()
                                        buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                        if buy_more == "yes" or buy_more == "y":
                                            print()
                                            print("'Excellent!'")
                                            continue
                                        elif buy_more == "no" or buy_more == "n":
                                            print()
                                            print("'Thank you for your purchase, have a nice day!")
                                            break
                                    else:
                                        print()
                                        print("You only have", player.gold, "gold pieces, that's not enough.")
                                        print("'Come back when you have more gold to spend!'")
                                        break
                                elif sale == "no" or sale == "n":
                                    print()
                                    print("'Maybe I can interest you in something else?")
                                    continue
                                else:
                                    print()
                                    print("You took to long to answer, the Alchemist takes the deal off the table.")
                                    continue
                            else:
                                print()
                                print("You failed tho convince the Alchemist to mark down the price and became aggitated.")
                                print("'Come back when you have more gold to spend!'")
                                break
                        else:
                            print()
                            print("'Maybe you need to rethink your choices'")
                            continue 
                    else:
                        print()
                        print("'If you'd like to hear the options again.'")
                        continue
                if amount == "three" or amount == "3":        
                    three_scrolls_cost = alchemy.scrolls_cost * 3
                    print()
                    print("You look into your pocket to see you have", player.gold, "gold pieces.")
                    print("'I have", alchemy.scrolls, "for", three_scrolls_cost, "gold.'")
                    scrolls = input("'Would you like to buy 3 scrolls? Yes or No?'").lower()
                    if scrolls == "yes" or scrolls == "y":
                        print()
                        print("'That will be", three_scrolls_cost, "gold pieces' says the Alchemist.")
                        haggle = input("Do you Buy the health potion or do you Haggle?").lower()
                        if haggle == "buy":
                            if player.gold >= three_scrolls_cost:
                                player.spell += 3
                                player.gold -= three_scrolls_cost
                                alchemy.scrolls -= 3
                                print()
                                print("Congratulations, you bought 3 scrolls: you now have", player.spell, "spells")
                                print("You currently have", player.gold, "gold.")
                                print()
                                buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                if buy_more == "yes" or buy_more == "y":
                                    print()
                                    print("'Excellent!'")
                                    continue
                                elif buy_more == "no" or buy_more == "n":
                                    print()
                                    print("'Thank you for your purchase, have a nice day!")
                                    break
                            else:
                                print()
                                print("You only have", player.gold, "gold pieces, that's not enough.")
                                print("'Come back when you have more gold to spend'")
                                break
                        elif haggle == "haggle":
                            print()
                            print("You try to use your talking skills to get a better deal.")
                            input("Roll to see if you can convince them to sell the scrolls to you for less(hit enter/return to continue)")
                            charisma_check = dtwenty(player.die_modifier)
                            print(name, "rolled a", charisma_check, ".")
                            if charisma_check == 22:
                                print()
                                print("'You are very convincing, it's on the house!'")
                                player.spell += 3
                                alchemy.scrolls -= 3
                                print()
                                print("Congratulations,", name, "bought 3 scrolls: you now have", player.spell, "spells.")
                                print("You currently have", player.gold, "gold.")
                                print()
                                buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                if buy_more == "yes" or buy_more == "y":
                                    print()
                                    print("'Excellent!'")
                                    continue
                                elif buy_more == "no" or buy_more == "n":
                                    print()
                                    print("'Thank you for your purchase, have a nice day!")
                                    break
                            elif charisma_check >= 18:
                                three_scrolls_cost -= int(alchemy.scrolls_cost / 2)
                                print()
                                print("'You make a convincing argument, I can sell them for", three_scrolls_cost, ".'")
                                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                                sale = input("'Do we have a deal? Yes or No?").lower()
                                if sale == "yes" or sale == "y":
                                    if player.gold >= three_scrolls_cost:
                                        player.spell += 3
                                        player.gold -= three_scrolls_cost 
                                        alchemy.scrolls -= 3
                                        print()
                                        print("Congratulations, you bought 3 scrolls: you now have", player.spell, "spells")
                                        print("You currently have", player.gold, "gold.")
                                        print()
                                        buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                        if buy_more == "yes" or buy_more == "y":
                                            print()
                                            print("'Excellent!'")
                                            continue
                                        elif buy_more == "no" or buy_more == "n":
                                            print()
                                            print("'Thank you for your purchase, have a nice day!")
                                            break
                                    else:
                                        print()
                                        print("You only have", player.gold, "gold pieces, that's not enough.")
                                        print("'Come back when you have more gold to spend!'")
                                        break
                                elif sale == "no" or sale == "n":
                                    print()
                                    print("'Maybe I can interest you in something else?")
                                    continue
                                else:
                                    print()
                                    print("You took to long to answer, the Alchemist takes the deal off the table.")
                                    continue
                            elif charisma_check >= 13:
                                three_scrolls_cost -= int(three_scrolls_cost / 4)
                                print()
                                print("'You make a convincing argument, I can sell them for", three_scrolls_cost, ".'")
                                print("You look into your pocket to see you have", player.gold, "gold pieces.")
                                sale = input("'Do we have a deal? Yes or No?").lower()
                                if sale == "yes" or sale == "y":
                                    if player.gold >= three_scrolls_cost:
                                        player.spell += 3
                                        player.gold -= three_scrolls_cost 
                                        alchemy.scrolls -= 3
                                        print()
                                        print("Congratulations, you bought 3 scrolls: you now have", player.spell, "spells")
                                        print("You currently have", player.gold, "gold.")
                                        print()
                                        buy_more = input("The Alchemist says 'Would you like to make another purchase? Yes or No?'").lower()
                                        if buy_more == "yes" or buy_more == "y":
                                            print()
                                            print("'Excellent!'")
                                            continue
                                        elif buy_more == "no" or buy_more == "n":
                                            print()
                                            print("'Thank you for your purchase, have a nice day!")
                                            break
                                    else:
                                        print()
                                        print("You only have", player.gold, "gold pieces, that's not enough.")
                                        print("'Come back when you have more gold to spend!'")
                                        break
                                elif sale == "no" or sale == "n":
                                    print()
                                    print("'Maybe I can interest you in something else?")
                                    continue
                                else:
                                    print()
                                    print("You took to long to answer, the Alchemist takes the deal off the table.")
                                    continue
                            else:
                                print()
                                print("You failed tho convince the Alchemist to mark down the price and became aggitated.")
                                print("'Come back when you have more gold to spend!'")
                                break
                        else:
                            print()
                            print("'Maybe you need to rethink your choices'")
                            continue 
                    else:
                        print()
                        print("'If you'd like to hear the options again.'")
                        continue
                else:
                    print()
                    print("'If you'd like to hear the options again.'")
                    continue
        else:
            print()
            print("If you are not interested right now, feel free to come back another time.")
            break
    







