from game.core.dice import roll, dtwenty, healing_die

from game.core.characters import Character

from game.core.combat import combat, defense, level_up

from game.core.multiple import three_enemies


#Level one obtacles
def leave_the_arena(name, player):
    while True:
        print()
        decision = input("Do you Run for the exit or Fight?").lower()
        if decision == "run":
            print()
            print("You have successfully dodged the arrows. But the gate has closed before you could exit!")
            break
        elif decision == "fight":
            round_num = dtwenty(4)
            if round_num >= player.character_armor:
                round_damage = roll(4)
                player.character_health = player.character_health - round_damage
                if player.character_health > 0:
                    print()
                    print(name, "was hit with an arrow! Your health is now at", player.character_health, ". The archers prepare another volley.")
                    continue
                else:
                    print()
                    print(name, player.title, "was hit with an arrow! You DIED!!! Game Over")
                    exit()
            else:
                print()
                print("You dodged all of the archers arrows. They prepare another volley.")
        elif decision == "status":
            print("AC is", player.character_armor, ", current Health is", player.character_health, ", and max damage is", player.damage_die, ".") 
            print("You also have", player.health_potion, "Health Potions,", player.parry, "parry manuevers, and", player.spell, "spells.")
            continue
        else:
            continue
    gate_health = 10
    while True:
        print()
        print("As arrows fly past", name, ", you try to get passed the gate.")
        print()
        decision_two = input("Do you Open the gate or Break it down.(use enter or return to use break)")
        if decision_two == "open":
            round_num = dtwenty(0)
            if round_num >= 18:
                print()
                print("Someone closed the gate but never locked it. The gate opens!")
                break
            else:
                round_damage = roll(4)
                player.character_health = player.character_health - round_damage
                print()
                print("It won't budge. Try something else", name, ". You were hit by an arrow, your health is at", player.character_health, ".")
                if player.character_health > 0:
                    continue
                else:
                    print()
                    print(name, player.title, "DIED!!! Game Over")
                    exit()
        elif decision_two == "" or decision_two == "break":
            round_num = dtwenty(player.die_modifier)
            if round_num > 5:                
                round_damage = dtwenty(player.damage_die)
                gate_health = gate_health - round_damage
                if gate_health <= 0:
                    print()
                    print("The gate is destroyed!", name, "has escaped the arena floor")
                    break
                else:
                    print()
                    print("The gate looks to be damaged. Keep trying to Break the gate!")
                    continue
            elif round_num <= 5:
                round_num = dtwenty(0)
                if round_num >= player.character_armor:                   
                    round_damage = roll(4)
                    player.character_health = player.character_health - round_damage
                    print()
                    print(name, "was hit by an arrow when trying to break down the door. your health is at", player.character_health, ".")
                    if player.character_health > 0:
                        continue
                    else:
                        print()
                        print(name, player.title, "DIED!!! Game Over")
                        exit()
                else:
                    print()
                    print("You have to swing harder than that. Keep trying!")
                    continue
        elif decision_two == "status":
            print("AC is", player.character_armor, ", current Health is", player.character_health, ", and max damage is", player.damage_die, ".") 
            print("You also have", player.health_potion, "Health Potions,", player.parry, "parry manuevers, and", player.spell, "spells.")
            continue



def the_balcony_door(name, player, enemy):
    while True:
        if enemy.character_health <= 0:
            break
        else:
            pass
        print()
        decision = input("There is a Guard waiting at the door. Do you Intimidate them to leave or Fight?").lower()
        if decision == "intimidate":
            round_num = dtwenty(player.die_modifier)
            if round_num > 15:
                print("The Guard run away cowering, leaving the door unguarded for", name, ".")
                break
            else:
                print("The guard is not afraid of you.")                
                round_num = dtwenty(enemy.die_modifier)
                if round_num >= player.character_armor:  
                    round_damage = roll(enemy.damage_die) * 2
                    player.character_health = player.character_health - round_damage
                    print("The Guard hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                    if player.character_health <= 0:
                        print()
                        print(name, "the", player.title, "DIED!!!! Game Over.")
                        exit()
                    else:
                        continue
                else:
                    print("The Guards missed", name)
                    continue
        elif decision == "status":
            print("AC is", player.character_armor, ", current Health is", player.character_health, ", and max damage is", player.damage_die, ".") 
            print("You also have", player.health_potion, "Health Potions,", player.parry, "parry manuevers, and", player.spell, "spells.")
            continue
        elif decision == "fight":
            combat(name, player, enemy)



#level two obstacles
def spying(name, player, guard_one, guard_two, guard_three, enemies):
    print()
    print("Before leaving the tavern, you see three of the Royal Guard conversing in the back.")
    eavesdrop = input("Do you listen in to their conversation? Yes or No?").lower()
    if eavesdrop == "yes" or eavesdrop == "y":
        print()
        print("You find a hiding spot to listen in on their conversation.")
        print("The smelly guard says 'Let's drink up lads!'")
        input("The tall guard says 'But we got to guard the main gate.'")
        print()
        print("The smelly guard says 'No one is going to try to sneak into the palace, we deserve this!'")
        print("The tall guard shrugs in agreement")
        input("You realize that the front gate may be unguarded, you quickly stand up and leave.")
        print()
        print("Before you exit the door a hand is placed on your shoulder, it's one of the guards.")
        print("'Couldn't help but see you listening in on our conversation, who are you?'")
        fake_name = input("You should give them a fake name, what name do you give?")
        print()
        input("You gave them a fake name, roll to see if you deceived them...")
        deception = dtwenty(player.die_modifier)
        if deception >= 20:
            print()
            print("You rolled a", deception, ".")
            print("The smelly guard says 'Stop bothering the civilian, come and drink!'")
            print("'Right, sorry for the misunderstanding", fake_name, ", thought you were someone else.' says the wise guard")
        elif deception >= 15:
            print()
            print("You rolled a", deception, ".")
            print("'Ok,", fake_name, ", why don't you join us if you're so interested in our conversation' says the wise guard")
            drinks = input("Do you Join or do you Leave?").lower()
            if drinks == "join":
                print()
                print("You sit down with the guards, 'So why so nosy about our conversation?' asks one of them.")
                talk_or_fight = input("Do you Talk your way out of this or do you Attack").lower()
                if talk_or_fight == "talk":
                    print()
                    print("You tell them that you appreciate all the city guard does and wanted to buy them drinks.")
                    input("Roll to see if you can deceive them again.")
                    talk = dtwenty(player.die_modifier)
                    if talk >= 15:
                        if player.gold >= 30:
                            player.gold -= 30
                            print()
                            print("The guards bought your story, you buy them multiple drinks.")
                            print("You currently have", player.gold, "gold left.")
                            input("The guards passed out at the table, this should make it easier to get into the palace.")
                        elif player.gold >= 15:
                            print()
                            print("You can only buy them one round, which makes them ask you a lot of questions")
                            input("Before you can leave, the wise guard realizes something.")
                            print()
                            print("'You're not", fake_name, ", you're", name, "the", player.title, "' says the wise guard.")
                            input("'You're supposed to be dead, but we can correct that, get em lads!'")
                            three_enemies(name, player, guard_one, guard_two, guard_three, enemies)
                            print()
                            print("You just killed three guards in a busy tavern, maybe this can cause a distration")
                        else:
                            print()
                            print("Turns out you don't have enough gold on you to buy them any drinks.")
                            print("'Why would you offer to buy drinks you can't buy, unless you're lying!'")
                            print("'You're not", fake_name, ", you're", name, "the", player.title, "' says the wise guard.")
                            input("'You're supposed to be dead, but we can correct that, get em lads!'")
                            three_enemies(name, player, guard_one, guard_two, guard_three, enemies)
                            print()
                            print("You just killed three guards in a busy tavern, maybe this can cause a distration")
                    else:
                        print()
                        print("'You're not", fake_name, ", you're", name, "the", player.title, "' says the wise guard.")
                        input("'You're supposed to be dead, but we can correct that, get em lads!'")
                        three_enemies(name, player, guard_one, guard_two, guard_three, enemies)
                        print()
                        print("You just killed three guards in a busy tavern, maybe this can cause a distration")
                else:
                    print()
                    print("'You're not", fake_name, ", you're", name, "the", player.title, "' says the wise guard.")
                    input("'You're supposed to be dead, but we can correct that, get em lads!'")
                    three_enemies(name, player, guard_one, guard_two, guard_three, enemies)
                    print()
                    print("You just killed three guards in a busy tavern, maybe this can cause a distration")
            else:
                print()
                print("You ignore their invitation to join, but that's when the wise guard realizes something.")
                print("'You're not", fake_name, ", you're", name, "the", player.title, "' says the wise guard.")
                input("'You're supposed to be dead, but we can correct that, get em lads!'")
                three_enemies(name, player, guard_one, guard_two, guard_three, enemies)
                print()
                print("You just killed three guards in a busy tavern, maybe this can cause a distration")
        else:
            print("You rolled a", deception, ".")
            print("'You're not", fake_name, ", you're", name, "the", player.title, "' says the wise guard.")
            input("'You're supposed to be dead, but we can correct that, get em lads!'")
            three_enemies(name, player, guard_one, guard_two, guard_three, enemies)
            print()
            print("You just killed three guards in a busy tavern, maybe this can cause a distration")
    else:
        print()
        print("You decide it's best to continue on with your mission, killing the emperor")


def confronting_the_emperor(name, player, royal_guard_one, royal_guard_two, royal_guard_three, enemies, champion):
    print()
    print("You find the front gate of the palace to be unguarded, what luck.")
    print("Walking straight into the thrown chamber, you find yourself surrounded by the Royal Guard.")
    input("Emperor Maximus enters the thrown room, making you realize he expected you to be here.")
    print()
    print("'I knew you survived once my assassin didn't return, only a matter of time before you tried to kill me.'")
    print("'I'm afraid, no matter what you sent as tribute, you were always going to die, for you killed my friend to become the Master.'")
    print("'You should have died in the arena, but now, you will die here,", name, "the", player.title, "!'")
    input("You are surrounded by the royal guard, it's time to fight or die!")
    three_enemies(name, player, royal_guard_one, royal_guard_two, royal_guard_three, enemies)
    print()
    print("You slay the Royal Guards, the Emperor gives a slow clap.")
    print("'Fine, kill my guards, and you may have killed the Master's Champion, but lets see you face mine!'")
    input("Walking in to the throne room, you see a massive brute in full clad armor, who then charges at you before you can attack!")
    print()
    defense(name, player, champion)
    combat(name, player, champion)
    print()
    print("The Emperor's face shows it all, he is completely helpless.")
    riches_or_power = input("'I'm sorry,' says the Emperor, 'I'll give you whatever you want: Riches, Power, anything!'").lower()
    if riches_or_power == "riches":
        print()
        print("'Of course, here is the key to the royal treasury'. The Emperor holds out a key.")
        print("As you reach for the key, Emperor Maximus pulls out a knife and slits your throat.")
        print(name, "the", player.title, "DIED!!! Game Over.")
        exit()
    if riches_or_power == "power":
        print()
        print("Of course, I can make you a Duke of this nearby village where you can have control of the entire populace.")
        print("You would just have to give up your title of Master of the Arena...")
        take_it_all = input("Do you want to be a Duke, or do you want real Power.").lower()
        if take_it_all == "duke":
            print()
            print("'Of course, here is the dead to the land'. The Emperor holds out a piece of parchment.")
            print("As you reach for the dead, Emperor Maximus pulls out a knife and slits your throat.")
            print(name, "the", player.title, "DIED!!! Game Over.")
            exit()
        else:
            print()
            print("You tell the Emperor, 'I want your power!'")
            print(name, "the", player.title, "takes their weapon and decapitates the Emperor's head!")
            print("A Champion, a Master, now", name, "the", player.title, "is now an Emperor!")
            print("Congratulations!!! So concludes another chapter in your epic tale!")
            exit()
    else:
        print()
        print("You tell the Emperor, 'I want your head!'")
        print(name, "the", player.title, "takes their weapon and decapitates the Emperor's head!")
        print("A Champion, a Master, now", name, "the", player.title, "is now an Emperor!")
        print("Congratulations!!! So concludes another chapter in your epic tale!")
        exit()



#For level two: village.py
