from game.core.dice import dtwenty, healing_die, roll, critical_hit, spell_hit, parry_hit


def old_combat(name, player, enemy):
    while True:
        print()
        action = input("Will you Attack, Heal, Counter, or cast a Spell?(Use enter/return to attack)").lower()
        if action == "attack" or action == "":
            twenty_sided_die = roll(20)
            round_num = twenty_sided_die + player.die_modifier
            if twenty_sided_die == 20:
                crit_damage = critical_hit(player.damage_die, player.crit)
                enemy.character_health = enemy.character_health - crit_damage
                print("You rolled a natural 20. That's a critical hit!", name, "did", crit_damage, "damage.")
            elif twenty_sided_die == 1:
                print(name, "rolled a natural 1. You left yourself open for", enemy.title, "to get an additional attack that can't be parried!")
                round_num = dtwenty(enemy.die_modifier)
                if round_num >= player.character_armor:
                    round_damage = roll(enemy.damage_die)
                    player.character_health = player.character_health - round_damage
                    print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                else:
                    print("The", enemy.title, "misses", name)
            elif round_num >= enemy.character_armor:
                round_damage = roll(player.damage_die)
                enemy.character_health = enemy.character_health - round_damage
                print("You rolled a", round_num, ". That's a hit!", name, "did", round_damage, "damage.")
            else:
                print("You rolled a", round_num, ". You missed!")
        elif action == "heal":
            if player.health_potion > 0:
                round_healing = healing_die()
                player.character_health += round_healing
                player.health_potion = player.health_potion - 1
                print("You healed", round_healing, ".", name, "has a total health of", player.character_health, ". Your total Health Potions are", player.health_potion)
            elif player.health_potion == 0:
                print("You have used all of your Healing Potions")
                continue
        elif action == "counter":
            round_num = dtwenty(enemy.die_modifier)
            if round_num < player.character_armor:
                crit_damage = critical_hit(player.damage_die, player.crit)
                enemy.character_health = enemy.character_health - crit_damage
                print(enemy.title, "rolled a", round_num, ".", name, "successfully countered the enemy attack! You did", crit_damage, "damage.")
                if enemy.character_health <= 0:
                    print()
                    print(name, "the", player.title, "has killed the", enemy.title, "!")
                    break
                else:
                    continue
            else:
                round_damage = critical_hit(enemy.damage_die, enemy.crit)
                player.character_health = player.character_health - round_damage
                print("Failed to counter. The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                if player.character_health <= 0:
                    print()
                    print(name, "the", player.title, "DIED!!!! Game Over.")
                    exit()
                else:
                    continue
        elif action == "spell":
            if player.spell > 0:
                round_num = dtwenty(player.die_modifier)
                if round_num >= enemy.character_armor:
                    spell_damage = spell_hit(player.damage_die)
                    enemy.character_health = enemy.character_health - spell_damage
                    player.spell = player.spell - 1
                    print("You rolled a", round_num, "Your spell hits, causing the enemy to stagger.", name, "did", spell_damage, "damage. You have", player.spell, "spells left.")
                    if enemy.character_health <= 0:
                        print()
                        print(name, "the", player.title, "has killed", enemy.title, "!")
                        break
                    else:
                        continue
                else:
                    player.spell = player.spell - 1
                    print(name, "your spell missed. You have", player.spell, "spells left.")
            elif player.spell == 0:
                print("You have no spells.")
                continue
        elif action == "status":
            print("AC is", player.character_armor, ", current Health is", player.character_health, ", and max damage is", player.damage_die, ".") 
            print("You also have", player.health_potion, "Health Potions,", player.parry, "parry manuevers, and", player.spell, "spells.")
            continue
        else:
            continue
        if enemy.character_health <= 0:
            print()
            print(name, "the", player.title,"has killed the", enemy.title, "!")
            break
        else:
            pass
        print("The", enemy.title, "swings at", name, "!")
        round_num = dtwenty(enemy.die_modifier)
        if round_num >= player.character_armor:
            if player.parry > 0:
                defense = input("The enemy is going to hit you, will you parry? Yes or No?").lower()
                if defense == "yes" or defense == "y":
                    round_num = dtwenty(0)
                    if round_num > 10:
                        parry_damage = parry_hit(player.damage_die)
                        enemy.character_health = enemy.character_health - parry_damage
                        player.parry = player.parry - 1
                        print("You successfully parried the attack.", name, "did", parry_damage, "damage. You have", player.parry, "parries left.")
                        if enemy.character_health <= 0:
                            print()
                            print(name, "the", player.title, "has killed the", enemy.title, "!")
                            print()
                            break
                        else:
                            continue
                    else:
                        print(name, "failed to parry. You still have", player.parry, "parries left.")
                        round_damage = roll(enemy.damage_die)
                        player.character_health = player.character_health - round_damage
                        print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                elif defense == "no" or defense == "n":
                    round_damage = roll(enemy.damage_die)
                    player.character_health = player.character_health - round_damage
                    print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                else:
                    round_damage = roll(enemy.damage_die)
                    player.character_health = player.character_health - round_damage
                    print(name, "tried to parry but must have misstepped.")
                    print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
            else:
                round_damage = roll(enemy.damage_die)
                player.character_health = player.character_health - round_damage
                print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
        else:
            print("The", enemy.title, "misses", name)
        if player.character_health <= 0:
            print()
            print(name, "the", player.title, "DIED!!!! Game Over.")
            exit()
        else:
            pass
    




def endless(player, enemy):
    name = player.name
    count = 0
    while True:
        if enemy.character_armor == 18:
            count += 1
        else:
            enemy.character_armor += 1
            count += 1
        if enemy.character_health <= 0:
            enemy.character_health = 10
            enemy.character_health = enemy.character_health + (5 * (count - 1))
        if count % 4 == 0:
            enemy.die_modifier += 1
            enemy.parry += 1
            enemy.spell += 1
            enemy.health_potion += 1
        elif count % 8 == 0:
            level_up(name, player)
        print("Enemy health: ", enemy.character_health)
        print("Enemy armor: ", enemy.character_armor)
        print("Enemy strength:", enemy.die_modifier)
        print("Enemy spells:", enemy.spell)
        print("Enemy parries:", enemy.parry)
        print("Enemy health potions", enemy.health_potion)
        print()
        print(name, "has killed", (count - 1), "enemies!")
        combat(name, player, enemy)
    


def level_up(name, player):
    print()
    print("Congratulations!", name, "the", player.title, "has leveled up!")
    print("Your base health, damage die will increase and you'll gain two health potions.")
    input("You will also choose if you want to increase your parries or your spells!(hit enter/return to continue)")
    player.character_armor += 1
    player.character_health += 10
    player.max_health += 10
    player.health_potion += 2
    player.damage_die += 2
    player.crit += 1
    player.die_modifier += 2
    while True:
        print()
        print("Do you wish to practice your Sword skills(increase parries)?")
        decision = input("Or do you wish to practice your Magic(increase spells)?").lower()
        if decision == "sword":
            player.parry += 3
            player.parry_max += 3
            print()
            print("AC is", player.character_armor, ", Health is", player.character_health, ", and max damage is", player.damage_die, ".") 
            print("You also have", player.health_potion, "Health Potions,", player.parry, "parry manuevers, and", player.spell, "spells.")
            print("Lastly, your die modifier is", player.die_modifier, "which increases your dice roll.")
            confirmation = input("Are you ready to continue? Yes or No?").lower()
            if confirmation == "yes" or confirmation == "y":
                break
            else:
                player.parry -= 3
                player.parry_max -= 3
                continue
        elif decision == "magic":
            player.spell += 4
            player.spell_max += 4
            print()
            print("AC is", player.character_armor, ", Health is", player.character_health, ", and max damage is", player.damage_die, ".") 
            print("You also have", player.health_potion, "Health Potions,", player.parry, "parry manuevers, and", player.spell, "spells.")
            print("Lastly, your die modifier is", player.die_modifier, "which increases your dice roll.")
            confirmation = input("Are you ready to continue? Yes or No?").lower()
            if confirmation == "yes" or confirmation == "y":
                break
            else:
                player.spell -= 4
                player.spell_max -= 4
                continue
        else:
            continue



def attack(name, player, enemy):
    for attack in range(1,4):
        if attack == 4:
            print()
            print("While deep in thought, you were quickly attacked!")
            break
        else:
            print()
            action = input("Will you Attack, Heal, Counter, or cast a Spell?(Use enter/return to attack)").lower()
            if action == "attack" or action == "":
                twenty_sided_die = roll(20)
                round_num = twenty_sided_die + player.die_modifier
                if twenty_sided_die == 20:
                    crit_damage = critical_hit(player.damage_die, player.crit)
                    enemy.character_health = enemy.character_health - crit_damage
                    print("You rolled a natural 20. That's a critical hit!", name, "did", crit_damage, "damage.")
                    break
                elif twenty_sided_die == 1:
                    print(name, "rolled a natural 1. You left yourself open for", enemy.title, "to get an additional attack that can't be parried!")
                    round_num = dtwenty(enemy.die_modifier)
                    if round_num >= player.character_armor:
                        round_damage = roll(enemy.damage_die)
                        player.character_health = player.character_health - round_damage
                        print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                        break
                    else:
                        print("The", enemy.title, "misses", name)
                        break
                elif round_num >= enemy.character_armor:
                    if enemy.parry > 0:
                        parry_or_not = roll(6)
                        if parry_or_not >= 4:
                            input("The enemy tries to parry your attack...")
                            round_num = dtwenty(0)
                            if round_num > 10:
                                parry_damage = parry_hit(enemy.damage_die)
                                player.character_health = player.character_health - parry_damage
                                enemy.parry = enemy.parry - 1
                                print("The", enemy.title, "successfully parried your attack. They did", parry_damage, "damage. You have", player.character_health, "health left.")
                                if player.character_health <= 0:
                                    print()
                                    print(name, "the", player.title, "DIED!!!! Game Over.")
                                    exit()
                                else:
                                    break 
                            else:
                                round_damage = roll(player.damage_die)
                                enemy.character_health = enemy.character_health - round_damage
                                print(enemy.title, "failed to parry. You did", round_damage, "damage.")
                                break
                        else:
                            round_damage = roll(player.damage_die)
                            enemy.character_health = enemy.character_health - round_damage
                            print("You rolled a", round_num, ". That's a hit!", name, "did", round_damage, "damage.")
                            break
                    else:
                        round_damage = roll(player.damage_die)
                        enemy.character_health = enemy.character_health - round_damage
                        print("You rolled a", round_num, ". That's a hit!", name, "did", round_damage, "damage.")
                        break
                else:
                    print("You rolled a", round_num, ". You missed!")
                    break
            elif action == "heal":
                if player.health_potion > 0:
                    round_healing = healing_die()
                    player.character_health += round_healing
                    player.health_potion = player.health_potion - 1
                    if player.character_health >= player.max_health:
                        player.character_health = player.max_health
                        print("You are back to max health:", player.character_health)
                        break
                    else:
                        print("You healed", round_healing, ".", name, "has a total health of", player.character_health, ". Your total Health Potions are", player.health_potion)
                        break
                elif player.health_potion == 0:
                    print("You have used all of your Healing Potions")
                    continue
            elif action == "counter":
                round_num = dtwenty(enemy.die_modifier)
                if round_num < player.character_armor:
                    crit_damage = critical_hit(player.damage_die, player.crit)
                    enemy.character_health = enemy.character_health - crit_damage
                    print(enemy.title, "rolled a", round_num, ".", name, "successfully countered the enemy attack! You did", crit_damage, "damage.")
                    if enemy.character_health <= 0:
                        print()
                        print(name, "the", player.title, "has killed the", enemy.title, "!")
                        break
                    else:
                        break#continue
                else:
                    round_damage = critical_hit(enemy.damage_die, enemy.crit)
                    player.character_health = player.character_health - round_damage
                    print("Failed to counter. The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                    if player.character_health <= 0:
                        print()
                        print(name, "the", player.title, "DIED!!!! Game Over.")
                        exit()
                    else:
                        break#continue
            elif action == "spell":
                if player.spell > 0:
                    round_num = dtwenty(player.die_modifier)
                    if round_num >= enemy.character_armor:
                        spell_damage = spell_hit(player.damage_die)
                        enemy.character_health = enemy.character_health - spell_damage
                        player.spell = player.spell - 1
                        print("Your spell hits, causing the enemy to stagger.", name, "did", spell_damage, "damage. You have", player.spell, "spells left.")
                        if enemy.character_health <= 0:
                            print()
                            print(name, "the", player.title, "has killed", enemy.title, "!")
                            break
                        else:
                            break#continue
                    else:
                        player.spell = player.spell - 1
                        print(name, "your spell missed. You have", player.spell, "spells left.")
                        break
                elif player.spell == 0:
                    print("You have no spells.")
                    continue
            elif action == "status":
                print("AC is", player.character_armor, ", current Health is", player.character_health, ", and max damage is", player.damage_die, ".") 
                print("You also have", player.health_potion, "Health Potions,", player.parry, "parry manuevers, and", player.spell, "spells.")
                continue
            else:
                continue
            if enemy.character_health <= 0:
                print()
                print(name, "the", player.title,"has killed the", enemy.title, "!")
                break
            # else:
            #     pass


def defense(name, player, enemy):
    if enemy.health_potion > 0:
        if enemy.character_health <= (enemy.character_health / 2):
            round_healing = healing_die()
            enemy.character_health += round_healing
            enemy.health_potion = enemy.health_potion - 1
            if enemy.character_health >= enemy.max_health:
                enemy.character_health = enemy.max_health
                print("The", enemy.title, "is back at full health")
            else:
                print("The", enemy.title, "healed", round_healing, ".")
        else:
            pass
    else:
        pass
    if enemy.spell > 0:
        use_spell = roll(6)
        if use_spell >= 4:
            round_num = dtwenty(enemy.die_modifier)
            if round_num >= player.character_armor:
                spell_damage = spell_hit(enemy.damage_die)
                player.character_health = player.character_health - spell_damage
                enemy.spell = enemy.spell - 1
                print("The", enemy.title, "casts a spell and hits! They did", spell_damage, "damage.", name, "has", player.character_health, "health left.")
                if player.character_health <= 0:
                    print()
                    print(name, "the", player.title, "DIED!!!! Game Over.")
                    exit()
                else:
                    input("The enemy's spell causes you to stager, leaving you open for another attack...")
                    print("The", enemy.title, "swings at", name, "!")
                    round_num = dtwenty(enemy.die_modifier)
                    if round_num >= player.character_armor:
                        round_damage = roll(enemy.damage_die)
                        player.character_health = player.character_health - round_damage
                        print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                    else:
                        print("The", enemy.title, "misses", name)
                    if player.character_health <= 0:
                        print()
                        print(name, "the", player.title, "DIED!!!! Game Over.")
                        exit()
                    else:
                        pass
            else:
                enemy.spell = enemy.spell - 1
                print("The", enemy.title, "missed with their spell!")
                return
        else:
            pass
    else:
        pass
    print("The", enemy.title, "swings at", name, "!")
    round_num = dtwenty(enemy.die_modifier)
    if round_num >= player.character_armor:
        if player.parry > 0:
            defense = input("The enemy is going to hit you, will you parry? Yes or No?").lower()
            if defense == "yes" or defense == "y":
                round_num = dtwenty(0)
                if round_num > 10:
                    parry_damage = parry_hit(player.damage_die)
                    enemy.character_health = enemy.character_health - parry_damage
                    player.parry = player.parry - 1
                    print("You successfully parried the attack.", name, "did", parry_damage, "damage. You have", player.parry, "parries left.")
                    if enemy.character_health <= 0:
                        print()
                        print(name, "the", player.title, "has killed", enemy.title, "!")
                        print()
                        # break
                    # else:
                        # continue
                else:
                    print(name, "failed to parry. You still have", player.parry, "parries left.")
                    round_damage = roll(enemy.damage_die)
                    player.character_health = player.character_health - round_damage
                    print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
            elif defense == "no" or defense == "n":
                round_damage = roll(enemy.damage_die)
                player.character_health = player.character_health - round_damage
                print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
            else:
                round_damage = roll(enemy.damage_die)
                player.character_health = player.character_health - round_damage
                print(name, "tried to parry but must have misstepped.")
                print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
        else:
            round_damage = roll(enemy.damage_die)
            player.character_health = player.character_health - round_damage
            print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
    else:
        print("The", enemy.title, "misses", name)
    if player.character_health <= 0:
        print()
        print(name, "the", player.title, "DIED!!!! Game Over.")
        exit()
    else:
        pass


def dragon_combat(name, player, enemy):
    while True:
        print()
        action = input("Will you Attack, Heal, Counter, or cast a Spell?(Use enter/return to attack)").lower()
        if action == "attack" or action == "":
            twenty_sided_die = roll(20)
            round_num = twenty_sided_die + player.die_modifier
            if twenty_sided_die == 20:
                crit_damage = critical_hit(player.damage_die, player.crit)
                enemy.character_health = enemy.character_health - crit_damage
                print("You rolled a natural 20. That's a critical hit!", name, "did", crit_damage, "damage.")
            elif twenty_sided_die == 1:
                print(name, "rolled a natural 1. You left yourself open for", enemy.title, "to get an additional attack that can't be parried!")
                round_num = dtwenty(enemy.die_modifier)
                if round_num >= player.character_armor:
                    round_damage = roll(enemy.damage_die)
                    player.character_health = player.character_health - round_damage
                    print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                else:
                    print("The", enemy.title, "misses", name)
            elif round_num >= enemy.character_armor:
                round_damage = roll(player.damage_die)
                enemy.character_health = enemy.character_health - round_damage
                print("You rolled a", round_num, ". That's a hit!", name, "did", round_damage, "damage.")
            else:
                print("You rolled a", round_num, ". You missed!")
        elif action == "heal":
            if player.health_potion > 0:
                round_healing = healing_die()
                player.character_health += round_healing
                player.health_potion = player.health_potion - 1
                print("You healed", round_healing, ".", name, "has a total health of", player.character_health, ". Your total Health Potions are", player.health_potion)
            elif player.health_potion == 0:
                print("You have used all of your Healing Potions")
                continue
        elif action == "counter":
            round_num = dtwenty(enemy.die_modifier)
            if round_num < player.character_armor:
                crit_damage = critical_hit(player.damage_die, player.crit)
                enemy.character_health = enemy.character_health - crit_damage
                print(enemy.title, "rolled a", round_num, ".", name, "successfully countered the enemy attack! You did", crit_damage, "damage.")
                if enemy.character_health <= 0:
                    print()
                    print(name, "the", player.title, "has killed the", enemy.title, "!")
                    break
                else:
                    continue
            else:
                round_damage = critical_hit(enemy.damage_die, enemy.crit)
                player.character_health = player.character_health - round_damage
                print("Failed to counter. The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                if player.character_health <= 0:
                    print()
                    print(name, "the", player.title, "DIED!!!! Game Over.")
                    exit()
                else:
                    continue
        elif action == "spell":
            if player.spell > 0:
                round_num = dtwenty(player.die_modifier)
                if round_num >= enemy.character_armor:
                    spell_damage = spell_hit(player.damage_die)
                    enemy.character_health = enemy.character_health - spell_damage
                    player.spell = player.spell - 1
                    print("You rolled a", round_num, "Your spell hits, causing the enemy to stagger.", name, "did", spell_damage, "damage. You have", player.spell, "spells left.")
                    if enemy.character_health <= 0:
                        print()
                        print(name, "the", player.title, "has killed", enemy.title, "!")
                        break
                    else:
                        continue
                else:
                    player.spell = player.spell - 1
                    print(name, "your spell missed. You have", player.spell, "spells left.")
            elif player.spell == 0:
                print("You have no spells.")
                continue
        elif action == "status":
            print("AC is", player.character_armor, ", current Health is", player.character_health, ", and max damage is", player.damage_die, ".") 
            print("You also have", player.health_potion, "Health Potions,", player.parry, "parry manuevers, and", player.spell, "spells.")
            continue
        else:
            continue
        if enemy.character_health <= 0:
            print()
            print(name, "the", player.title,"has killed the", enemy.title, "!")
            break
        else:
            pass
        if enemy.spell == 1:
            input("The Dragon breathes fire at you!!! Roll to evade!(enter/return to continue)")
            dodge = dtwenty(player.die_modifier)
            enemy.spell -= 1
            print()
            print("You rolled a", dodge)
            if dodge >= 15:
                print(name, "evaded the Dragons fire breath!")
            else:
                fire_damage = spell_hit(enemy.damage_die)
                player.character_health = player.character_health - fire_damage
                print("You were hit by the Dragons fire.", name, "was damaged for", fire_damage, "health points, your health is", player.character_health)
            # print("The dragon breathes fire at you!!!")
            # round_num = dtwenty(enemy.die_modifier)
            # enemy.spell -= 1
            # if round_num >= player.character_armor:
            #     fire_damage = spell_hit(enemy.damage_die)
            #     player.character_health = player.character_health - fire_damage
            #     print("You were hit by the Dragons fire.", name, "was damaged for", fire_damage, "health points, your health is", player.character_health)
            # else:
            #     print(name, "evaded the Dragons fire breath!")
        else:
            recharge = roll(6)
            if recharge == 6:
                enemy.spell += 1
            else:
                pass
            print("The", enemy.title, "swings at", name, "!")
            round_num = dtwenty(enemy.die_modifier)
            if round_num >= player.character_armor:
                if player.parry > 0:
                    defense = input("The enemy is going to hit you, will you parry? Yes or No?").lower()
                    if defense == "yes" or defense == "y":
                        round_num = dtwenty(0)
                        if round_num > 10:
                            parry_damage = parry_hit(player.damage_die)
                            enemy.character_health = enemy.character_health - parry_damage
                            player.parry = player.parry - 1
                            print("You successfully parried the attack.", name, "did", parry_damage, "damage. You have", player.parry, "parries left.")
                            if enemy.character_health <= 0:
                                print()
                                print(name, "the", player.title, "has killed", enemy.title, "!")
                                print()
                                break
                            else:
                                continue
                        else:
                            print(name, "failed to parry. You still have", player.parry, "parries left.")
                            round_damage = roll(enemy.damage_die)
                            player.character_health = player.character_health - round_damage
                            print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                    elif defense == "no" or defense == "n":
                        round_damage = roll(enemy.damage_die)
                        player.character_health = player.character_health - round_damage
                        print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                    else:
                        round_damage = roll(enemy.damage_die)
                        player.character_health = player.character_health - round_damage
                        print(name, "tried to parry but must have misstepped.")
                        print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                else:
                    round_damage = roll(enemy.damage_die)
                    player.character_health = player.character_health - round_damage
                    print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
            else:
                print("The", enemy.title, "misses", name)
        if player.character_health <= 0:
            print()
            print(name, "the", player.title, "DIED!!!! Game Over.")
            exit()
        else:
            pass


def combat(name, player, enemy):
    while True:
        print()
        action = input("Will you Attack, Heal, Counter, or cast a Spell?(Use enter/return to attack)").lower()
        if action == "attack" or action == "":
            twenty_sided_die = roll(20)
            round_num = twenty_sided_die + player.die_modifier
            if twenty_sided_die == 20:
                crit_damage = critical_hit(player.damage_die, player.crit)
                enemy.character_health = enemy.character_health - crit_damage
                print("You rolled a natural 20. That's a critical hit!", name, "did", crit_damage, "damage.")
            elif twenty_sided_die == 1:
                print(name, "rolled a natural 1. You left yourself open for", enemy.title, "to get an additional attack that can't be parried!")
                round_num = dtwenty(enemy.die_modifier)
                if round_num >= player.character_armor:
                    round_damage = roll(enemy.damage_die)
                    player.character_health = player.character_health - round_damage
                    print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                else:
                    print("The", enemy.title, "misses", name)
            elif round_num >= enemy.character_armor:
                if enemy.parry > 0:
                    parry_or_not = roll(6)
                    if parry_or_not >= 4:
                        input("The enemy tries to parry your attack...")
                        round_num = dtwenty(0)
                        if round_num > 10:
                            parry_damage = parry_hit(enemy.damage_die)
                            player.character_health = player.character_health - parry_damage
                            enemy.parry = enemy.parry - 1
                            print("The", enemy.title, "successfully parried your attack. They did", parry_damage, "damage. You have", player.character_health, "health left.")
                            if player.character_health <= 0:
                                print()
                                print(name, "the", player.title, "DIED!!!! Game Over.")
                                exit()
                            else:
                                pass 
                        else:
                            round_damage = roll(player.damage_die)
                            enemy.character_health = enemy.character_health - round_damage
                            print(enemy.title, "failed to parry. You did", round_damage, "damage.")
                    else:
                        round_damage = roll(player.damage_die)
                        enemy.character_health = enemy.character_health - round_damage
                        print("You rolled a", round_num, ". That's a hit!", name, "did", round_damage, "damage.")
                else:
                    round_damage = roll(player.damage_die)
                    enemy.character_health = enemy.character_health - round_damage
                    print("You rolled a", round_num, ". That's a hit!", name, "did", round_damage, "damage.")
            else:
                print("You rolled a", round_num, ". You missed!")
        elif action == "heal":
            if player.health_potion > 0:
                round_healing = healing_die()
                player.character_health += round_healing
                player.health_potion = player.health_potion - 1
                if player.character_health >= player.max_health:
                    player.character_health = player.max_health
                    print("You are back to max health:", player.character_health)
                else:
                    print("You healed", round_healing, ".", name, "has a total health of", player.character_health, ". Your total Health Potions are", player.health_potion)
            elif player.health_potion == 0:
                print("You have used all of your Healing Potions")
                continue
        elif action == "counter":
            round_num = dtwenty(enemy.die_modifier)
            if round_num < player.character_armor:
                crit_damage = critical_hit(player.damage_die, player.crit)
                enemy.character_health = enemy.character_health - crit_damage
                print(enemy.title, "rolled a", round_num, ".", name, "successfully countered the enemy attack! You did", crit_damage, "damage.")
                if enemy.character_health <= 0:
                    print()
                    print(name, "the", player.title, "has killed the", enemy.title, "!")
                    break
                else:
                    continue
            else:
                round_damage = critical_hit(enemy.damage_die, enemy.crit)
                player.character_health = player.character_health - round_damage
                print("Failed to counter. The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                if player.character_health <= 0:
                    print()
                    print(name, "the", player.title, "DIED!!!! Game Over.")
                    exit()
                else:
                    continue
        elif action == "spell":
            if player.spell > 0:
                round_num = dtwenty(player.die_modifier)
                if round_num >= enemy.character_armor:
                    spell_damage = spell_hit(player.damage_die)
                    enemy.character_health = enemy.character_health - spell_damage
                    player.spell = player.spell - 1
                    print("You rolled a", round_num, "Your spell hits, causing the enemy to stagger.", name, "did", spell_damage, "damage. You have", player.spell, "spells left.")
                    if enemy.character_health <= 0:
                        print()
                        print(name, "the", player.title, "has killed", enemy.title, "!")
                        break
                    else:
                        continue
                else:
                    player.spell = player.spell - 1
                    print(name, "your spell missed. You have", player.spell, "spells left.")
            elif player.spell == 0:
                print("You have no spells.")
                continue
        elif action == "status":
            print("AC is", player.character_armor, ", current Health is", player.character_health, ", and max damage is", player.damage_die, ".") 
            print("You also have", player.health_potion, "Health Potions,", player.parry, "parry manuevers, and", player.spell, "spells.")
            continue
        else:
            continue
        if enemy.character_health <= 0:
            print()
            print(name, "the", player.title,"has killed the", enemy.title, "!")
            break
        else:
            pass
        if enemy.health_potion > 0:
            if enemy.character_health <= (enemy.max_health / 2):
                round_healing = healing_die()
                enemy.character_health += round_healing
                enemy.health_potion = enemy.health_potion - 1
                if enemy.character_health >= enemy.max_health:
                    enemy.character_health = enemy.max_health
                    print("The", enemy.title, "is back at full health")
                    continue
                else:
                    print("The", enemy.title, "healed", round_healing, ".")
                    continue
            else:
                pass
        else:
            pass
        if enemy.spell > 0:
            use_spell = roll(6)
            if use_spell >= 4:
                round_num = dtwenty(enemy.die_modifier)
                if round_num >= player.character_armor:
                    spell_damage = spell_hit(enemy.damage_die)
                    player.character_health = player.character_health - spell_damage
                    enemy.spell = enemy.spell - 1
                    print("The", enemy.title, "casts a spell and hits! They did", spell_damage, "damage.", name, "has", player.character_health, "health left.")
                    if player.character_health <= 0:
                        print()
                        print(name, "the", player.title, "DIED!!!! Game Over.")
                        exit()
                    else:
                        input("The enemy's spell causes you to stager, leaving you open for another attack...")
                        print("The", enemy.title, "swings at", name, "!")
                        round_num = dtwenty(enemy.die_modifier)
                        if round_num >= player.character_armor:
                            round_damage = roll(enemy.damage_die)
                            player.character_health = player.character_health - round_damage
                            print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                        else:
                            print("The", enemy.title, "misses", name)
                        if player.character_health <= 0:
                            print()
                            print(name, "the", player.title, "DIED!!!! Game Over.")
                            exit()
                        else:
                            continue
                else:
                    enemy.spell = enemy.spell - 1
                    print("The", enemy.title, "missed with their spell!")
                    continue
            else:
                pass
        else:
            pass
        print("The", enemy.title, "swings at", name, "!")
        round_num = dtwenty(enemy.die_modifier)
        if round_num >= player.character_armor:
            if player.parry > 0:
                defense = input("The enemy is going to hit you, will you parry? Yes or No?").lower()
                if defense == "yes" or defense == "y":
                    round_num = dtwenty(0)
                    if round_num > 10:
                        parry_damage = parry_hit(player.damage_die)
                        enemy.character_health = enemy.character_health - parry_damage
                        player.parry = player.parry - 1
                        print("You successfully parried the attack.", name, "did", parry_damage, "damage. You have", player.parry, "parries left.")
                        if enemy.character_health <= 0:
                            print()
                            print(name, "the", player.title, "has killed the", enemy.title, "!")
                            print()
                            break
                        else:
                            continue
                    else:
                        print(name, "failed to parry. You still have", player.parry, "parries left.")
                        round_damage = roll(enemy.damage_die)
                        player.character_health = player.character_health - round_damage
                        print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                elif defense == "no" or defense == "n":
                    round_damage = roll(enemy.damage_die)
                    player.character_health = player.character_health - round_damage
                    print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
                else:
                    round_damage = roll(enemy.damage_die)
                    player.character_health = player.character_health - round_damage
                    print(name, "tried to parry but must have misstepped.")
                    print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
            else:
                round_damage = roll(enemy.damage_die)
                player.character_health = player.character_health - round_damage
                print("The", enemy.title, "hits you!", name, "was damaged for", round_damage, "health points, your health is", player.character_health)
        else:
            print("The", enemy.title, "misses", name)
        if player.character_health <= 0:
            print()
            print(name, "the", player.title, "DIED!!!! Game Over.")
            exit()
        else:
            pass